<?php
/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();
$sql2 = array();
$sql3 = array();
$sql4 = array();
$sql5 = array();
$sql6 = array();
$sql7 = array();


$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'user_creation` (
    `id_product` int(11) NOT NULL,
    `id_user` int(11) NOT NULL,
    `num_sold` int(50) NOT NULL,
    PRIMARY KEY  (`id_product`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql2[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'user_product_validation` (
    `id_product` int(11) NOT NULL,
    `id_user` int(11) NOT NULL,
    `message` varchar(5000),
    `validated` int(1) NOT NULL,
    `to_admin` int(1) NOT NULL,
    `time_stamp` TIMESTAMP,
    `notification_user` int(2) NOT NULL,
    `notification_admin` int(2) NOT NULL,,
    `lastsawuser` TIMESTAMP, 
    `lastsawadmin` TIMESTAMP,
    PRIMARY KEY  (`id_product`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql3[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'stluploader_message` (
    `id_message` int NOT NULL AUTO_INCREMENT,
    `id_product` int(11) NOT NULL,
    `id_user` int(11) NOT NULL,
    `author` varchar(24) NOT NULL,
    `message` varchar(5000),
    `time_stamp` TIMESTAMP, 
    PRIMARY KEY  (`id_message`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql4[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'stluploader_designers` (
    `id_user` int(11) NOT NULL,
    `time_stamp` TIMESTAMP, 
    PRIMARY KEY  (`id_user`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql5[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'stluploader_registeringdesigners` (
    `id_user` int(11) NOT NULL,
    `validated` int(1) NOT NULL,
    `message` varchar(250000),
    `time_stamp` TIMESTAMP, 
    `notification_user` int(2) NOT NULL,
    `notification_admin` int(2) NOT NULL,
    `lastsawuser` TIMESTAMP, 
    `lastsawadmin` TIMESTAMP,
    PRIMARY KEY  (`id_user`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql6[] =
'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'stluploader_proposal_message` (
    `id_message` int NOT NULL AUTO_INCREMENT,
    `id_user` int(11) NOT NULL,
    `author` varchar(24) NOT NULL,
    `message` varchar(5000),
    `time_stamp` TIMESTAMP, 
    PRIMARY KEY  (`id_message`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql7[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . 'stluploader_images` (
    `id_product` int(11) NOT NULL,
    `name` varchar(70)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
       return false;
    }
}


foreach ($sql2 as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}

foreach ($sql3 as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}

foreach ($sql4 as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}

foreach ($sql5 as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
foreach ($sql6 as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}