export interface IProducts extends Array<IProduct> {
  [index: number]: IProduct;
}

export interface IImage {
  url: string;
  url_large: string;
  name: string;
  size: number;
  id: number;
}
export interface IMessage {
  id?: number;
  isEdited?: boolean;
  type: "text";
  author: string;
  data: {
    text: any;
  };
}

export interface IFile {
  name: string;
  size: number;
}
export interface IProduct {
  name: string;
  price: number;
  num_sold: number;
  shortDescription: string;
  description: string;
  keyWords: string;
  validated: boolean;
  images: {
    cover: IImage;
    others: IImage[];
  };
  files: IFile[];
  filesecret: string;
  categories: {
    id: string;
    name: string;
    code: string;
  }[];
  categories_selection: {
    id: string;
    name: string;
    code: string;
  }[];
  notifications: number;
  uneditable: boolean;
  sales_total: number;
  sales_lastmonth: number;
  num_sold_lastmonth: number;
  owned: boolean;
  cut: number;
}
export interface IStats {
  info: {
    name: string;
    this_month: string;
    last_month: string;
    total: string;
  }[];
  to: string;
  from: string;
  to_before: string;
  from_before: string;
  product_month: {
    name: string;
    sold: number;
    cut: number;
    sales: number;
    sales_fee: number;
  };
}
