import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/axios";
import {
  DropdownPlugin,
  FormGroupPlugin,
  ButtonPlugin,
  LinkPlugin,
  FormSelectPlugin,
  FormPlugin,
  FormInputPlugin,
  AlertPlugin,
  FormTextareaPlugin,
  TablePlugin,
  ImagePlugin,
  LayoutPlugin,
  BadgePlugin,
  InputGroupPlugin,
  ModalPlugin,
  PaginationPlugin,
  FormCheckboxPlugin,
} from "bootstrap-vue";
import VueTippy, { TippyComponent } from "vue-tippy";
Vue.use(VueTippy);
Vue.component("tippy", TippyComponent);
import "tippy.js/themes/light.css";
import "tippy.js/themes/light-border.css";
import "tippy.js/themes/google.css";
import "tippy.js/themes/translucent.css";
import VuejsDialog from "vuejs-dialog";
Vue.use(VuejsDialog);
import "vuejs-dialog/dist/vuejs-dialog.min.css";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Chat from "vue-beautiful-chat";
Vue.use(Chat);
Vue.use(FormCheckboxPlugin);
Vue.use(PaginationPlugin);
Vue.use(ModalPlugin);
Vue.use(ButtonPlugin);
Vue.use(FormInputPlugin);
Vue.use(LinkPlugin);
Vue.use(DropdownPlugin);
Vue.use(FormPlugin);
Vue.use(FormSelectPlugin);
Vue.use(AlertPlugin);
Vue.use(FormTextareaPlugin);
Vue.use(FormGroupPlugin);
Vue.use(TablePlugin);
Vue.use(ImagePlugin);
Vue.use(BadgePlugin);
Vue.use(InputGroupPlugin);
Vue.use(LayoutPlugin);
Vue.config.productionTip = false;
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
