import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/CreateProduct.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/products",
    name: "Vytvořte nový produkt",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Products.vue"),
  },
  {
    path: "/productdetail/:id",
    name: "Produkty",
    props: true,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this routemodules
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/ProductDetail.vue"),
  },
  {
    path: "/howto",
    name: "Jak na to",
    component: () => import("../views/Howto.vue"),
  },
  {
    path: "/payment",
    name: "Statistika prodeje",
    component: () => import("../views/Payment.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
