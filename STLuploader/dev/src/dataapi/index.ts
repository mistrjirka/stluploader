import Axios, { AxiosError } from "axios";
import { IProducts, IProduct, IMessage, IStats } from "../definitions";

export interface ServerSendMessageResponse {
  data: "OK" | null;
  error: AxiosError | null;
}
export interface ServerMessageResponse {
  data: IMessage[] | null;
  error: AxiosError | null;
}

export interface ServerProductsResponse {
  data: IProducts | null;
  error: AxiosError | null;
}
export interface ServerProductInfoResponse {
  data: IProduct | null;
  error: AxiosError | null;
}
export interface ServerStatsResponse {
  data: IStats | null;
  error: AxiosError | null;
}

export abstract class InfoApi {
  private static dataAxios = Axios.create();
  static async getProducts(
    url = "/module/STLuploader/getproducts"
  ): Promise<ServerProductsResponse> {
    let responseData: IProducts | undefined = undefined;
    let error: AxiosError | null = null;
    const data = new FormData();
    data.append("type", "getproducts");
    await this.dataAxios
      .post<IProducts>(url, data)
      .then((response) => {
        responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
  static async getProductInfo(
    id: string,
    url = "/module/STLuploader/getproducts"
  ): Promise<ServerProductInfoResponse> {
    let responseData: IProduct | undefined = undefined;
    let error: AxiosError | null = null;
    const data = new FormData();
    data.append("id_product", id);
    data.append("type", "getinfo");
    await this.dataAxios
      .post<IProduct>(url, data)
      .then((response) => {
        responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
  static async getMessages(
    id: string,
    url = "/module/STLuploader/message"
  ): Promise<ServerMessageResponse> {
    let responseData: ServerMessageResponse | undefined = undefined;
    let error: AxiosError | null = null;
    const data = new FormData();
    data.append("id_product", id);
    data.append("action", "getmessages");
    await this.dataAxios
      .post<ServerMessageResponse>(url, data)
      .then((response) => {
        responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
  static async sendMessage(
    id: string,
    message: string,
    url = "/module/STLuploader/message"
  ): Promise<ServerSendMessageResponse> {
    let responseData: ServerSendMessageResponse | undefined = undefined;
    let error: AxiosError | null = null;
    const data = new FormData();
    data.append("id_product", id);
    data.append("message", message);

    data.append("action", "sendmessage");
    await this.dataAxios
      .post<ServerSendMessageResponse>(url, data)
      .then((response) => {
        responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
  static async getStats(
    url = "/module/STLuploader/getstats"
  ): Promise<ServerStatsResponse> {
    let responseData: IStats | undefined = undefined;
    let error: AxiosError | null = null;
    const data = new FormData();

    await this.dataAxios
      .post<IStats>(url, data)
      .then((response) => {
        responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
}
