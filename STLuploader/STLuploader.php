<?php

/**
 * 2007-2021 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2021 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once(_PS_MODULE_DIR_ . "STLuploader/classes/userCreationsClass.php");
require_once(_PS_MODULE_DIR_ . "STLuploader/classes/userProductValidationClass.php");
require_once(_PS_MODULE_DIR_ . "STLuploader/classes/stluploaderImagesClass.php");
require_once(_PS_MODULE_DIR_ . "STLuploader/classes/STLStatsProduct.php");

use PrestaShop\PrestaShop\Adapter\SymfonyContainer;
use PrestaShop\PrestaShop\Core\Domain\Category\Exception\CategoryNotFoundException;
use PrestaShop\PrestaShop\Core\Domain\Category\ValueObject\CategoryId;
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;


class STLuploader extends Module implements WidgetInterface
{
    protected $config_form = false;
    public $adminControllers;

    public function __construct()
    {
        $this->name = 'STLuploader';
        $this->tab = 'administration';
        $this->version = '0.0.1';
        $this->author = 'Jiří Svítil';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('STL uploader and management module');
        $this->description = $this->l('STL uploader and management module');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall the module?');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->adminControllers = [
            'images' => 'AdminImageController',

        ];
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('STLUPLOADER_LIVE_MODE', false);
        Configuration::updateValue('STLUPLOADER_PAYDAY', 15);

        include(dirname(__FILE__) . '/sql/install.php');
        return parent::install() &&
            $this->registerHook('actionAdminControllerSetMedia') &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionObjectOrderAddAfter') &&
            $this->registerHook('actionPaymentConfirmation') &&
            $this->registerHook('displayBackOfficeFooter') &&
            $this->registerHook('displayFeaturePostProcess') &&
            $this->registerHook('actionValidateOrder');
    }

    public function uninstall()
    {
        Configuration::deleteByName('STLUPLOADER_LIVE_MODE');
        include(dirname(__FILE__) . '/sql/uninstall.php');

        return parent::uninstall();
    }
    public function getAdminControllers()
    {
        return $this->adminControllers;
    }
    /**
     * Load the configuration form
     */
    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getContent()
    {

        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('deleteSTLuploader_categories')) == true && Tools::getValue("id_product")) {
            $product_id = Tools::getValue("id_product");
            $product = new Product($product_id);
            $db = \Db::getInstance();

            $product->delete() && $db->delete('user_product_validation', "id_product=$product_id") && $db->delete('stluploader_message', "id_product=$product_id") && $db->delete('user_creation', "id_product=$product_id");
        }
        $form = "";
        $this->context->smarty->assign('module_dir', $this->_path);
        $link = new Link;
        $url = $link->getAdminLink("AdminModules");
        $newpeople = intval(count($this->getFromTable("stluploader_registeringdesigners", "id_user", "id_user", "validated=0")));
        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');
        $proposals = '<a class="btn btn-default" style="margin-bottom: 1em;" href=' . $url . '&configure=STLuploader&viewSTLuploader_proposals=" role="button">Zobrazit požadavky na pozici designera <span class="badge"> ' . $newpeople . ' </span></a> ';
        $viewproductsvalidated = '<br><a class="btn btn-default" style="margin-bottom: 1em;" href=' . $url . '&configure=STLuploader&viewSTLuploader_validatedproducts=" role="button">Zobrazit schválené produkty</a>';

        $listproposals = "";
        $listvalidatedproducts = "";
        if (((bool)Tools::isSubmit('viewSTLuploader_categories')) == true || (bool)Tools::isSubmit('viewSTLuploader_viewed') == true) {
            $proposals = '<a class="btn btn-default" style="margin-bottom: 1em;" href=' . $url . '&configure=STLuploader&" role="button">Zpět</a>';
            $form = $this->renderProductForm();
        }






        if (((bool)Tools::isSubmit('viewSTLuploader_proposals')) == true) {
            $proposals = '<a class="btn btn-default" style="margin-bottom: 1em;" href=' . $url . '&configure=STLuploader&" role="button">Zpět</a>';
            $listproposals = $this->renderListofProposals();
        }

        if (((bool)Tools::isSubmit('viewSTLuploader_validatedproducts')) == true &&
            !Tools::isSubmit('viewSTLuploader_viewed') == true
        ) {
            $proposals = '<a class="btn btn-default" style="margin-bottom: 1em;" href=' . $url . '&configure=STLuploader&" role="button">Zpět</a>';
            $listvalidatedproducts = $this->renderList(true);
            $viewproductsvalidated = "";
        }

        if (((bool)Tools::isSubmit('viewSTLuploader_proposals_table')) == true && Tools::getValue("id_user")) {
            $proposals = '<a class="btn btn-default" style="margin-bottom: 1em;" href=' . $url . '&configure=STLuploader&" role="button">Zpět</a>';
            $id_user = Db::getInstance()->escape(Tools::getValue("id_user"));
            $proposalContent = $this->getFromTable("stluploader_registeringdesigners", "time_stamp", "*", "id_user=$id_user")[0];
            $proposals .= $this->renderProposal($proposalContent);
        }

        $this->updateDate();
        return $proposals . $viewproductsvalidated . $listproposals . $listvalidatedproducts . $output . $form . $this->renderList(false) . $this->renderConfigDate();
    }
    protected function updateDate()
    {
        if (Tools::isSubmit("submitPaydayConfig")) {
            $id = intval(Tools::getValue("STLUPLOADER_PAYDAY"));
            Configuration::updateValue("STLUPLOADER_PAYDAY", $id);
        }
    }
    protected function closetags($html)
    {
        preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];
        $len_opened = count($openedtags);
        if (count($closedtags) == $len_opened) {
            return $html;
        }
        $openedtags = array_reverse($openedtags);
        for ($i = 0; $i < $len_opened; $i++) {
            if (!in_array($openedtags[$i], $closedtags)) {
                $html .= '</' . $openedtags[$i] . '>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }
    protected function renderProposal($data)
    {
        $link = new Link;

        $router = SymfonyContainer::getInstance()->get('router');

        $pictures = [];
        $customer_id = $data["id_user"];
        try {
            if (file_exists(_PS_UPLOAD_DIR_ . "registering_proposals/" . $customer_id)) {
                $pictures_uf = scandir(_PS_UPLOAD_DIR_ . "registering_proposals/" . $customer_id);
                try {
                    foreach ($pictures_uf as $value) {

                        if ($value == "." || $value == "..") {
                        } else {

                            array_push($pictures,  "/upload/registering_proposals/" . $customer_id . "/" . $value);
                        }
                    };
                } catch (Throwable $e) {
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
        $customer = new Customer($customer_id);
        $toreturn = [];

        $from = new DateTime();
        $from->setDate(intval(date("Y")), intval(date("m", strtotime("-1 months"))), intval(Configuration::get("STLUPLOADER_PAYDAY", 15)));

        $to = new DateTime();
        $to->setDate(intval(date("Y")), intval(date("m")), intval(Configuration::get("STLUPLOADER_PAYDAY", 15)));

        $from_before = new DateTime();
        $from_before->setDate(intval(date("Y")), intval(date("m", strtotime("-2 months"))), intval(Configuration::get("STLUPLOADER_PAYDAY", 15)));

        $to_before = new DateTime();
        $to_before->setDate(intval(date("Y")), intval(date("m", strtotime("-1 months"))), intval(Configuration::get("STLUPLOADER_PAYDAY", 15)));

        $toreturn = array_merge(
            $this->getStatsTotal($customer_id),
            $this->getStatsFrom($from->format('Y-m-d'), $to->format('Y-m-d'), $customer_id),
            $this->getStatsFrom($from_before->format('Y-m-d'), $to_before->format('Y-m-d'), $customer_id, "last_")
        );

        $toreturn["product_month"] = $this->getBestProductFrom(date("Y-m-d H:i:s", strtotime("-1 months")), $customer_id);

        $params = array(
            "message" => $this->closetags($data["message"]),
            "time_stamp" => $data["time_stamp"],
            "getmsgUrl" => $router->generate('proposal_get_route'),
            "sendmsgUrl" => $router->generate('proposal_send_route'),
            "acceptUrl" => $router->generate('accept_proposal_route'),
            "denyUrl" => $router->generate('deny_proposal_route'),
            "backredirectURL" => $link->getAdminLink("AdminModules") . '&configure=STLuploader',
            "state" => ["Nerozhodnuto", "zamítnuto", "Přijato"][intval($data["validated"])],
            "validated" => intval($data["validated"]),
            "pictures" => $pictures,
            "customer" => $customer->firstname . " " . $customer->lastname,
            "customeremail" => $customer->email,
            "from" => $from->format('Y-m-d'),
            "to" => $to->format('Y-m-d'),
            "from_before" => $from_before->format('Y-m-d'),
            "to_before" => $to_before->format('Y-m-d'),
        );
        //die(var_dump(array_merge($params, $toreturn)));
        $this->context->smarty->assign(array_merge($params, $toreturn));
        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/proposal.tpl');
    }
    protected function getStatsTotal($customer_id)
    {
        $sql = "SELECT  COALESCE(SUM(order_detail.total_price_tax_incl), 0) AS sales_total, 
                        COALESCE(SUM(order_detail.product_quantity),0) AS sold_total, 
                        COALESCE(SUM(order_detail.total_price_tax_incl - (product_validation.cut * order_detail.total_price_tax_incl)),0) AS sales_total_fee_designer,
                        COALESCE(SUM(product_validation.cut * order_detail.total_price_tax_incl),0) AS sales_total_fee_shop,
                        COALESCE(AVG(product_validation.cut),0) AS cut_avg_total 
        FROM " . _DB_PREFIX_ . "order_detail as order_detail 
        INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order 
        INNER JOIN " . _DB_PREFIX_ . "user_product_validation as product_validation ON product_validation.id_product=order_detail.product_id 
        WHERE product_validation.owned=0 AND orders.valid=1 AND product_validation.id_user=" . $customer_id;
        $result = Db::getInstance()->executeS($sql);

        return $result[0];
    }
    protected function getStatsFrom($from, $to, $customer_id, $prefix = "")
    {

        $sql = "SELECT  COALESCE(SUM(order_detail.total_price_tax_incl),0) AS " . $prefix . "sales_month, 
                        COALESCE(SUM(order_detail.product_quantity),0) AS " . $prefix . "sold_month, 
                        COALESCE(SUM(order_detail.total_price_tax_incl - (product_validation.cut * order_detail.total_price_tax_incl)),0) AS " . $prefix . "sales_month_fee_designer,
                        COALESCE(SUM(product_validation.cut * order_detail.total_price_tax_incl),0) AS " . $prefix . "sales_month_fee_shop,
                        COALESCE(AVG(product_validation.cut), 0) AS " . $prefix . "cut_avg_month 
        FROM " . _DB_PREFIX_ . "order_detail AS order_detail 
        INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order 
        INNER JOIN " . _DB_PREFIX_ . "user_product_validation as product_validation ON product_validation.id_product=order_detail.product_id 
        WHERE product_validation.owned=0 AND orders.valid=1 AND product_validation.id_user=" . $customer_id . " AND orders.date_add > " . '"' . $from . '" AND orders.date_add < "' . $to . '"';
        $result = Db::getInstance()->executeS($sql);
        return $result[0];
    }
    protected function getBestProductFrom($date, $customer_id)
    {
        $ids_product_sql = "SELECT id_product FROM " . _DB_PREFIX_ . "user_product_validation WHERE id_user=" . $customer_id;
        $result_ids =  Db::getInstance()->executeS($ids_product_sql);
        $ids = "";
        foreach ($result_ids as $i => $id) {
            if ($i + 1 < count($result_ids)) {
                $ids .= $id["id_product"] . ",";
            } else {
                $ids .= $id["id_product"];
            }
        }

        $result = [
            "sales" => 0,
            "sold" => 0,
            "sales_fee" => 0,
            "cut" => 0,
            "name" => ""
        ];

        if (count($result_ids)) {

            $sql = "SELECT  COALESCE(SUM(order_detail.total_price_tax_incl),0) AS sales, 
                            COALESCE(SUM(order_detail.product_quantity),0) AS sold, 
                            COALESCE(SUM(order_detail.total_price_tax_incl - (product_validation.cut * order_detail.total_price_tax_incl)),0) AS sales_fee,
                            COALESCE(product_validation.cut, 0) AS cut,
                            COALESCE(product.name, '') AS name
            FROM " . _DB_PREFIX_ . "order_detail as order_detail 
            INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order 
            INNER JOIN " . _DB_PREFIX_ . "user_product_validation as product_validation ON product_validation.id_product=order_detail.product_id 
            INNER JOIN " . _DB_PREFIX_ . "product_lang as product ON product.id_product=order_detail.product_id 
            WHERE product_validation.owned=0 AND orders.valid=1 AND product.id_lang=" . $customer_id . ' AND product_validation.id_user=' . $customer_id . " AND orders.date_add > " . '"' . $date . '" AND product_validation.id_product=(
                SELECT id_product FROM `' . _DB_PREFIX_ . 'product_sale`
                WHERE quantity = (SELECT 
                MAX(quantity)
                    FROM
                `' . _DB_PREFIX_ . 'product_sale` WHERE id_product IN (' . $ids . ') ) AND id_product IN (' . $ids . ') LIMIT 1
            );';
            $result = Db::getInstance()->executeS($sql)[0];
        }

        return $result;
    }
    protected function renderConfigDate()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = "stluploader_date";
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPaydayConfig';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => [
                "STLUPLOADER_PAYDAY" =>  Configuration::get("STLUPLOADER_PAYDAY", 15),

            ]
        );

        $options = [];
        for ($i = 1; $i < 29; $i++) {
            array_push($options, [
                "id_option" => $i,
                "name" => $i
            ]);
        }
        $form = [
            [
                'form' => [
                    'legend' => [
                        'title' => $this->l('Konfigurace modulu'),
                        'icon' => 'icon-cogs'
                    ],
                    'input' => [
                        [
                            'type' => 'select',                              // This is a <select> tag.
                            'label' => $this->l('Výplatní den:'),         // The <label> for this <select> tag.
                            'desc' => $this->l('Vyberte den výplat'),  // A help text, displayed right next to the <select> tag.
                            'name' => 'STLUPLOADER_PAYDAY',                     // The content of the 'id' attribute of the <select> tag.
                            'required' => true,                              // If set to true, this option must be set.
                            'options' => [
                                'query' => $options,                           // $options contains the data itself.
                                'id' => 'id_option',                           // The value of the 'id' key must be the same as the key for 'value' attribute of the <option> tag in each $options sub-array.
                                'name' => 'name'                               // The value of the 'name' key must be the same as the key for the text content of the <option> tag in each $options sub-array.
                            ]
                        ]
                    ],
                    'submit' => [
                        'title' => $this->l('Save'),
                        'class' => 'btn btn-default pull-right'
                    ],
                ],
            ],
        ];

        return $helper->generateForm($form);
    }
    protected function renderProductForm()
    {
        $id_product = Tools::GetValue('id_product');
        $product = new STLStatsProduct($id_product);
        $img = $product->getCover($product->id);
        $link = new Link;
        $image_objects = $product->getImages($this->context->language->id);
        $image_urls = array();
        foreach ($image_objects as $image_object) {
            if ($image_object["cover"] != 1) {
                $imgClass = new Image(intval($image_object['id_image']));
                $name = strval($imgClass->id_image) . ".jpg";
                try {
                    $name = $this->getFromTable("stluploader_images", "id_image", "name", "id_image=" . $imgClass->id_image)[0]["name"];
                } catch (Throwable $error) {
                }
                array_push($image_urls, array(
                    "url" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id],  $image_object['id_image'], 'home_default'),
                    "url_large" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $image_object['id_image'], ''),
                    "name" => $name,
                    "size" => 128,
                    "id" => $imgClass->id
                ));
            }
        }

        $cover = new Image($img["id_image"]);

        $name = strval($cover->id_image) . ".jpg";
        try {
            $name = $this->getFromTable("stluploader_images", "id_image", "name", "id_image=" . $cover->id_image)[0]["name"];
        } catch (Throwable $error) {
        }

        $images = array(
            "cover" => array(
                "url" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $img['id_image'], 'home_default'),
                "url_large" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $img['id_image'], ''),
                "name" => $name,
                "size" => 128,
                "id" => $img['id_image']
            ),
            "others" => $image_urls
        );


        // section for virtual product


        try {
            $product_download_id = $this->getFromTable("product_download", "id_product", "id_product_download", "id_product = $product->id")[0]["id_product_download"];
        } catch (Throwable $error) {
            echo "Tento produkt není virtuální!";
            return "Tento produkt není virtuální!";
        }

        $download = new ProductDownload($product_download_id);

        $download_path = _PS_DOWNLOAD_DIR_ . $download->filename;
        $random_string = $this->generateRandomString(15);
        $file_name = _PS_UPLOAD_DIR_ . $random_string;

        try {
            mkdir($file_name);
        } catch (Throwable $err) {
        }

        $zip = new ZipArchive;
        $filelist_uf = [];
        $filelist = [];

        if ($zip->open($download_path) === TRUE) {
            // Unzip Path
            $zip->extractTo($file_name);
            $zip->close();
            $filelist_uf = scandir($file_name . "/3D", SCANDIR_SORT_ASCENDING);
        } else {
            echo 'Unzipped Process failed';
        }
        foreach ($filelist_uf as $value) {
            if ($value == "." || $value == "..") {
            } else {
                array_push($filelist, ["name" => $value, "size" => filesize($file_name . "/3D/" . $value)]);
            }
        };
        $allCat = $this->getAllCategories();

        $router = SymfonyContainer::getInstance()->get('router');
        $categoriesTmp = $product->getCategories();
        $categories = [];
        $children = [];
        $id_main = $this->getCategoryId();
        foreach ($allCat as $category) {
            if ($category != $id_main) {
                $selected = false;
                foreach ($categoriesTmp as $selCategory) {
                    if ($category == $selCategory) $selected = true;
                }
                $categoryClass = new Category(intval($category));
                $nameofcategory =  $categoryClass->getName();
                if (!in_array($nameofcategory, $children)) {
                    array_push($categories, [
                        "id" => $category,
                        "name" => $nameofcategory,
                        "selected" => $selected
                    ]);
                }
                $childrenCategories = $categoryClass->getAllChildren();
                foreach ($childrenCategories as $child) {
                    $selChildren = false;
                    foreach ($categoriesTmp as $selCategory) {
                        if ($child->id == $selCategory) $selChildren = true;
                    }
                    array_push($children, $child->name);
                    array_push($categories, [
                        "id" => $child->id,
                        "name" => " - " . $child->name,
                        "selected" => $selChildren
                    ]);
                }
            }
        }
        $product_module_table = $this->getFromTable("user_product_validation", "", "id_user, owned, cut", "id_product=" . $id_product);
        $owned = 0;
        $cut = 1;
        $id_user = 0;
        if (!$product_module_table) {
            $author = "Neznámý uživatel";
        } else {
            $user = $this->getFromTable("customer", "id_customer", "firstname, lastname", "id_customer=" . $product_module_table[0]["id_user"]);
            $id_user = $product_module_table[0]["id_user"];
            $author = $user[0]["firstname"] . " " . $user[0]["lastname"];
            $owned = $product_module_table[0]["owned"];
            $cut = $product_module_table[0]["cut"];
        }
        $admin = explode(DIRECTORY_SEPARATOR, _PS_ADMIN_DIR_);
        $to_pop = (array_slice($admin, -1));
        $admin_folder = array_pop($to_pop);


        $this->context->smarty->assign(array(
            "author" => $author,
            "owned" => $owned,
            "cut" => $cut * 100,
            "images" => json_encode($images),
            "product" => $product,
            "files" => json_encode($filelist),
            "uploadURL" => $router->generate('image_upload_route'),
            "deleteURL" => $router->generate('image_delete_route'),
            "uploadFileURL" => $router->generate('file_upload_route'),
            "deleteFileURL" => $router->generate('file_delete_route'),
            "getmessagesURL" => $router->generate('message_get_route'),
            "sendmessagesURL" => $router->generate('message_send_route'),
            "editProductURL" => $router->generate('product_edit_route'),
            "activateProductURL" => $router->generate('product_activate_route'),
            "removeProductURL" => $router->generate('product_remove_route'),
            "securePrefix" => $random_string,
            "updateURL" => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
            "targetPath" => $download_path,
            "categories" => $categories,
            "backredirectURL" => $link->getAdminLink("AdminModules") . '&configure=STLuploader',
            "num_sold" => $product->getNbOfSales($id_product),
            "num_sold_lastmonth" => $product->getNbSoldfrom(date("Y-m-d H:i:s", strtotime("-1 months"))),
            "sales_total" => round(floatval($product->getSalesTotal()), 2),
            "sales_lastmonth" => round($product->getSales(date("Y-m-d H:i:s", strtotime("-1 months"))), 2),
            "author_link" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" . $admin_folder . "/index.php?controller=AdminModules&configure=STLuploader&viewSTLuploader_proposals=&viewSTLuploader_proposals_table=&id_user=$id_user&token=" . Tools::getAdminTokenLite('AdminModules'),
            "redirect_product" => $router->generate("redirect_product_route") . "&id_product=" . $id_product
        ));

        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/productForm.tpl');
    }
    //list of users that want to become designers
    protected function renderListofProposals()
    {
        $fields_list_proposals = array(

            'id_user' => array(
                'title' => $this->l('Id user'),
                'width' => 40,
                'type' => 'text',
            ),

            'user' => array(
                'title' => $this->l("Jméno autora"),
                'width' => 100,
                'type' => 'text',
            ),
            "shortDescription" => [
                'title' => $this->l("Zkrácená zpráva"),
                'width' => 300,
                'type' => 'text',
            ],
            "validated" => [
                'title' => $this->l("Schválen"),
                'width' => 300,
                'type' => 'text',
            ]

        );
        $pagination_length = 20;

        $helper = new HelperList();

        $helper->shopLinkType = '';

        $helper->simple_header = false;

        // Actions to be displayed in the "Actions" column
        $helper->actions = array('view');

        $helper->identifier = 'id_user';
        $helper->show_toolbar = true;
        $helper->title = 'Požadavky na pozici designera';
        $helper->table = $this->name . '_proposals_table';
        $prefixName = $helper->table;
        $current_index = "0";
        if (Tools::getValue($prefixName . 'current_index')) {
            $current_index = Tools::getValue($prefixName . 'current_index');
        }
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . "&current_index=" . $current_index . '&viewSTLuploader_proposals=';

        $user_proposals_filtered = [];

        $orderWay = "DESC";
        $orderBy = "id_user";
        if (Tools::getValue($prefixName . "Orderby")) {

            if (Tools::getValue($prefixName . "Orderby") == "user") {
                $orderBy = "id_user";
                $orderWay = Tools::getValue($prefixName . "Orderway");
            } elseif (Tools::getValue($prefixName . "Orderby") == "shortDescription") {
                $orderBy = "message";
                $orderWay = Tools::getValue($prefixName . "Orderway");
            } else {
                $orderBy = Tools::getValue($prefixName . "Orderby");
                $orderWay = Tools::getValue($prefixName . "Orderway");
            }
        }
        $condition = "1=1";
        $proposal_filters = [];
        if (Tools::isSubmit("submitFilter") && Tools::getValue("submitFilterSTLuploader_proposals_table")) {
            $condition = "";
            if (strlen(Tools::getValue($prefixName . "Filter_id_user"))) {
                $condition .= " id_user LIKE '%" . Tools::getValue($prefixName . "Filter_id_user") . "%'";
            }
            if (strlen(Tools::getValue($prefixName . "Filter_shortDescription"))) {
                $condition .= " message LIKE '%" . Tools::getValue($prefixName . "Filter_shortDescription") . "%'";
            }

            if (strlen(Tools::getValue($prefixName . "Filter_user"))) {
                array_push($proposal_filters, [
                    "name" => "user",
                    "data" => Tools::getValue($prefixName . "Filter_user"),
                    "multiple" => false,
                    "specific" => false

                ]);
            }

            if (strlen(Tools::getValue($prefixName . "Filter_validated"))) {
                array_push($proposal_filters, [
                    "name" => "validated",
                    "data" => Tools::getValue($prefixName . "Filter_validated"),
                    "multiple" => false,
                    "specific" => false

                ]);
            }
        }

        $user_proposals_unfiltered = $this->getFromTable("stluploader_registeringdesigners", $orderBy . " " . $orderWay, "*", $condition);
        foreach ($user_proposals_unfiltered as $key => $proposal) {
            $user = $this->getFromTable("customer", "id_customer", "firstname, lastname", "id_customer=" . $proposal['id_user']);
            if (!$user) {
                $username = "smazaný uživatel";
            } else {
                $username = $user[0]["firstname"] . " " . $user[0]["lastname"];
            }
            $user_proposals_filtered[$key] = [
                "id_user" => $proposal["id_user"],
                "user" =>
                $username,
                "shortDescription" => substr(
                    strip_tags($proposal["message"]),
                    0,
                    25
                ),
                "validated" => ["Nezkontrolován", "Zamítnut", "Schválen"][intval($proposal["validated"])]
            ];
        }
        function afterfilters($object, $params)
        {
            return $object;
        }

        $user_proposals_filtered = $this->filterArray($proposal_filters, $user_proposals_filtered, 'afterfilters', []);

        $current_index = intval(Tools::getValue($prefixName . 'current_index'));
        $this->context->smarty->assign(array(
            "num_ofpages" => ceil(count($user_proposals_filtered) / $pagination_length),
            "current_index" => $current_index + 1,
            "query" => AdminController::$currentIndex . '&configure=' . $this->name . '&viewSTLuploader_proposals=',
            "prefix" => $prefixName,
            'token' => Tools::getAdminTokenLite('AdminModules'),
        ));
        $user_proposals_filtered = array_slice($user_proposals_filtered, $current_index * $pagination_length, $pagination_length);
        $pagination = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/pagination.tpl');

        return $helper->generateList($user_proposals_filtered, $fields_list_proposals) . $pagination;
    }


    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderList($validated)
    {
        $this->fields_list = array(
            'image' =>  array(

                'title' => $this->l('Image'),

                'align' => 'center',

                'image' => 'p',

                'orderby' => false,

                'filter' => false,

                'search' => false

            ),
            'id_product' => array(
                'title' => $this->l('Id'),
                'width' => 40,
                'type' => 'text',
            ),
            'name' => array(
                'title' => $this->l('Product name'),
                'type' => 'text',
                'width' => 100
            ),

            'user' => array(
                'title' => $this->l("Jméno autora"),
                'width' => 100,
                'type' => 'text',
            ),
            'price_tax_exc' => array(
                'title' => $this->l('Price without tax'),
                'width' => 40,
                'type' => 'decimal',
                'suffix' => 'Kč',
            ),
            'price' => array(
                'title' => $this->l('Price'),
                'width' => 40,
                'type' => 'decimal',
                'suffix' => 'Kč',
            ),
            'owned' => array(
                'title' => $this->l('Odkoupeno'),
                'width' => 40,
                'type' => 'text',
            ),
            'profitmargin' => array(
                'title' => $this->l('Odvedno z prodeje'),
                'width' => 40,
                'type' => 'decimal',
                'suffix' => '%',
            ),
            'sold' => array(
                'title' => $this->l('Prodáno'),
                'width' => 40,
                'type' => 'text',
                'suffix' => '',
            ),
            'profit' => array(
                'title' => $this->l('Výdělek'),
                'width' => 40,
                'type' => 'text',
                "suffix" => 'Kč'
            ),
            'notification_admin' => array(
                'title' => $this->l('Notifikace'),
                'width' => 40,
                'type' => 'text',
                'color' => 'blue'
            ),

        );
        $pagination_length = 20;

        $helper = new HelperList();

        $helper->shopLinkType = '';

        $helper->imageType = 'jpg';
        $helper->simple_header = false;
        $helper->_pagination = array(20);
        // Actions to be displayed in the "Actions" column
        $helper->actions = array('view', 'disable');

        $helper->identifier = 'id_product';
        $helper->show_toolbar = true;
        if (!$validated) {
            $helper->title = 'Neschválené produkty';
        } else {
            $helper->title = 'Schválené produkty';
        }

        if ($validated) {
            $helper->table = $this->name . '_viewed';
        } else {
            $helper->table = $this->name . '_categories';
        }
        $prefixName = $helper->table;

        $additional_query = "";
        if ($validated) {
            $additional_query = "viewSTLuploader_validatedproducts=''";
        }
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $current_index = "0";
        if (Tools::getValue($prefixName . 'current_index')) {
            $current_index = Tools::getValue($prefixName . 'current_index');
        }
        $helper->currentIndex = "?controller=AdminModules&configure=" . $this->name . "&" . $prefixName .  'current_index=' . $current_index . "&" . $additional_query;
        $orderByCustom = "time_stamp";
        $orderWayCustom = "DESC";
        if (Tools::getValue($prefixName . "Orderby")) {
            if (Tools::getValue($prefixName . "Orderby") == "user" || Tools::getValue($prefixName . "Orderby") ==  "notification_admin") {
                if (Tools::getValue($prefixName . "Orderby") == "user") {
                    $orderByCustom = "id_user";
                    $orderWayCustom = Tools::getValue($prefixName . "Orderway");
                }
            }
        }

        $product_ids_to_review = $this->getFromTable("user_product_validation", "$orderByCustom $orderWayCustom", "*", "validated=" . intval($validated));

        $main_category_id = $this->getCategoryId();
        $productCore = new Category($main_category_id);
        $orderBy = "id_product";
        $orderWay = "DESC";
        if (Tools::getValue($prefixName . "Orderby")) {
            if (!in_array(Tools::getValue($prefixName . "Orderby"), ["user", "notification_admin", "sold", "owned", "profitmargin", "profit"])) {
                if (Tools::getValue($prefixName . "Orderby") != "price_tax_exc") {
                    $orderBy = Tools::getValue($prefixName . "Orderby");
                    $orderWay = Tools::getValue($prefixName . "Orderway");
                }
            }
        }
        $products_without_filter = $productCore->getProducts((int) $this->context->language->id, 1, 1000, $orderBy, $orderWay, false, false, false, 0, false, $this->context);

        /*getting products*/

        $ids_array = [];
        $notification_array = [];
        $userid_array = [];

        foreach ($product_ids_to_review as $product) {
            array_push($ids_array, $product["id_product"]);
            $notification_array[$product["id_product"]] = $product["notification_admin"];
            $userid_array[$product["id_product"]] = $product["id_user"];
        }
        if (!function_exists('afterfilters')) {
            function afterfilters($product, $params)
            {

                $user = $params["getFromTable"]("customer", "id_customer", "firstname, lastname", "id_customer=" . $params["userid_array"][intval($product["id_product"])]);
                $product["user"] = $user[0]["firstname"] . " " . $user[0]["lastname"];
                $product["notification_admin"] = number_format($params["notification_array"][intval($product["id_product"])], 0);
                return $product;
            }
        }
        if (!function_exists('getFromTable')) {
            function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
            {
                $sql = new DbQuery();
                $sql->select("$whatToget");
                $sql->from("$tableName");
                $sql->where("$condition");
                $sql->orderBy("$orderBy");
                return Db::getInstance()->executeS($sql);
            }
        }

        $additional_parameters = [
            "notification_array" => $notification_array,
            "userid_array" => $userid_array,
            "getFromTable" => "getFromTable",
            "product_info" => $product_ids_to_review
        ];

        $product_filters = [
            [
                "name" => "id_product",
                "data" => $ids_array,
                "multiple" => true,
                "specific" => true
            ],
            [
                "name" => "active",
                "data" => $validated,
                "multiple" => false,
                "specific" => true
            ]
        ];
        $product_second_filters = [];
        if (Tools::isSubmit("submitFilter") && Tools::getValue("submitFilterSTLuploader_viewed")) {

            array_push($product_filters, [

                "name" => "id_product",
                "data" => Tools::getValue($prefixName . "Filter_id_product"),
                "multiple" => false,
                "specific" => false

            ]);
            array_push($product_filters, [

                "name" => "name",
                "data" => Tools::getValue($prefixName . "Filter_name"),
                "multiple" => false,
                "specific" => false

            ]);

            array_push($product_filters, [

                "name" => "price_tax_exc",
                "data" => Tools::getValue($prefixName . "Filter_price_tax_exc"),
                "multiple" => false,
                "specific" => false

            ]);
            array_push($product_filters, [

                "name" => "price",
                "data" => Tools::getValue($prefixName . "Filter_price"),
                "multiple" => false,
                "specific" => false

            ]);
            array_push($product_second_filters, [

                "name" => "user",
                "data" => Tools::getValue($prefixName . "Filter_user"),
                "multiple" => false,
                "specific" => false

            ]);
            array_push($product_second_filters, [
                "name" => "notification_admin",
                "data" => Tools::getValue($prefixName . "Filter_notification_admin"),
                "multiple" => false,
                "specific" => false

            ]);
        } elseif (Tools::isSubmit("submitFilter") && !Tools::getValue("submitFilterSTLuploader_viewed")) {
            array_push($product_filters, [

                "name" => "id_product",
                "data" => Tools::getValue($prefixName . "Filter_id_product"),
                "multiple" => false,
                "specific" => false

            ]);
            array_push($product_filters, [

                "name" => "name",
                "data" => Tools::getValue($prefixName . "Filter_name"),
                "multiple" => false,
                "specific" => false

            ]);

            array_push($product_filters, [

                "name" => "price_tax_exc",
                "data" => Tools::getValue($prefixName . "Filter_price_tax_exc"),
                "multiple" => false,
                "specific" => false

            ]);
            array_push($product_filters, [

                "name" => "price",
                "data" => Tools::getValue($prefixName . "Filter_price"),
                "multiple" => false,
                "specific" => false

            ]);
            array_push($product_second_filters, [

                "name" => "user",
                "data" => Tools::getValue($prefixName . "Filter_user"),
                "multiple" => false,
                "specific" => false

            ]);
            array_push($product_second_filters, [
                "name" => "notification_admin",
                "data" => Tools::getValue($prefixName . "Filter_notification_admin"),
                "multiple" => false,
                "specific" => false

            ]);
        }
        if (!function_exists('secondafterfilters')) {
            function secondafterfilters($product, $params)
            {
                $product_class = new STLStatsProduct($product["id_product"]);
                $product["sold"] = strval(round($product_class->getNbOfSales($product["id_product"]))) . "x";

                foreach ($params["product_info"] as $key => $product_info) {
                    if ($product["id_product"] == $product_info["id_product"]) {

                        $owned = (floatval($product_info["owned"]) == 1) ? "Vlastněno" : 'Nevlastněno';
                        $product["owned"] = $owned;
                        $product["profit"] = round((floatval($product_class->getSalesTotal()) * floatval($product_info["cut"])), 2);
                        $product["profitmargin"] =  round(floatval($product_info["cut"]) * 100);
                    }
                }


                $user = $params["getFromTable"]("customer", "id_customer", "firstname, lastname", "id_customer=" . $params["userid_array"][intval($product["id_product"])]);
                $product["user"] = $user[0]["firstname"] . " " . $user[0]["lastname"];
                $product["notification_admin"] = number_format($params["notification_array"][intval($product["id_product"])], 0);
                return $product;
            }
        }
        $products_filtered = $this->filterArray($product_filters, $products_without_filter, 'afterfilters', $additional_parameters);
        $products_filtered = $this->filterArray($product_second_filters, $products_filtered, 'secondafterfilters', $additional_parameters);

        if (in_array(Tools::getValue($prefixName . "Orderby"), ["user", "notification_admin", "sold", "owned", "profitmargin", "profit"])) { //sorting out of sql 
            usort($products_filtered, function ($a, $b) use (&$prefixName) {
                $modifier = 1;
                if (Tools::getValue($prefixName . "Orderway") == "desc") {
                    $modifier = -1;
                }

                return strcmp($a[Tools::getValue($prefixName . "Orderby")], $b[Tools::getValue($prefixName . "Orderby")]) * $modifier;
            });
        }
        if (count($products_without_filter) === 0) {
            return $helper->generateList($products_filtered, []);
        }
        foreach ($products_filtered as $key => $val) {
            $image = Image::getCover($val['id_product']);
            $products_filtered[$key]['image'] = $image['id_image'];
            $products_filtered[$key]['id_image'] = $image['id_image'];
        }
        $current_index = intval(Tools::getValue($prefixName . 'current_index'));
        $this->context->smarty->assign(array(
            "num_ofpages" => ceil(count($products_filtered) / $pagination_length),
            "current_index" => $current_index + 1,
            "query" => '&configure=' . $this->name . "&" . $additional_query,
            "prefix" => $prefixName,
            'token' => Tools::getAdminTokenLite('AdminModules'),
        ));
        $products_filtered = array_slice($products_filtered, $current_index  * $pagination_length, $pagination_length);
        $pagination = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/pagination.tpl');
        return $helper->generateList($products_filtered, $this->fields_list) . $pagination;
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");

        return Db::getInstance()->executeS($sql);
    }
    protected function getAllCategories()
    {
        $categoriesTmp = Category::getAllCategoriesName($this->getCategoryId(), $this->context->language->id);
        $categories = [];
        $id_main = $this->getCategoryId();
        foreach ($categoriesTmp as $category) {
            if ($category["id_category"] !== $id_main) {
                array_push(
                    $categories,
                    $category["id_category"]
                );
            }
        }
        return $categories;
    }
    function filterArray($filter_array, $array, $after_filters = null, $additional_parameters = null)
    /*filter_array
            main array operation "AND", subfilters (multiple option) operation "OR"
            [
                [
                    "name" => string "parameter of object",
                    "data" => string | array "data to compare"
                    "multiple" => boolean "if data is an array or single variable"
                    "specific" => if the data string is what you are searching for this needs to be enabled if the string you are searching for is incomplete disable this

                ]
            ]
        add to object [

        ]
        */
    {

        $result = [];
        foreach ($array as $element) {

            $pass = true;
            foreach ($filter_array as $filter) {


                if ($filter["multiple"]) {

                    $subpass = false;
                    foreach ($filter["data"] as $filter_multiple) {
                        if (!$pass) {

                            break;
                        }
                        if ($filter["specific"]) {
                            if ($filter_multiple == $element[$filter["name"]]) {

                                $subpass = true;
                            }
                        } else {
                            if (strpos(strval($element[$filter["name"]]), strval($filter_multiple))) {
                                $subpass = true;
                            }
                        }
                    }
                    if (!$subpass) {

                        $pass = false;
                        break;
                    } else {
                    }
                } else {
                    if ($filter["specific"]) {

                        if (!$filter["data"] == $element[$filter["name"]]) {

                            $pass = false;
                        }
                    } else {

                        if (strlen(strval($filter["data"]))) {
                            if (preg_match("/" . strval($filter["data"]) . "/i", strval($element[$filter["name"]])) == 0) {
                                $pass = false;
                            }
                        }
                    }
                }
            }
            if ($pass) {

                $element = $after_filters($element, $additional_parameters);
                array_push($result, $element);
            }
        }

        return $result;
    }

    /**
     * Save form data.
     */
    public function getCategoryId()
    {
        $categories = $this->getFromTable("category_lang", "id_category", "id_category", "name='3D soubory'");
        if (count($categories) == 0) {
            $category_id = new CategoryId(10);
            throw new CategoryNotFoundException($category_id, "Vytvořte novou kategorii se jménem '3D soubory'");
        }
        return $categories[0]['id_category'];
    }
    /*public function getHomeCategoryId()
    {
        $categories = $this->getFromTable("category_lang", "id_category", "id_category", "name='Domů'");
        if (count($categories) == 0) {
            $category_id = new CategoryId(10);
            throw new CategoryNotFoundException($category_id, "Vytvořte novou kategorii se jménem 'Domů'");
        }
        return $categories[0]['id_category'];
    }*/


    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJquery();


        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }
    public function getPath()
    {
        return $this->_path;
    }

    public function hookActionAdminControllerSetMedia()
    {
        // Adds jQuery and some it's dependencies for PrestaShop
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJquery();

            $this->context->controller->addCSS('https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/ui/trumbowyg.min.css');
            $this->context->controller->addJS('https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/trumbowyg.min.js');
            $this->context->controller->addJS($this->_path . 'views/js/dropzone.min.js');
            $this->context->controller->addCSS('https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
            $this->context->controller->addJS('https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js');
            $this->context->controller->addCSS($this->_path . 'views/css/dropzone.css');

            $this->context->controller->addCSS($this->_path . 'views/css/tagger.css');
            $this->context->controller->addJS($this->_path . 'views/js/tagger.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
        }
    }

    public function hookActionObjectOrderAddAfter($params)
    {
        /* Place your code here. */
        //$this->logger->info('Hook action customer account add fired');
        // print_r($params);
        //die();
    }

    public function hookActionPaymentConfirmation()
    {
        /* Place your code here. */
    }
    public function ajaxProcess()
    {
    }

    public function hookActionValidateOrder($params)
    {

        $cart = $params["cart"];
        $products = $cart->getProducts();
        $product_ids = array();
        foreach ($products as $product) {

            array_push($product_ids, (int)$product['id_product']);
        }
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('user_creation');
        $sql->where('1 = 1');
        $sql->orderBy('id_product');
        $productDatabase = Db::getInstance()->executeS($sql);
        foreach ($product_ids as $id) {
            if (in_array($id, $product_ids)) {
                $num_sold = 0;
                foreach ($productDatabase as  $product_data) {
                    if (intval($product_data["id_product"]) === $id) {
                        $num_sold = $product_data["num_sold"] + 1;
                    }
                }
                $query = "UPDATE `" . _DB_PREFIX_ . "user_creation` SET num_sold=" . $num_sold . " WHERE id_product=" . $id . ";";

                Db::getInstance()->Execute($query);
            }
        }
    }

    public function CreateProductProposal($ean13, $ref, $name, $qty, $text, $features, $price, $images, $catDef, $catAll, $shortDescription, $keywords, $files)
    {
        function deleteDirectory($dir)
        {
            if (!file_exists($dir)) {
                return true;
            }

            if (!is_dir($dir)) {
                return unlink($dir);
            }

            foreach (scandir($dir) as $item) {
                if ($item == '.' || $item == '..') {
                    continue;
                }

                if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                    return false;
                }
            }

            return rmdir($dir);
        }
        $product = new Product();              // Create new product in prestashop
        $product->ean13 = $ean13;
        $product->reference = substr($ref, 0, 30);;
        $product->name = $this->createMultiLangField($name);
        $product->description = $text;
        $product->id_category_default = $catDef;
        $product->redirect_type = '301';
        $product->price = number_format($price, 6, '.', '');
        $product->minimal_quantity = 1;
        $product->show_price = 1;
        $product->on_sale = 0;
        $product->online_only = 0;
        $product->active = 0;
        $product->meta_description = htmlspecialchars($shortDescription);
        $product->description_short = htmlspecialchars($shortDescription);
        $product->link_rewrite = $this->createMultiLangField(Tools::str2url($name)); // Contribution credits: mfdenis
        $product->meta_keywords = $keywords;
        $product->is_virtual = 1;
        $product->add();                        // Submit new product
        StockAvailable::setQuantity($product->id, null, $qty); // id_product, id_product_attribute, quantity
        $product->addToCategories($catAll);     // After product is submitted insert all categories

        // Insert "feature name" and "feature value"
        if (is_array($features)) {
            foreach ($features as $feature) {
                $attributeName = $feature['name'];
                $attributeValue = $feature['value'];

                // 1. Check if 'feature name' exist already in database
                $FeatureNameId = Db::getInstance()->getValue('SELECT id_feature FROM ' . _DB_PREFIX_ . 'feature_lang WHERE name = "' . pSQL($attributeName) . '"');
                // If 'feature name' does not exist, insert new.
                if (empty($getFeatureName)) {
                    Db::getInstance()->execute('INSERT INTO `' . _DB_PREFIX_ . 'feature` (`id_feature`,`position`) VALUES (0, 0)');
                    $FeatureNameId = Db::getInstance()->Insert_ID(); // Get id of "feature name" for insert in product
                    Db::getInstance()->execute('INSERT INTO `' . _DB_PREFIX_ . 'feature_shop` (`id_feature`,`id_shop`) VALUES (' . $FeatureNameId . ', 1)');
                    Db::getInstance()->execute('INSERT INTO `' . _DB_PREFIX_ . 'feature_lang` (`id_feature`,`id_lang`, `name`) VALUES (' . $FeatureNameId . ', ' . Context::getContext()->language->id . ', "' . pSQL($attributeName) . '")');
                }

                // 1. Check if 'feature value name' exist already in database
                $FeatureValueId = Db::getInstance()->getValue('SELECT id_feature_value FROM ' . _DB_PREFIX_ . 'feature_value WHERE id_feature_value IN (SELECT id_feature_value FROM `' . _DB_PREFIX_ . 'feature_value_lang` WHERE value = "' . pSQL($attributeValue) . '") AND id_feature = ' . $FeatureNameId);
                // If 'feature value name' does not exist, insert new.
                if (empty($FeatureValueId)) {
                    Db::getInstance()->execute('INSERT INTO `' . _DB_PREFIX_ . 'feature_value` (`id_feature_value`,`id_feature`,`custom`) VALUES (0, ' . $FeatureNameId . ', 0)');
                    $FeatureValueId = Db::getInstance()->Insert_ID();
                    Db::getInstance()->execute('INSERT INTO `' . _DB_PREFIX_ . 'feature_value_lang` (`id_feature_value`,`id_lang`,`value`) VALUES (' . $FeatureValueId . ', ' . Context::getContext()->language->id . ', "' . pSQL($attributeValue) . '")');
                }
                Db::getInstance()->execute('INSERT INTO `' . _DB_PREFIX_ . 'feature_product` (`id_feature`, `id_product`, `id_feature_value`) VALUES (' . $FeatureNameId . ', ' . $product->id . ', ' . $FeatureValueId . ')');
            }
        }

        try {

            /*Adding virtual files*/
            $virtual_product_filename = ProductDownload::getNewFilename();
            $filename = _PS_DOWNLOAD_DIR_ . "$virtual_product_filename";
            $this->Zip(_PS_UPLOAD_DIR_ . $this->context->customer->id, $filename);

            $is_shareable = 0;
            $virtual_product_nb_days = 0;
            $virtual_product_nb_downloable = 0;
            $virtual_product_expiration_date = new DateTime('2026-12-12');
            $virtual_product_expiration_date->modify('+21 year');
            $download = new ProductDownload((int) $product->id);
            $download->id_product = (int) $product->id;
            $download->display_filename = "$name.zip";
            $download->filename = $virtual_product_filename;

            $download->date_add = date('Y-m-d H:i:s');
            $download->date_expiration = $virtual_product_expiration_date->format("Y-m-d H:i:s");
            $download->nb_days_accessible = (int) $virtual_product_nb_days;
            $download->nb_downloadable = (int) $virtual_product_nb_downloable;
            $download->active = 1;
            $download->is_shareable = (int) $is_shareable;



            // add product image. moved to after download
            $cover = true;
            foreach ($images as $imgUrl) {

                $image = new Image();
                $image->id_product = $product->id;
                $image->position = 1;
                $image->cover = $cover; // or false;
                $imgName = $imgUrl;

                $id = $this->context->customer->id;
                $imgUrl = urldecode(_PS_UPLOAD_DIR_ . "$id/img/$imgUrl");
                if (($image->validateFields(false, true)) === true &&
                    ($image->validateFieldsLang(false, true)) === true && $image->add()
                ) {

                    $imageName = new stluploaderImagesClass();
                    $imageName->name = $imgName;
                    $imageName->id_image = $image->id;

                    $imageName->save();

                    if (AdminImportControllerOverride::copyImg($product->id, $image->id, $imgUrl, 'products', false)) {
                    } else {

                        $image->delete();
                    }
                }
                if ($cover) {
                    $cover = false;
                }
            }
            // moved to after all images and files are added
            deleteDirectory(_PS_UPLOAD_DIR_ . $this->context->customer->id);

            if ($download->save()) {

                $creation = new userProductValidationClass();
                $creation->id_product = (int) $product->id;
                $creation->id_user = (int) $this->context->customer->id;
                $creation->lastsawuser = '0000-00-00 00:00:00';
                $creation->lastsawadmin = '0000-00-00 00:00:00';
                $creation->validated = 0;
                $creation->owned = 0;
                $creation->cut = 0;
                $creation->notification_user = 0;
                $t = time();
                $creation->time_stamp = date("Y-m-d H:i:s", $t);
                $creation->notification_admin = 0;
                $creation->save();
                $product->save();
                $link = new Link;
                /*http_response_code(422);
                $url = $link->getAdminLink("AdminModules");
                $url = $url . '&configure=STLuploader&viewSTLuploader_proposals=' . $product->id;*/

                Mail::Send(
                    (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                    'adminnewproduct', // email template file to be use
                    'Nový produkt', // email subject
                    array(
                        '{heading}' => "Nový produkt",
                        '{message1}' => "Dobrý den, byl přidán nový produkt " . $product->name[$this->context->language->id] . " s ID: " . $product->id,
                        '{message2}' => "S krátkým popisem: " . $product->description_short,
                        '{message3}' => "Od autora <a href='mailto: " . $this->context->customer->email . "'>" . $this->context->customer->firstname . " " . $this->context->customer->lastname . " </a>"
                    ),
                    Configuration::get('PS_SHOP_EMAIL')/*Configuration::get('PS_SHOP_EMAIL')*/, // receiver email address
                    NULL, //receiver name
                    NULL, //from email address
                    NULL,  //from name,
                    NULL,
                    NULL,
                    _PS_MODULE_DIR_ . $this->name . '/mails/'
                );

                Mail::Send(
                    (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                    'adminnewproduct', // email template file to be use
                    "Produkt " . $product->name[$this->context->language->id] . " přidán ke schválení", // email subject
                    array(
                        '{heading}' => "Váš produkt " . $product->name[$this->context->language->id] . " (ID: " . $product->id . ") přidán ke schválení",
                        '{message1}' => "Dobrý den, Váš produkt jsme předali adminovi schvalovacímu procesu.",
                        '{message2}' => "V případě potíží či potřeby kontaktovat administrátora použijte tento <a href='mailto:" . Configuration::get('PS_SHOP_EMAIL') . "'>email</a>",
                        '{message3}' => "Nebo použijte zabudovaný chat na stránce detail <a href='https://www.allstl-shop.cz/module/STLuploader/creatorpage#/productdetail/" . $product->id . "'> produktu </a> <br> Hezký zbytek dne"
                    ),
                    $this->context->customer->email, // receiver email address
                    NULL, //receiver name
                    NULL, //from email address
                    NULL,  //from name,
                    NULL,
                    NULL,
                    _PS_MODULE_DIR_ . $this->name . '/mails/'
                );
            }
        } catch (Throwable $error) {
            echo "chyba ";
            echo $error;
        }
    }

    // copied from https://stackoverflow.com/questions/1334613/how-to-recursively-zip-a-directory-in-php
    function Zip($source, $destination)
    {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true) {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                } else if (is_file($file) === true) {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        } else if (is_file($source) === true) {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }

    protected static function createMultiLangField($field)
    {
        $res = [];
        foreach (Language::getIDs(false) as $id_lang) {
            $res[$id_lang] = $field;
        }

        return $res;
    }
    function clean($file)
    {
        $file = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $file);
        // Remove any runs of periods (thanks falstro!)
        $file = mb_ereg_replace("([\.]{2,})", '', $file);
        return $file;
    }


    public function renderWidget($hookName, array $configuration)
    {

        $userid = $this->context->customer->id;
        if ($userid) {
            $user_creator = boolval(count($this->getFromTable("stluploader_designers", "id_user", "id_user", "id_user=$userid")));
            $base_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
            $link_page = $base_link . '/module/STLuploader/creatorpage';
            $proposal_page = $base_link . '/module/STLuploader/proposal';

            if ($user_creator) {
                return '<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="history-link" href="' . $link_page . '">
                <span class="link-item">
                   <p><img alt="form by Jason Tropp from the Noun Project" src="/noun_form_87087.svg" width="42"></p>
                    Vytvoř nový produkt
                </span>
                </a>' . '<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="history-link" href="' . $link_page . "#/products" . '">
                <span class="link-item">
                   <p><img alt="form by Jason Tropp from the Noun Project" src="/noun_form_87087.svg" width="42"></p>
                    Seznam produktů
                </span>
                </a>' . '<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="history-link" href="' . $link_page . "#/payment" . '">
                <span class="link-item">
                   <p><img alt="form by Jason Tropp from the Noun Project" src="/noun_form_87087.svg" width="42"></p>
                    Statistika prodeje
                </span>
                </a>';
            } else {
                $proposal = $this->getFromTable("stluploader_registeringdesigners", "id_user", "validated", "id_user=$userid");
                $proposal_exists = boolval(count($proposal));
                if (isset($_COOKIE["stl_proposal_notification"])) {
                    if ($_COOKIE["stl_proposal_notification"] == 1) {
                        unset($_COOKIE['stl_proposal_notification']);
                        echo "<script>alert('Váše žádost byla odeslána. Prosím kontrolujte chat jestli vám Admin nenapsal, pokud budete schválen, přijde vám email.')</script>";
                    }
                }
                if ($proposal_exists) {
                    $text = "Kliknutím otevřete Chat.";
                    $this->context->smarty->assign(array(
                        "text" => $text,
                        "state" => ["Admin se zatím k vaší žádosti nedostal. Prosíme o strpení. Odpověd většinou přijde do 24H (pracovní dny).", "Admin vaši žádost zamítl. Pro více informací použijte chat", "Vaše žádost byla schválena. Prosím načtěte tuto stránku znovu"][$proposal[0]["validated"]]
                    ));
                    return $this->context->smarty->fetch($this->local_path . 'views/templates/front/buttonwithchat.tpl');
                } else {
                    return '<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="history-link" href="' . $proposal_page . '">
                    <span class="link-item">
                        <p><img alt="form by Jason Tropp from the Noun Project" src="/noun_form_87087.svg" width="42"></p>
                        Požádat o možnost vytvářet produkty
                    </span>
                    </a>';
                }
            }
        }
    }
    public function getWidgetVariables($hookName, array $configuration)
    {
    }
}

class AdminImportControllerOverride extends AdminImportControllerCore
{

    public static function copyImg($id_entity, $id_image = null, $url = '', $entity = 'products', $regenerate = true)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();

                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . (int) $id_entity;

                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . (int) $id_entity;

                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_ . (int) $id_entity;

                break;
            case 'stores':
                $path = _PS_STORE_IMG_DIR_ . (int) $id_entity;

                break;
        }

        $url = urldecode(trim($url));
        $parced_url = parse_url($url);

        if (isset($parced_url['path'])) {
            $uri = ltrim($parced_url['path'], '/');
            $parts = explode('/', $uri);
            foreach ($parts as &$part) {
                $part = rawurlencode($part);
            }
            unset($part);
            $parced_url['path'] = '/' . implode('/', $parts);
        }

        if (isset($parced_url['query'])) {
            $query_parts = [];
            parse_str($parced_url['query'], $query_parts);
            $parced_url['query'] = http_build_query($query_parts);
        }

        if (!function_exists('http_build_url')) {
            require_once _PS_TOOL_DIR_ . 'http_build_url/http_build_url.php';
        }

        $url = http_build_url('', $parced_url);

        $orig_tmpfile = $tmpfile;

        if (Tools::copy(urldecode($url), $tmpfile)) {
            // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);

                return false;
            }

            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize($tmpfile, $path . '.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5, $src_width, $src_height);
            $images_types = ImageType::getImagesTypes($entity, true);
            foreach ($images_types as $image_type) {
                ImageManager::resize($tmpfile, $path . '-' . stripslashes($image_type['name']) . '.jpg', $image_type['width'], $image_type['height']);
                if (in_array($image_type['id_image_type'], $watermark_types)) {
                    Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                }
            }
            if (false) {

                $previous_path = null;
                $path_infos = [];
                $path_infos[] = [$tgt_width, $tgt_height, $path . '.jpg'];
                foreach ($images_types as $image_type) {
                    $tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

                    if (ImageManager::resize(
                        $tmpfile,
                        $path . '-' . stripslashes($image_type['name']) . '.jpg',
                        $image_type['width'],
                        $image_type['height'],
                        'jpg',
                        false,
                        $error,
                        $tgt_width,
                        $tgt_height,
                        5,
                        $src_width,
                        $src_height
                    )) {


                        // the last image should not be added in the candidate list if it's bigger than the original image
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = [$tgt_width, $tgt_height, $path . '-' . stripslashes($image_type['name']) . '.jpg'];
                        }
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '_' . (int) Context::getContext()->shop->id . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '_' . (int) Context::getContext()->shop->id . '.jpg');
                            }
                        }
                    }
                    if (in_array($image_type['id_image_type'], $watermark_types)) {
                        Hook::exec('actionWatermark', ['id_image' => $id_image, 'id_product' => $id_entity]);
                    }
                }
            }
        } else {
            @unlink($orig_tmpfile);
            return false;
        }
        unlink($orig_tmpfile);

        return true;
    }
}
