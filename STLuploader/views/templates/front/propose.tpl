
{extends file='page.tpl'}

{block name='page_content'}
<script>
    var pictures = JSON.parse('{l|escape:"javascript" s="{$pictures}" d="Modules.STLuploader"}');
    var description = '{l|escape:"javascript" s="{$description}" d="Modules.STLuploader"}';
</script>
{foreach from=$scripts item=script}
<script src="{$script|escape:'htmlall':'UTF-8'}"></script>
{/foreach}
{foreach from=$styles item=style}
<link rel="stylesheet" type="text/css" href="{$style|escape:'htmlall':'UTF-8'}">
{/foreach}
<div class="page-propose">
    <h1 style="text-align: center; margin-bottom:20px">
        Napište svoje zkušenosti s designem a 3D tiskem
    </h1>
    {if strlen($error) > 1}
    <div class="alert alert-danger" role="alert">{$error}</div>
    {/if}
    <div style="margin-top:20px; margin-bottom:20px">
        <p>
            Nějaký návod nějaký návod Nějaký návod nějaký návod Nějaký návod nějaký návod Nějaký návod nějaký návodNějaký návod nějaký návodNějaký návod nějaký návodNějaký návod nějaký návod Nějaký návod nějaký návodNějaký návod nějaký návodvNějaký návod nějaký návodNějaký návod nějaký návodvvNějaký návod nějaký návodv v vNějaký návod nějaký návodv Nějaký návod nějaký návodNějaký návod nějaký návodv
        </p>
    </div>
    <div>
        <form action="" id="form_propose" method="post">
            <textarea id="description">testtesttest</textarea>
            <h3 class="p-1">{l s='Nahrajte fotky (minimálně 3, maximálně 10)' mod='STLuploader'}</h3>
            <div class="dropzone" action="/image-upload" id="image-upload"></div>
            <button type="submit" class="btn btn-danger" name="submitproposal" style="margin-top: 20px">
            Odeslat
            </button>
        </form>
    </div>
</div>

{/block}
