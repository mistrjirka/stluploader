<a class='col-lg-4 col-md-6 col-sm-6 col-xs-12' style='cursor: pointer; ' id='history-link' data-toggle="modal" data-target="#chatModal">
    <span class='link-item'>
    <p>
      <img alt="form by Jason Tropp from the Noun Project" src="/noun_form_87087.svg" width="42">
    </p>
         {$text} 
    </span>
</a>
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Chat</h4>
      </div>
      <div class="modal-body">
        <div class="proposal-chat">
            <div class="proposal-messages">
                <ul class="proposal-message-list">
                
                </ul>
                <div class="proposal-message-input">
                <input type="text" id="messageinputproposal" placeholder="Napište zprávu..." />
                <button type="button" id="btnsendproposal" class="btn">Odeslat</button>
                </div>
            </div>
            </div>
        </div>
      </div>
     
    </div>
  </div>
<script>
var state = "{$state}"

</script>
