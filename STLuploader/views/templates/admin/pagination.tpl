<nav>
    <ul class="pagination">
        <li {if $current_index eq 1} class="disabled"{/if} >
        
            <a href="?controller=AdminModules&configure=STLuploader&token={$token}&{$prefix}current_index={$current_index - 2}{$query}" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
    
            </li>
        {for $pagination=1 to $num_ofpages}
            <li {if $current_index == $pagination} class="active"{/if} >

                <a href="?controller=AdminModules&configure=STLuploader&token={$token}&{$prefix}current_index={$pagination - 1}{$query}">{$pagination}{if $current_index == $pagination}<span class="sr-only">(current)</span>{/if}</a>
            </li>
        {/for}
            
            
        <li {if $num_ofpages == 1 and $current_index == $num_ofpages} class="disabled"{/if}>
            <a href="?controller=AdminModules&configure=STLuploader&token={$token}&{$prefix}current_index={$current_index}{$query}" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>