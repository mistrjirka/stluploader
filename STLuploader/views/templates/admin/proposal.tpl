<div id="loader" class="loader-div">
  <span class="loader">
    <span></span>
    <span></span>
  </span>
</div>
<div class="panel">
<h3>Motivační dopis</h3>
<div class="row spacebetween-div">
  <div class="col-sm-6">
     <a href="mailto:{$customeremail}">
     {$customeremail}
     </a>
  </div>
  <div class="col-sm-6">
    {$customer}
  </div>
</div>
<div class="motivationLetter">

{$message}
</div>
<div class="row">
  {foreach from=$pictures item=picture}
    <div class="col-md-6 col-lg-4">
      <img src="{$picture}" class="stl-image-respozive"> 
    </div>
  {/foreach}
</div>
<div class="stl-section">
  <h3>Stav</h3>
  <p>
  <b>{$state}</b>
</div>
</p>
<div class="row button-row"> 
 <button   id="acceptProposal" class="btn btn-primary col-sm-3">{l s='Accept' mod='STLuploader'}</button>
 <button   id="denyProposal" class="btn btn-danger col-sm-3">{l s='Deny' mod='STLuploader'}</button>
 </div>
 {if $validated}
   
 <div>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Za součástné výplatní období ({$from} do {$to})</th>
        <th scope="col">Za minulé výplatní období ({$from_before} do {$to_before})</th>
        <th scope="col">Za celou dobu</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">Prodaných kusů</th>
        <td>{$sold_month}</td>
        <td>{$last_sold_month}</td>
        <td>{$sold_total}</td>
      </tr>
      <tr>
        <th scope="row">Výdělek celkový</th>
        <td>{round($sales_month, 2)} Kč</td>
        <td>{round($last_sales_month, 2)} Kč</td>
        <td>{round($sales_total, 2)} Kč</td>
        
      </tr>
      <tr>
        <th scope="row">Skutečný výdělek Pro obchod</th>
        <td>{round($sales_month_fee_shop, 2)} Kč</td>
        <td>{round($last_sales_month_fee_shop, 2)} Kč</td>
        <td>{round($sales_total_fee_shop, 2)} Kč</td>
      </tr>
      <tr>
        <th scope="row">Výdělek Pro Designera</th>
        <td>{round($sales_month_fee_designer, 2)} Kč</td>
        <td>{round($last_sales_month_fee_designer, 2)} Kč</td>
        <td>{round($sales_total_fee_designer, 2)} Kč</td>
      </tr>
    </tbody>
  </table>
 </div>
 {/if}

 <div class="proposal-chat">
      <div class="proposal-messages">
        <ul class="proposal-message-list">
          
        </ul>

        <div class="proposal-message-input">
          <input type="text" id="messageinputproposal" placeholder="Napište zprávu..." />

          <button type="button" id="btnsendproposal" class="btn">{l s='Odeslat' mod='STLuploader'}</button>
        </div>
      </div>
    </div>
</div>
<script>
var sendmsgUrl = "{$sendmsgUrl}";
var getmsgUrl = "{$getmsgUrl}";
var acceptUrl = "{$acceptUrl}";
var denyUrl = "{$denyUrl}";
var backredirectURL = "{$backredirectURL}"
</script>