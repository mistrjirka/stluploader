<div id="loader" class="loader-div">
  <span class="loader">
    <span></span>
    <span></span>
  </span>
</div>
<div class="panel">
<h3> Autor</h3>
<p style="padding-bottom: 15px"><a href="{$author_link}">{$author}</a></p>
<h3> Obrázky </h3>
<div class="dropzone" action="/file-upload" id="image-editor"></div>
<h3 style="margin-top:30px"> soubory </h3>
<div class="dropzone" action="/object-upload" id="file-editor"></div>

<form id="product_form"  method="GET">

<div class="row"> 
<div class="col">
 <div class="form-group">
    <label for="productInputName">{l s='Product name' mod='STLuploader'}</label>
    <input type="text" class="form-control" id="productInputName" name="name" value="{$product->name[1]}" placeholder="{l s='Product name' mod='STLuploader'}">
  </div>
  <div class="form-group">
    <label for="InputPrice">{l s='Price' mod='STLuploader'}</label>
    <div class="input-group">
      <div class="input-group-addon">{l s='CZK' mod='STLuploader'}</div>
      <input name="price" type="number" min="10" step="1" value="{round($product->price,2)}" id="InputPrice" class="form-control" placeholder="Price">
    </div>
  </div>
  <div class="form-group">
    <label for="productInputShortDescription">{l s='Short description' mod='STLuploader'}</label>
    <input type="textarea" class="form-control" name="shortDescription" value="{$product->meta_description[1]}" id="productInputShortDescription" placeholder="{l s='Short description' mod='STLuploader'}">
  </div>

  <div class="form-group">
      <label for="description">{l s='Description' mod='STLuploader'}</label>

    <div id="description"></div>
   
  </div>
 <div class="form-group">
    <label  for="keyWordsEditor">{l s='Keywords' mod='STLuploader'}</label>
    <input type="text" class="form-control" id="keyWordsEditor" name="keywords" value="{$product->meta_keywords[1]}" placeholder="{l s='Keywords' mod='STLuploader'}">
  </div>
  
  <div class="form-group">
    <h3> Kategorie </h3>
<select id="categories" name="categories[]" multiple="multiple">
  {foreach from=$categories item=category}
          <option value="{$category['id']}" {if $category['selected']}selected="selected"{/if}>{$category['name']}</option>
          
    {/foreach}
</select>
        

  </div>
  <div class="form-group">
    <h3> Podíl (0 - žádný poplatek, 100 - vše jde eshopu)</h3>
    <div class="slidecontainer">
      <input type="range" min="0" max="100" value="{$cut}" name="cut" class="slider" id="cut">
    </div>
    <p>Podíl: <span id="cut_info">{$cut}</span>%</p>
  </div>
  <div class="form-check">
    <input type="checkbox" class="form-check-input" name="validated" id="validated"{if $product->active} checked {/if}>
    <label class="form-check-label" for="validated">Schválit?</label>
  </div>
  <div class="form-check">
    <input type="checkbox" class="form-check-input" name="owned" id="owned"{if $owned} checked {/if}>
    <label class="form-check-label" for="owned">Odkoupeno od {$author}</label>
  </div>

</div>

</div>
</form>
<div class="row button-row"> 
 <button   id="editProduct" class="btn btn-primary col-sm-3">{l s='Save' mod='STLuploader'}</button>
 {if !$product->active}
 <button   id="activateProduct" class="btn btn-success col-sm-3">{l s='Activate' mod='STLuploader'}</button>
 {/if}
 <button onclick="return confirm('Jste si jistí že chcete tento produkt úplně odstranit?')" id="removeProduct" class="btn btn-danger col-sm-2 ">{l s='Delete' mod='STLuploader'}</button>
 <a style="margin-left:17px;" href="{$redirect_product}" id="redirectProduct" class="btn btn-primary col-sm-3">{l s='Produktová stránka' mod='STLuploader'}</a>
 </div>
 
  <div>
    <h3>{l s='Statistiky' mod='STLuploader'}</h3>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Za poslední měsíc</th>
          <th scope="col">Za celou dobu existence</th>
          
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Prodaných kusů</th>
          <td>{$num_sold_lastmonth}</td>
          <td>{$num_sold}</td>

        </tr>
        <tr>
          <th scope="row">Výdělek celkový</th>
          <td>{$sales_lastmonth} Kč</td>
          <td>{$sales_total} Kč</td>
          
        </tr>
        <tr>
          <th scope="row">Výdělek pro obchod</th>
          <td>{round($sales_lastmonth * ($cut/100),2)} Kč</td>
          <td>{round($sales_total * ($cut/100), 2)} Kč</td>
        </tr>
      </tbody>
    </table>
  </div>
  
    <div class="stl-chat">
      <div class="stl-messages">
        <ul class="stl-message-list">
          
        </ul>
        <div class="stl-message-input">
          <input type="text" id="messageinput" placeholder="Napište zprávu..." />
          <button type="button" id="btnsend" class="btn">Send</button>
        </div>
      </div>
    </div>
<script>
var description = '{l|escape:"javascript" s="{$product->description[1]}" d="Modules.STLuploader"}';
var uploadURL = "{$uploadURL}";
var deleteURL = "{$deleteURL}";
var getMessagesURL = "{$getmessagesURL}";
var sendMessagesURL = "{$sendmessagesURL}"
var uploadFileURL = "{$uploadFileURL}";
var deleteFileURL = "{$deleteFileURL}";
var editProductURL = "{$editProductURL}";
var removeProductURL = "{$removeProductURL}";
var activateProductURL = "{$activateProductURL}";
var prefix = "{$securePrefix}";
var targetfile = "{$targetPath}";
var images = {$images};
var files = {$files};
var backredirectURL = "{$backredirectURL}"
</script>

</div>