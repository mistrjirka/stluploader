/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
try {

    (() => {
            var msgUrl = "/module/STLuploader/messageproposal";
            var previousMessages = [];

            var messages = document.querySelector('.proposal-message-list');

            var btn = document.getElementById('btnsendproposal');
            var input = document.getElementById('messageinputproposal');
            // Button/Enter Key
            btn.addEventListener('click', () => {
                if(input.value.length > 0)
                    sendMessage();
            });
            input.addEventListener('keyup', function (e) {
                if (e.keyCode == 13) 
                    sendMessage();
                


            })

            // Messenger Functions
            function sendMessage() {
                var msg = input.value;
                input.value = '';
                $.get({
                    url: msgUrl,
                    data: {
                        action: "sendmessage",
                        message: msg
                    },
                    success: () => {
                        writeLine(msg);
                        previousMessages.push({message: msg, author: "admin"});
                        getMessages();

                    },
                    error: (err) => {
                        alert("Nastala chyba při posílání zprávy")
                    }

                });
            }
            function addMessage(e) {
                var msg = e.data ? JSON.parse(e.data) : e;
                writeLine(`${
                    msg.FROM
                }: ${
                    msg.MESSAGE
                }`);
            }
            function writeLine(text) {
                var message = document.createElement('li');
                message.classList.add('proposal-message-item', 'item-secondary');
                message.innerHTML = 'Já: ' + text;
                messages.appendChild(message);
                messages.scrollTop = messages.scrollHeight;
            }
            function writeLineCustomer(text, author) {
                var message = document.createElement('li');
                message.classList.add('proposal-message-item', 'item-primary');
                message.innerHTML = author + ': ' + text;
                messages.appendChild(message);
                messages.scrollTop = messages.scrollHeight;
            }
            function removeAll() {
                $(messages).empty();
            }
            function getMessages() {
                $.post({
                        url: msgUrl,
                        data: {
                            action: "getmessages"
                        },
                        success: (data) => {

                            if (typeof data === "string") {
                                var regex = /-->.*?<\!--/g;
                                var regex2 = /<\!--.*?-->/g;
                                data = data.replace(regex, '').replace(regex2, '').replace(/.*?(\[.*\]).*/, "$1");
                                console.log(data);
                                data = JSON.parse(data);
                            }
                            if (data.length > 0) {
                                if (previousMessages.length == 0) {
                                    removeAll();
                                }
                                if (data.length > previousMessages.length) {
                                    $("#chatModal").modal('show');
                                }
                                data.forEach((message, index) => {
                                    if (previousMessages.length - 1 < index) {
                                        if (message.you) {
                                            writeLine(message.message);
                                        } else {
                                            writeLineCustomer(message.message, message.author);
                                        }
                                    }
                                });
                                    previousMessages = data;

                            }
                        }, error : (err) => {
                        alert("Nastala chyba při získávání zpráv")
                    }}
            );
        }
        setInterval(getMessages, 3000);
        getMessages();
        writeLineCustomer(state, "info");
    }
)();} catch (error) {}
