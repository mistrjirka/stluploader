/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediatly.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
Dropzone.autoDiscover = false;
var count = 0;
var filesToSend = [];
window.onload = () => {
    var cover = false;
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }
    $('#description').trumbowyg({lang: 'cs'});
    $('#description').trumbowyg('html', description);
    var keywords = tagger(document.querySelector('[name="keywords"]'), {
        allow_duplicates: false,
        allow_spaces: true,
        add_on_blur: true,
        tag_limit: 10

    });

    (function () {
        var myDropzone = $("div#image-editor").dropzone({
            drop_element: "image-editor",
            url: uploadURL,
            addRemoveLinks: true,
            dictRemoveFile: "Odstranit soubor",
            acceptedFiles: 'text/plain,text/*, application/sla, model/stl, application/wavefront-obj, application/vnd.ms-package.3dmanufacturing-3dmodel+xml, application/vnd.ms-printing.printticket+xml, application/x-openscad, application/x-extension-fcstd, image/x-dwg, application/solids, application/step, application/STEP,  application/dxf, application/iges, text/x-fortran, application/octet-stream',
            addedfile: (file) => {
                if (this.files.length) {
                    var _i, _len;
                    for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
                    {
                        if(this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString())
                        {
                            this.removeFile(file);
                        }
                    }
                }
            },
            removedfile: (file) => {
                x = confirm('chcete skutečně soubor smazat?' + file.id);
                if (!x)
                    return false;
                else {
                    file.previewElement.remove();

                    $.ajax({

                        url: deleteURL,
                        type: 'GET',
                        data: {
                            'id': file.id
                        },
                        dataType: 'json',
                        success: function (data) {
                            alert('Úspěch!');
                        },
                        error: function (request, error) {
                            alert("Nastala chyba, toto pošlete administrátorovi: " + JSON.stringify(request));
                        },
                        dataType: "application/javascript"
                    });

                    return true;
                }
            },
            init: function () {
                thisDropzone = this;
                thisDropzone.options.thumbnailHeight = 120;
                thisDropzone.options.thumbnailMethod = "contain";
                var mockFile = {
                    name: images.cover.name,
                    size: images.cover.size,
                    id: images.cover.id
                }
                if (images.cover.id == null) {
                    cover = true;
                } else {
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, images.cover.url);
                }
                console.log(images.others);
                if (images.others.length > 0) {
                    images.others.forEach((value, key) => {

                        var mockFile = {
                            name: value.name,
                            size: value.size,
                            id: value.id

                        };
                    
                    
                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, value.url);
                    
                    });
                }
                this.on("sending", function (file, xhr, formData) {

                    formData.append("product_id", decodeURIComponent($.urlParam('id_product')));
                    formData.append("cover", cover);
                    if (cover) cover = false;
                });
                this.on("success", function (file, responseText) {
                    console.log(responseText);
                });


            },
            thumbnailMethod: "contain",
            thumbnailHeight: 120
        });
    })();
    (function () {

    var myDropzone2 = new Dropzone(document.getElementById("file-editor"),{
        drop_element: "file-editor",
        url: uploadFileURL,
        addRemoveLinks: true,
        dictRemoveFile: "Odstranit soubor",
        dataType: "application/javascript",
        removedfile: (file) => {
                x = confirm('chcete skutečně soubor smazat?' + file.name);
                if (!x)
                    return false;
                else {
                    file.previewElement.remove();

                    $.ajax({

                        url: deleteURL,
                        type: 'GET',
                        data: {
                            'name': file.name
                        },
                        dataType: 'json',
                        success: function (data) {
                            alert('Úspěch!');
                        },
                        error: function (request, error) {
                            alert("Nastala chyba, toto pošlete administrátorovi: " + JSON.stringify(request));
                        },
                        dataType: "application/javascript"
                    });

                    return true;
                }
            },
        init: function() {
            let thisDropzone2 = this;
            console.log(this);
            if (files.length > 0) {
                files.forEach((file) => {
                    var mockFile = {
                        name: file.name,
                        size: file.size
                    }
                    thisDropzone2.options.addedfile.call(thisDropzone2, mockFile);
                    filesToSend.push(file.name);

                });
            }
            


        },
        thumbnailMethod: "contain",
        thumbnailHeight: 120
    });
    })();
    $('#image-editor .dz-details').each(function (index, element) {
        (function (index) {
            $(element).attr('id', "filename_" + index);
            var selectFile = document.getElementById("image-editor").querySelector("#filename_" + index);
            selectFile.addEventListener("click", function () {

                window.open(window.location.origin + "/" + $('#image-editor #filename_' + index + '> div > [data-dz-name=""]').text().replace('.jpg', '') + "/" + $('#image-editor #filename_' + index + '> div > [data-dz-name=""]').text());
            });
        }(index));
    });
    $('#file-editor .dz-details').each(function (index, element) {
        (function (index) {
            $(element).attr('id', "filename_" + index);
            var selectFile = document.getElementById("file-editor").querySelector("#file-editor #filename_" + index);
            selectFile.addEventListener("click", function () {

                window.open(window.location.origin + "/upload/" + prefix +"/"+ $('#file-editor #filename_' + index + '> div > [data-dz-name=""]').text())
            });
        }(index));
    });


};
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};
/*
            const pathnameArray = window.location.pathname.split('/');

 $.ajax({
                url: window.location.origin + "/" + pathnameArray[1] + "/modules/STLuploader/getimages",
                type: "GET",
                cache: false,
                data: jQuery.param(
                    {
                        _token: secureToken,
                        product_id: getUrlParameter('id_product'),
                    }
                ),
                success: (data) => {
                    alert(data);
                    console.log(data);
                    $.each(data, function (key, value) {

                        var mockFile = {
                            name: value.name,
                            size: value.size,
                            
                        };

                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);

                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "uploads/" + value.name);

                    });

                },
                error: (err) => {
                    console.log(err);
                }

            });*/
