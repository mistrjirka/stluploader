/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediatly.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
Dropzone.autoDiscover = false;
var count = 0;
var stlFilesToSend = [];
var images_name = [];
var validated = 0;
var readyImages = true;
var readyFiles = true;
window.onload = () => {
    var cover = false;
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }


    try {
        $('#description').trumbowyg({lang: 'cs'});
        $('#description').trumbowyg('html', description);
        var keywords = tagger(document.querySelector('[name="keywords"]'), {
            allow_duplicates: false,
            allow_spaces: true,
            add_on_blur: true,
            tag_limit: 10

        });
    } catch (error) {}

    try {
        (function () {
            var myDropzone = $("div#image-editor").dropzone({
                drop_element: "image-editor",
                url: uploadURL,
                addRemoveLinks: true,
                dictRemoveFile: "Odstranit soubor",
                thumbnailWidth: 200,
                timeout: 60000,
                queuecomplete: () => {
                    readyImages = true;
                },
                acceptedFiles: 'image/jpeg, image/gif, image/png',
                removedfile: (file) => {
                    x = confirm('chcete skutečně soubor smazat?' + file.id);

                    if (!x) 
                        return false;
                     else {
                        file.previewElement.remove();

                        $.ajax({

                            url: deleteURL,
                            type: 'GET',
                            data: {
                                'name': file.name,
                                'prefix': prefix
                            },
                            dataType: "text",
                            success: function (data) {
                                images_name.forEach((val, index) => {
                                    if (val.name == file.name) {
                                        images_name.splice(index, 1);
                                    }
                                });
                            },
                            error: function (request, error) {
                                alert("Nastala chyba, toto pošlete administrátorovi: " + JSON.stringify(request));
                            }
                        });

                        return true;
                    }
                },
                init: function () {

                    thisDropzone = this;

                    var mockFile = {
                        name: images.cover.name,
                        size: images.cover.size,
                        id: images.cover.id
                    }
                    if (images.cover.id == null) {
                        cover = true;
                    } else {
                        images_name.push({name: images.cover.name, cover: true});

                        thisDropzone.displayExistingFile(mockFile,images.cover.url);
                    }
                    console.log(images.others);
                    if (images.others.length > 0) {
                        images.others.forEach((value, key) => {

                            var mockFile = {
                                name: value.name,
                                size: value.size,
                                id: value.id

                            };
                            images_name.push({name: value.name, cover: false});

                            thisDropzone.displayExistingFile(mockFile,value.url);


                        });
                    }
                    this.on("sending", function (file, xhr, formData) {
                        formData.append("product_id", decodeURIComponent($.urlParam('id_product')));
                        formData.append("cover", cover);
                        formData.append("prefix", prefix);
                        readyImages = false;


                    });
                    this.on("success", function (file, responseText) {

                        console.log(responseText);
                        file.id = responseText;
                        images_name.push({name: file.name, cover: cover});
                        if (cover) 
                            cover = false;
                        
                    });


                }
            });
        })();
    } catch (error) {}
    try {

        (function () {

            var myDropzone2 = new Dropzone(document.getElementById("file-editor"), {
                drop_element: "file-editor",
                url: uploadFileURL,
                addRemoveLinks: true,
                dictRemoveFile: "Odstranit soubor",
                                thumbnailWidth: 200,

                queuecomplete: () => {
                    readyFiles = true;
                    
                },
                thumbnailHeight: 150,
                timeout: 160000,

                init: function () {
                    let thisDropzone2 = this;
                    if (files.length > 0) {
                        files.forEach((file) => {
                            var mockFile = {
                                name: file.name,
                                size: file.size
                            }
                            thisDropzone2.options.addedfile.call(thisDropzone2, mockFile);
                            stlFilesToSend.push(file.name);
                            stlFilesToSend.push(file.name);
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');

                        });
                    }
                    this.on("sending", function (file, xhr, formData) {
                        readyFiles = false;
                        formData.append("prefix", prefix);
                    });


                },
                success: (file) => {
                    stlFilesToSend.push(file.name);
                },
                removedfile: (file) => {
                    x = confirm('chcete skutečně soubor smazat?' + file.name);
                    if (!x) 
                        return false;
                     else {
                        file.previewElement.remove();

                        $.ajax({

                            url: deleteFileURL,
                            type: 'GET',
                            data: {
                                'name': file.name
                            },
                            success: function (data) {
                                stlFilesToSend.forEach((val, index) => {
                                    if (val == file.name) {
                                        stlFilesToSend.splice(index, 1);
                                    }
                                });
                                alert('Úspěch!');
                            },
                            error: function (request, error) {
                                alert("Nastala chyba, toto pošlete administrátorovi: " + JSON.stringify(request));
                            },
                            dataType: "text"
                        });

                        return true;
                    }
                },

                acceptedFiles: 'application/octet-stream,application/sla,model/stl,application/wavefront-obj,application/vnd.ms-package.3dmanufacturing-3dmodel+xml,application/step,application/STEP,.stl,.STL,.obj,.OBJ,.3mf,.3MF,.stp,.STP,.STEP,.step'
            });
        })();
    } catch (error) {}
    try {
        $('#image-editor .dz-details').each(function (index, element) {
            (function (index) {
                $(element).attr('id', "filename_" + index);
                var selectFile = document.getElementById("image-editor").querySelector("#filename_" + index);
                selectFile.addEventListener("click", function () {
                    if (images.cover.name == $('#image-editor #filename_' + index + '> div > [data-dz-name=""]').text()) {
                        window.open(images.cover.url_large);
                    } else {
                        images.others.forEach((file) => {
                            if (file.name == $('#image-editor #filename_' + index + '> div > [data-dz-name=""]').text()) {
                                window.open(file.url_large);
                            }
                        })
                    }

                });
            }(index));
        });
    } catch (error) {}
    try {
        $('#file-editor .dz-details').each(function (index, element) {
            (function (index) {
                $(element).attr('id', "filename_" + index);
                var selectFile = document.getElementById("file-editor").querySelector("#file-editor #filename_" + index);
                selectFile.addEventListener("click", function () {

                    window.open(window.location.origin + "/upload/" + prefix + "/3D/" + $('#file-editor #filename_' + index + '> div > [data-dz-name=""]').text())
                });
            }(index));
        });
    } catch (error) {}
    try {
        $("#product_form").submit(function (e) {
            e.preventDefault();
        });
    } catch (error) {}
    try {

        $("#editProduct").click(function () {
            if (readyFiles && readyImages) {
                data_array = $("#product_form :input");
                document.getElementById("loader").style.display = "flex";

                /*$('#validated').change(function () {
                    if ($(this).is(":checked")) {
                        validated = 1;
                    }
                });*/
                data = {
                    files: JSON.stringify(stlFilesToSend),
                    images: JSON.stringify(images_name),
                    product_id: decodeURIComponent($.urlParam('id_product')),
                    prefix: prefix,
                    targetfile: targetfile

                };
                data_array.each(function () {
                    if(this.type == "checkbox"){
                        data[this.name] = $(this).prop("checked");
                    }else{
                        data[this.name] = $(this).val();
                    }
                });
                $.post({
                    url: editProductURL + "&" + data_array,
                    data: data,
                    success: () => {
                        if ($('#validated').is(":checked")) {
                            validated = true;
                        }
                        if (validated == false) {
                            $('#activateProduct').show();
                        } else {
                            $('#activateProduct').hide();
                        }
                        document.getElementById("loader").style.display = "none";

                    },
                    error: (err) => {
                        document.getElementById("loader").style.display = "none";
                        alert("nastala chyba, toto pošlete administrátorovi" + JSON.stringify(err));
                    }
                });
            } else {
                alert("Musíte počkat než se nahrají všechny soubory");
            }
        });
    } catch (error) {}
    try {
        $("#activateProduct").click(function () {
            document.getElementById("loader").style.display = "flex";

            $.get({
                url: activateProductURL,
                data: {
                    product_id: decodeURIComponent($.urlParam('id_product')),
                    validated: 1
                },
                success: () => {
                    document.getElementById("loader").style.display = "none";
                    $("#activateProduct").hide();
                    $('#validated').prop("checked", true);
                },
                error: (err) => {
                    document.getElementById("loader").style.display = "none";
                    alert("nastala chyba, toto pošlete administrátorovi" + JSON.stringify(err));
                }

            });
        });
    } catch (error) {}
    try {
        $("#removeProduct").click(function () {
            document.getElementById("loader").style.display = "flex";

            $.get({
                url: removeProductURL,
                data: {
                    product_id: decodeURIComponent($.urlParam('id_product'))
                },
                success: () => {
                    document.getElementById("loader").style.display = "none";
                    window.location.href = backredirectURL;
                },
                error: (err) => {
                    document.getElementById("loader").style.display = "none";
                    alert("nastala chyba, toto pošlete administrátorovi" + JSON.stringify(err));
                }

            });
        });
    } catch (error) {}
    try {
        (() => {
            var previousMessages = [];
            var messages = document.querySelector('.stl-message-list');

            var btn = document.getElementById('btnsend');
            var input = document.getElementById('messageinput');
            // Button/Enter Key
            btn.addEventListener('click', () => {
                sendMessage();
            });
            input.addEventListener('keyup', function (e) {
                if (e.keyCode == 13) 
                    sendMessage();
                


            })

            // Messenger Functions
            function sendMessage() {
                var msg = input.value;
                input.value = '';
                $.get({
                    url: sendMessagesURL,
                    data: {
                        id_product: decodeURIComponent($.urlParam('id_product')),
                        message: msg
                    },
                    success: () => {
                        writeLine(msg);
                        previousMessages.push({message: msg, author: "admin"});
                        getMessages();

                    },
                    error: (err) => {
                        alert("Nastala chyba při posílání zprávy")
                    }

                });

            }
            function addMessage(e) {
                var msg = e.data ? JSON.parse(e.data) : e;
                writeLine(`${
                    msg.FROM
                }: ${
                    msg.MESSAGE
                }`);
            }
            function writeLine(text) {
                var message = document.createElement('li');
                message.classList.add('stl-message-item', 'item-secondary');
                message.innerHTML = 'Já: ' + text;
                messages.appendChild(message);
                messages.scrollTop = messages.scrollHeight;

            }
            function writeLineCustomer(text, author) {
                var message = document.createElement('li');
                message.classList.add('stl-message-item', 'item-primary');
                message.innerHTML = author + ': ' + text;
                messages.appendChild(message);
                messages.scrollTop = messages.scrollHeight;
            }
            function removeAll() {
                scroll = $(messages).scrollTop();
                console.log(scroll);
                $(messages).empty();
            }
            function getMessages() {
                $.get({
                    url: getMessagesURL,
                    data: {
                        id_product: decodeURIComponent($.urlParam('id_product'))
                    },
                    success: (data) => {

                        data.forEach((message, index) => {
                            if (previousMessages.length - 1 < index) {

                                if (message.you) {
                                    writeLine(message.message);
                                } else {
                                    writeLineCustomer(message.message, message.author);
                                }
                            }
                        });
                        previousMessages = data;
                    },
                    error: (err) => {
                        alert("Nastala chyba při získávání zpráv")
                    }

                });
            }
            setInterval(getMessages, 3000);
            getMessages();
        })();
    } catch (error) {}
    try {
        (() => {
            $('#categories').select2();
        })();
    } catch (error) {}
    try {
        (() => {
            var previousMessages = [];

            var messages = document.querySelector('.proposal-message-list');

            var btn = document.getElementById('btnsendproposal');
            var input = document.getElementById('messageinputproposal');
            // Button/Enter Key
            btn.addEventListener('click', () => {
                sendMessage();
            });
            input.addEventListener('keyup', function (e) {
                if (e.keyCode == 13) 
                    sendMessage();
                


            })

            // Messenger Functions
            function sendMessage() {
                var msg = input.value;
                input.value = '';
                $.get({
                    url: sendmsgUrl,
                    data: {
                        id_user: decodeURIComponent($.urlParam('id_user')),
                        message: msg
                    },
                    success: () => {
                        writeLine(msg);
                        previousMessages.push({message: msg, author: "admin"});
                        getMessages();

                    },
                    error: (err) => {
                        alert("Nastala chyba při posílání zprávy")
                    }

                });
            }
            function addMessage(e) {
                var msg = e.data ? JSON.parse(e.data) : e;
                writeLine(`${
                    msg.FROM
                }: ${
                    msg.MESSAGE
                }`);
            }
            function writeLine(text) {
                var message = document.createElement('li');
                message.classList.add('proposal-message-item', 'item-secondary');
                message.innerHTML = 'Já: ' + text;
                messages.appendChild(message);
                messages.scrollTop = messages.scrollHeight;
            }
            function writeLineCustomer(text, author) {
                var message = document.createElement('li');
                message.classList.add('proposal-message-item', 'item-primary');
                message.innerHTML = author + ': ' + text;
                messages.appendChild(message);
                messages.scrollTop = messages.scrollHeight;
            }
            function removeAll() {
                $(messages).empty();
            }
            function getMessages() {
                $.get({
                    url: getmsgUrl,
                    data: {
                        id_user: decodeURIComponent($.urlParam('id_user'))
                    },
                    success: (data) => {
                        data.forEach((message, index) => {
                            if (previousMessages.length - 1 < index) {

                                if (message.you) {
                                    writeLine(message.message);
                                } else {
                                    writeLineCustomer(message.message, message.author);
                                }
                            }
                        });
                        previousMessages = data;
                    },
                    error: (err) => {
                        alert("Nastala chyba při získávání zpráv")
                    }

                });
            }
            setInterval(getMessages, 3000);
            getMessages();
        })();
    } catch (error) {}

    try {
        $("#acceptProposal").click(function () {
            document.getElementById("loader").style.display = "flex";

            $.get({
                url: acceptUrl,
                data: {
                    id_user: decodeURIComponent($.urlParam('id_user'))
                },
                success: () => {
                    document.getElementById("loader").style.display = "none";
                    window.history.back();
                },
                error: (err) => {
                    document.getElementById("loader").style.display = "none";
                    alert("nastala chyba, toto pošlete administrátorovi" + JSON.stringify(err));
                }

            });
        });
    } catch (error) {}

    try {
        $("#denyProposal").click(function () {
            document.getElementById("loader").style.display = "flex";

            $.get({
                url: denyUrl,
                data: {
                    id_user: decodeURIComponent($.urlParam('id_user'))
                },
                success: () => {
                    document.getElementById("loader").style.display = "none";
                    window.location.href = backredirectURL;
                },
                error: (err) => {
                    document.getElementById("loader").style.display = "none";
                    alert("nastala chyba, toto pošlete administrátorovi" + JSON.stringify(err));
                }

            });
        });
    } catch (error) {}
    try {
        if ($("#cut").length) {
            var slider = document.getElementById("cut");
            var output = document.getElementById("cut_info");
            slider.oninput = function () {
                output.innerHTML = this.value;
            }
        }
    } catch (error) {}
};
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};
