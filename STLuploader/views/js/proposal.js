    Dropzone.autoDiscover = false;
window.onload = () => {


    var editor = SUNEDITOR.create('description', {

        // plugins to load
        plugins: [
         
        ],

        // set the initial value
        value: description,

        // default tag name of the editor.
        defaultTag: 'p',

        // When recording the history stack, this is the delay time(miliseconds) since the last input
        historyStackDelayTime: 400,

        // Add tags to the default tags whitelist of editor.
        // _defaultTagsWhitelist : 'br|p|div|pre|blockquote|h[1-6]|ol|ul|li|hr|figure|figcaption|img|iframe|audio|video|table|thead|tbody|tr|th|td|a|b|strong|var|i|em|u|ins|s|span|strike|del|sub|sup'
        addTagsWhitelist: '',

        // Whitelist of tags when pasting. 
        // _editorTagsWhitelist  : _defaultTagsWhitelist + addTagsWhitelist
        // ex) 'p|h[1-6]'

        // Blacklist of the editor default tags. 
        // e.g. 'h1|h2' 
        tagsBlacklist: null,

        //  Blacklist of tags when pasting. 
        // e.g. 'h1|h2' 
        pasteTagsBlacklist: null,


        // Add attributes whitelist of tags that should be kept undeleted from the editor.
        // -- Fixed whitelist --
        // Native attributes: 'contenteditable|colspan|rowspan|target|href|src|class|type'
        // Editor attributes: 'data-format|data-size|data-file-size|data-file-name|data-origin|data-align|data-image-link|data-rotate|data-proportion|data-percentage|origin-size'
        // ex) {
        //  'all': 'style', // Apply to all tags
        //  'input': 'checked' // Apply to input tag
        // }              
        attributesWhitelist: '',

        // language object

        // change the tag of the default text button. 
        textTags: { bold: 'STRONG', underline: 'U', italic: 'EM', strike: 'DEL' },

        // change default formatBlock array.
        formats: ['p', 'div', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],

        // show the number of characters in the editor.   
        charCounter: false,

        // null || 'char' || 'byte' || 'byte-html'
        charCounterType: 'char',

        // text to be displayed in the "charCounter" area of the bottom bar
        charCounterLabel: null,

        // the maximum number of characters allowed to be inserted into the editor.
        maxCharCount: null,

        // the min-width size of the editor.
        minWidth: null,

        // the max-width size of the editor.
        maxWidth: null,

        // the size of the total uploadable images (in bytes).
        imageUploadSizeLimit: null,

        // if true, multiple images can be selected.
        imageMultipleFile: false,

        // allowed extensions like '.jpg, .png ..'
        imageAccept: "*",

        // The url of the image gallery, if you use the image gallery.
        // When "imageUrlInput" is true, an image gallery button is created in the image modal.
        // You can also use it by adding 'imageGallery' to the button list.
        imageGalleryUrl: null,

        // Http Header when get image gallery. 
        imageGalleryHeader: null,

        // 'classic', 'inline', 'balloon', 'balloon-always'
        mode: 'classic',

        // if true, the editor is set to RTL(Right To Left) mode.
        rtl: false,

        // toolbar width
        toolbarWidth: 'max-content',

        // 'cell', 'top'
        tableCellControllerPosition: 'cell',

        // if true, disables the interaction of the editor and tab key.
        tabDisable: false,

        // You can disable shortcuts.
        // e.g. ['bold', 'strike', 'underline', 'italic', 'undo', 'indent']
        shortcutsDisable: [],
                    
        // If false, hide the shortcuts hint.
        shortcutsHint: true,

        // A custom HTML selector placing the toolbar inside.
        toolbarContainer: null,

        // Sets to -1 or false or null to turn off
        // Sticky Toolbar
        // Default: 0px (offset)
        stickyToolbar: 0,

        // top offset value of "full Screen"
        fullScreenOffset: '',

        // the position property of suneditor.   
        position: null,

        // places content in the iframe
        iframe: false,

        // allows the usage of HTML, HEAD, BODY tags and DOCTYPE declaration.
        fullPage: false,

        // CSS file to apply inside the iframe
        iframeCSSFileName: 'suneditor',

        // e.g. <h1>Preview Template</h1> {{contents}} <div>_Footer_</div>
        previewTemplate: null,

        // A template of the "print".
        // The {{contents}} part in the HTML string is replaced with the contents of the editor.
        // e.g. "<div style='width:auto; max-width:1080px; margin:auto;'><h1>Print Template</h1> {{contents}} <div>_Footer_</div></div>"
        printTemplate: null,

        // CodeMirror option object
        codeMirror: null,

        // katex options
        // https://github.com/KaTeX/KaTeX
        katex: null,

        // Math plugin font size list.
        mathFontSize: [
            { text: '1', value: '1em', default: true },
            { text: '1.5', value: '1.5em' },
            { text: '2', value: '2em' },
            { text: '2.5', value: '2.5em' }
        ],

        // Shows the bottom resizing bar.
        resizingBar: true,

        // Font Family array
        font: ['Arial', 'Comic Sans MS', 'Courier New', 'Impact', 'Georgia', 'tahoma', 'Trebuchet MS', 'Verdana'],

        // Font Size array
        fontSize: [8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72],

        // Font size unit
        fontSizeUnit: 'px',

        // Enables video resizing
        videoResizing: true,

        // width/height of the video
        videoWidth: 560,
        videoHeight: '56.25%',

        // If true, video size can only be scaled by percentage.
        videoSizeOnlyPercentage: false,

        // Choose whether to video rotation buttons display.
        videoRotation: false,

        // The default aspect ratio of the video.
        videoRatio: 0.5625,

        // Video ratio selection options.
        videoRatioList: [{ name: 'Classic Film 3:2', value: 0.6666 }, { name: 'HD', value: 0.5625 }],

        // Choose whether the video height input is visible.
        videoHeightShow: true,

        // Choose whether the video ratio options is visible.
        videoRatioShow: true,

        // the query string of a YouTube embedded URL. 
        youtubeQuery: '',

        // whether to create a file input tag in the video upload window.
        videoFileInput: false,

        // whether to create a video url input tag in the video upload window.
        // if the value of videoFileInput is false, it will be unconditionally.
        videoUrlInput: true,

        // Http Header when uploading videos.  
        videoUploadHeader: null,

        // the video upload to server mapping address.
        videoUploadUrl: null,

        // the size of the total uploadable videos (in bytes).
        videoUploadSizeLimit: null,

        // if true, multiple videos can be selected. 
        videoMultipleFile: false,

        // define "Attributes" of the video tag.
        videoTagAttrs: null,

        // define "Attributes" of the iframe tag
        videoIframeAttrs: null,

        // allowed extensions like '.mp4, .avi ..'
        videoAccept: "*",

        // default width of the audio frame.  
        audioWidth: '300px',

        // default height of the audio frame. 
        audioHeight: "default",

        // whether to create a file input tag in the audio upload window
        audioFileInput: false,

        // whether to create a audio url input tag in the audio upload window.
        audioUrlInput: true,

        // Http Header when uploading audios. 
        audioUploadHeader: null,

        // upload url
        audioUploadUrl: null,

        // the size of the total uploadable audios (in bytes).
        // invokes the "onAudioUploadError" method.
        audioUploadSizeLimit: null,

        // if true, multiple audios can be selected.
        audioMultipleFile: false,

        // define "Attributes" of the audio tag.  
        audioTagAttrs: null,

        // allowed extensions like '.mp3, .wav ..'
        audioAccept: "*",

        // default protocol for the links. ('link', 'image', 'video', 'audio')
        linkProtocol: null,

        // the placeholder text.  
        placeholder: null,

        // Activate the media[image, video, audio] selection status immediately after inserting the media tag.
        mediaAutoSelect: true,

        // custom icons
        // {
        //   bold: 'B',
        //   table: '',
        //   insert_row_above: ''
        // }
        icons: null,

        // defines "rel" attribute list of anchor tag
        // e.g. ['author', 'external', 'nofollow']
        linkRel: [],

        // defines default "rel" attributes of anchor tag.
        // e.g.
        // {
        //   default: 'nofollow', // Default rel
        //   check_new_window: 'noreferrer noopener', // When "open new window" is checked 
        //   check_bookmark: 'bookmark' // When "bookmark" is checked 
        // },
        linkRelDefault: {},

        // Choose whether the image height input is visible.
        imageHeightShow: true,

        // enables image resizing
        imageResizing: true,

        // image width/height
        imageWidth: 'auto',
        imageHeight: 'auto',

        // If true, image size can only be scaled by percentage
        imageSizeOnlyPercentage: true,

        // Choose whether the image height input is visible.
        imageHeightShow: true,

        // Shows image rotation buttons
        imageRotation: false,

        // image file input
        imageFileInput: true,

        // image url input
        imageUrlInput: true,

        // image upload url
        imageUploadUrl: null,

        // Http Header when uploading images
        imageUploadHeader: null,

        // height/width of the editor
        height: '500px',
        width: '100%',

        // min height/width of the editor
        minHeight: null,
        minWidth: null,

        // color array of color picker
        // e.g. [['#ccc', '#dedede', 'OrangeRed', 'Orange', 'RoyalBlue', 'SaddleBrown'], ['SlateGray', 'BurlyWood', 'DeepPink', 'FireBrick', 'Gold', 'SeaGreen']]
        colorList: null,

        // line-height array
        lineHeights: [
            { text: '1', value: 1 },
            { text: '1.15', value: 1.15 },
            { text: '1.5', value: 1.5 },
            { text: '2', value: 2 }
        ],

        // Displays the current node structure to resizingBar
        showPathLabel: true,

        // Size of background area when activating dialog window
        popupDisplay: '',

        // CSS display property
        display: 'block',

        // show/hide toolbar icons
        buttonList: [
            ['undo', 'redo'],
            ['font', 'fontSize', 'formatBlock'],
            ['paragraphStyle', 'blockquote'],
            ['bold', 'underline', 'italic', 'strike', 'subscript', 'superscript'],
            ['fontColor', 'hiliteColor', 'textStyle'],
            ['removeFormat'],
            '/', // Line break
            ['outdent', 'indent'],
            ['align', 'horizontalRule', 'list', 'lineHeight'],
            ['table', 'link'], // You must add the 'katex' library at options to use the 'math' plugin.
            /** ['imageGallery'] */ // You must add the "imageGalleryUrl".

        ],

        // execute a function when the save button is clicked.
        callBackSave: function () { }
  
    });
    $("#form_propose").submit(function (eventObj) {
        if (!ready) {
            alert("Prosím počkejte než se nahrají soubory");
            eventObj.preventDefault();
        }
        $("<input />").attr("type", "hidden")
          .attr("value", editor.getContents())
          .attr("name", "propose_description")
          .appendTo("#form_propose");
        return true;
    });
    var ready = true;
    (function () {
            var myDropzone = new Dropzone( "div#image-upload", {
                drop_element: "#image-upload",
                url: "/module/STLuploader/uploadproposal",
                addRemoveLinks: true,
                dictRemoveFile: "Odstranit soubor",
                dictDefaultMessage: "Nahrajte fotografie",
                maxFiles: 10,
                thumbnailWidth: 300,
                thumbnailHeight: 300,
                acceptedFiles: 'image/jpeg, image/gif, image/png',
                queuecomplete: () => {
                    ready= true;
                },
                removedfile: (file) => {
                    x = confirm('chcete skutečně soubor smazat?');

                    if (!x) 
                        return false;
                     else {
                        file.previewElement.remove();

                        $.ajax({

                            url: "/module/STLuploader/uploadproposal",
                            type: 'POST',
                            data: {
                                'fileName': file.name,
                                'action': 'submitRemoveFile'
                            },
                            dataType: "text",
                            success: function (data) {
                            },
                            error: function (request, error) {
                                alert("Nastala chyba, toto pošlete administrátorovi: " + JSON.stringify(request));
                            }
                        });

                        return true;
                    }
                },
                init: function () {
                    var thisDropzone = this;
                    this.on("sending", function (file, xhr, formData) {
                        ready = false;
                    });
                    if (pictures.length > 0) {
                        pictures.forEach((value, key) => {

                            var mockFile = {
                                name: value.name,
                                size: value.size,

                            };


                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, value.url);
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');

                        });
                    }
                },
                
            });
          
        })();
  

}

