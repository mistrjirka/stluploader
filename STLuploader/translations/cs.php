<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{stluploader}prestashop>stluploader_78f3e402ff7773216a51862dc86568cc'] = 'Spravovací modul pro uživatelsky vytvářené virtuální produkty';
$_MODULE['<{stluploader}prestashop>stluploader_0f379daaee21894a7c6453950658fe8b'] = 'Jste si jistí že chcete odstranit tento modul?';
$_MODULE['<{stluploader}prestashop>stluploader_cc4cf33b0483a128de3c786aff75c71d'] = 'ID uživatele';
$_MODULE['<{stluploader}prestashop>stluploader_be53a0541a6d36f6ecb879fa2c584b08'] = 'Obrázek';
$_MODULE['<{stluploader}prestashop>stluploader_490aa6e856ccf208a054389e47ce0d06'] = 'ID';
$_MODULE['<{stluploader}prestashop>stluploader_df644ae155e79abf54175bd15d75f363'] = 'Název produktu';
$_MODULE['<{stluploader}prestashop>stluploader_5364259abab90e94890f2ed2481b9824'] = 'Schváleno';
$_MODULE['<{stluploader}prestashop>stluploader_2b00da4fe4c966b28b8dad518d4ac711'] = 'Cena bez daně';
$_MODULE['<{stluploader}prestashop>stluploader_3601146c4e948c32b6424d2c0a7f0118'] = 'Cena';
$_MODULE['<{stluploader}prestashop>stluploader_41af51cd826593654d0e01a79ed0fbb0'] = 'Notifikace';
$_MODULE['<{stluploader}prestashop>productform_df644ae155e79abf54175bd15d75f363'] = 'Název produktu';
$_MODULE['<{stluploader}prestashop>productform_3601146c4e948c32b6424d2c0a7f0118'] = 'Cena';
$_MODULE['<{stluploader}prestashop>productform_f7798e50e553b23427e7df0a7a2652f5'] = 'Kč';
$_MODULE['<{stluploader}prestashop>productform_c1069a480848e06782b81b8bea9c0c94'] = 'Krátký popis';
$_MODULE['<{stluploader}prestashop>productform_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Popis';
$_MODULE['<{stluploader}prestashop>productform_867343577fa1f33caa632a19543bd252'] = 'Klíčová slova';
$_MODULE['<{stluploader}prestashop>productform_7dce122004969d56ae2e0245cb754d35'] = 'Upravit';
$_MODULE['<{stluploader}prestashop>productform_a13367a8e2a3f3bf4f3409079e3fdf87'] = 'Schválit';
$_MODULE['<{stluploader}prestashop>productform_f2a6c498fb90ee345d997f888fce3b18'] = 'Smazat';
$_MODULE['<{stluploader}prestashop>proposal_c4408d335012a56ff58937d78050efad'] = 'Schválit';
$_MODULE['<{stluploader}prestashop>proposal_3682d1665cf331373000c20680732d3a'] = 'Zamítnout';
