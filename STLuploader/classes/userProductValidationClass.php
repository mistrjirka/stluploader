<?php


class userProductValidationClass extends ObjectModel 
{
    public $id_product;
    public $id_user;
    public $message;
    public $validated;
    public $notification_user;
    public $notification_admin;
    public $time_stamp;
    public static $definition = array(
        "table" => "user_product_validation",
        "primary" => "id_product",
        "fields" => array(
            "id_product" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "owned" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "cut" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "message" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
            ),
            "validated" => array(
                "type" => self::TYPE_INT,
                "required" => true

            ),
            "notification_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "time_stamp" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true
            ),
            "notification_admin" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "lastsawadmin" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true
            ),
            "lastsawuser" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true
            )
        )
    );
}