<?php


class DesignerClass extends ObjectModel 
{
    public $id_user;
    public $time_stamp;
    public static $definition = array(
        "table" => "ps_stluploader_designers",
        "primary" => "id_product",
        "fields" => array(
            
            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            
            "time_stamp" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true
            )
        )
        );
}