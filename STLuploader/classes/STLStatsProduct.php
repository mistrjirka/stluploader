<?php
class STLStatsProduct extends ProductCore
{
    public static function getNbOfSales($id_product)
    {
        $res = Db::getInstance()->getRow('
			SELECT quantity FROM `' . _DB_PREFIX_ . 'product_sale`
			WHERE `id_product` = ' . (int)$id_product);
        if(!$res) return 0;
        return $res['quantity'];
    }
    public function getNbSoldfrom($date)
    {
        $sql = "SELECT COALESCE(SUM(order_detail.product_quantity),0) as quantity FROM "._DB_PREFIX_."order_detail as order_detail INNER JOIN ". _DB_PREFIX_ ."orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.date_add > " .'"'. $date .'"'. " AND orders.valid=1 AND order_detail.product_id=".$this->id ;
        $result = Db::getInstance()->executeS($sql)[0]["quantity"];
        if($result == null){
            $result = 0;
        }
        return $result;
    }
    public function getSales($date)
    {
        $sql = "SELECT COALESCE(SUM(order_detail.total_price_tax_incl),0) AS price FROM "._DB_PREFIX_."order_detail as order_detail INNER JOIN ". _DB_PREFIX_ ."orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.date_add > " .'"'. $date .'"'. " AND orders.valid=1 AND order_detail.product_id=".$this->id ;
        $result = Db::getInstance()->executeS($sql)[0]["price"];
        if ($result == null){
            $result = 0;
        }
        return $result;
    }
    public function getSalesTotal()
    {
        $sql = "SELECT COALESCE(SUM(order_detail.total_price_tax_incl),0) AS price FROM "._DB_PREFIX_."order_detail as order_detail INNER JOIN ". _DB_PREFIX_ ."orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.valid=1 AND order_detail.product_id=".$this->id ;
        $result = Db::getInstance()->executeS($sql)[0]["price"];
        if ($result == null){
            $result = 0;
        }
        return $result;
    }

}