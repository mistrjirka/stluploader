<?php


class userCreationsClass extends ObjectModel 
{
    public $id_product;
    public $id_user;
    public $num_sold;

    public static $definition = array(
        "table" => "user_creation",
        "primary" => "id_product",
        "fields" => array(
            "id_product" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "num_sold" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
        )
        );
}