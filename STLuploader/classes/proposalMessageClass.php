<?php


class proposalMessageClass extends ObjectModel 
{
    public $id_user;
    public $author;
    public $time_stamp;
    public $message;
    public static $definition = array(
        "table" => "stluploader_proposal_message",
        "primary" => "id_user",
        "fields" => array(
            
            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "message" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            ), 
          
            "author"=> array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            ),            
            
            "time_stamp" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true

            )
        )
    );
}