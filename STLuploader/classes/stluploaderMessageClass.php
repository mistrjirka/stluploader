<?php



class stluploaderMessageClass extends ObjectModel
{
    public $id_product;
    public $id_user;
    public $author;
    public $time_stamp;
    public $message;
    public static $definition = array(
        "table" => "stluploader_message",
        "primary" => "id_product",
        "fields" => array(
            "id_product" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "message" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            ),

            "author" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            ),

            "time_stamp" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true

            )
        )
    );
}
