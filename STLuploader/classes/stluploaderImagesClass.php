<?php



class stluploaderImagesClass extends ObjectModel
{
    public $id_image;
    public $name;
    public static $definition = array(
        "table" => "stluploader_images",
        "primary" => "id_image",
        "fields" => array(
            "id_image" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "name" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            )
        )
    );
}
