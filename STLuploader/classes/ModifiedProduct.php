<?php


use ProductCore;


class ModifiedProduct extends ProductCore {
    public static function getNbOfSales($id_product)
    {
        $res = Db::getInstance()->getRow('
			SELECT quantity FROM `' . _DB_PREFIX_ . 'product_sale`
			WHERE `id_product` = ' . (int)$id_product);
        return $res['quantity'];
    }
}