<?php


class ProposalClass extends ObjectModel 
{
    public $id_user;
    public $message;
    public $time_stamp;
    public $notification_user;
    public $notification_admin;
    public static $definition = array(
        "table" => "stluploader_registeringdesigners",
        "primary" => "id_product",
        "fields" => array(
           
            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "message" => array(
                "type" => self::TYPE_HTML,
                "required" => true,
                "size"  => 250000
            ),             
            "time_stamp" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true
            ),
            "validated" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "notification_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "notification_admin" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "lastsawadmin" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true
            ),
            "lastsawuser" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true
            ),
        )
    );
}