<?php
// modules/your-module/src/Controller/DemoController.php

namespace STLuploader\Controller;

use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;

class DemoController extends FrameworkBundleAdminController
{
    public function demoAction()
    {
        // you can also retrieve services directly from the container
        $cache = $this->container->get('doctrine.cache');

        return $this->render('@Modules/your-module/templates/admin/demo.html.twig');
    }
}
