<?php

namespace STLuploader\Controller;

use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Symfony\Component\HttpFoundation\Response;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;

class AdminFileUploaderController extends FrameworkBundleAdminController
{

    // you can use symfony DI to inject services


    public function uploadAction()
    {
        if (!empty($_FILES)) {/*this checks if there is availible file to upload. If not it continues*/
            $this->context = Context::getContext();

            $tempFile = $_FILES['file']['tmp_name']; /*creating temporary file from $_FILES */
            if ($_FILES['file']["size"] > 256000000) {
                return new Response("file too large", 413);
            }

            $mimetype = mime_content_type($_FILES['file']['tmp_name']);
            if (!in_array($mimetype, array('application/sla', 'text/plain', 'model/stl', 'application/wavefront-obj', 'application/vnd.ms-package.3dmanufacturing-3dmodel+xml', 'application/vnd.ms-printing.printticket+xml', 'application/x-openscad, application/x-extension-fcstd', 'image/x-dwg', 'application/solids', 'application/STEP', 'application/dxf', 'application/iges', 'text/x-fortran', 'application/octet-stream'))) {
                return new Response("nahraj skutečný 3D model! $mimetype", 422);
            }
            $targetPath = _PS_UPLOAD_DIR_ . Tools::getValue("prefix"); /* default prestashop directory for uploads (not safe, no protection from downloading!!!!!)*/

            $targetFile = $targetPath . "/3D/" . $_FILES['file']['name']; /*target file path. "/img"*/


            return new Response(strval(move_uploaded_file($tempFile, $targetFile)));
        } else {
            return new Response("no files", 422);
        }
    }
    public function deleteAction()
    {
        $targetPath = _PS_UPLOAD_DIR_ . Tools::getValue("prefix");
        $targetFile = $targetPath . "/3D/" . Tools::getValue("name");
        try {
            unlink($targetFile);
        } catch (Throwable $error) {
            return new Response($error);
        }
        return new Response("OK");
    }
}
