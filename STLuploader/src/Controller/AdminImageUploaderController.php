<?php
// modules/your-module/src/Controller/DemoController.php

namespace STLuploader\Controller;


use Db;
use DbQuery;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;

use Symfony\Component\HttpFoundation\Response;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Throwable;




class AdminImageUploaderController extends FrameworkBundleAdminController
{
    public function uploadAction()
    {
        // you can also retrieve services directly from the container
        if (!empty($_FILES)) {/*this checks if there is availible file to upload. If not it continues*/
            $id_product = Tools::getValue("product_id");
            $tempFile = $_FILES['file']['tmp_name']; /*creating temporary file from $_FILES */
            $cover = 0;
            if (Tools::getValue("cover")=="true")
            {
                $cover = true;
            }
            
            if ($_FILES['file']["size"] > 3145728) {
                return new Response('Příliš velký soubor', 413);
            }
            try{
                $mimetype = mime_content_type($_FILES['file']['tmp_name']);
            }
            catch(Throwable $err){
                return new Response("Soubor nelze najít", 422);
            }
            if (!in_array($mimetype, array('image/jpeg', 'image/gif', 'image/png'))) {
                http_response_code(422);
                return new Response('Nahraj skutečný obrázek', 422);
            }
            $targetPath = _PS_UPLOAD_DIR_ . Tools::getValue("prefix"). "/img/";  /* default prestashop directory for uploads (not safe, no protection from downloading!!!!!) saved by random directory*/
            $targetFile = $targetPath . $_FILES['file']['name']; /*target file path. "/img"*/
            
            move_uploaded_file($tempFile,
                $targetFile
            );
            return new Response("ok");
        }
        
        
    }
    public function deleteImage(){

        try{
            $target_path = _PS_UPLOAD_DIR_ . Tools::getValue("prefix") . "/img/";
            
            $name = Tools::getValue("name");
            $target_file = $target_path . $name;
            unlink($target_file);
        }catch(Throwable $error){
        }
        return new JsonResponse(["message"=>"ok"]);
    }
    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
    protected function deleteDirectory($dir)
        {
            if (!file_exists($dir)) {
                return true;
            }

            if (!is_dir($dir)) {
                return unlink($dir);
            }

            foreach (scandir($dir) as $item) {
                if ($item == '.' || $item == '..') {
                    continue;
                }

                if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                    return false;
                }
            }

            return rmdir($dir);
        }
}