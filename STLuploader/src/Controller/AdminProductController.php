<?php
// modules/your-module/src/Controller/DemoController.php

namespace STLuploader\Controller;

use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use PrestaShop\PrestaShop\Adapter\SymfonyContainer;


class AdminProductController extends FrameworkBundleAdminController
{
    function redirectToProduct()
    {
        $sfContainer = SymfonyContainer::getInstance();
        if (null !== $sfContainer) {
            $sfRouter = $sfContainer->get('router');
            Tools::redirectAdmin($sfRouter->generate(
                'admin_product_form',
                ['id' => Tools::getValue('id_product')]
            ));
        }
    }
}
