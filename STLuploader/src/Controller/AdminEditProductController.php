<?php

namespace STLuploader\Controller;

use Category;
use Configuration;
use Customer;
use DbQuery;
use Db;
use Mail;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Symfony\Component\HttpFoundation\Response;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use PrestaShop\PrestaShop\Core\Domain\Category\Exception\CategoryNotFoundException;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use PrestaShop\PrestaShop\Core\Domain\Category\ValueObject\CategoryId;
use ProductDownload;
use ZipArchive;
use ImageType;
use PrestaShop\PrestaShop\Adapter\Entity\Hook;
use PrestaShop\PrestaShop\Adapter\Entity\Image;
use PrestaShop\PrestaShop\Adapter\Entity\ImageManager;
use PrestaShop\PrestaShop\Adapter\Entity\Shop;
use AdminImportControllerCore;
use Product;
use ObjectModel;

class AdminEditProductController extends FrameworkBundleAdminController
{

    // you can use symfony DI to inject services


    public function editAction()
    {
        $name = Tools::getValue("name");
        $shortDescription = Tools::getValue("shortDescription");
        $price = Db::getInstance()->escape(Tools::getValue("price"));
        $description = Tools::getValue("description");
        $keywords = Db::getInstance()->escape(Tools::getValue("keywords"));
        $validated = Db::getInstance()->escape(Tools::getValue("validated"));
        $owned = Db::getInstance()->escape(Tools::getValue("owned"));
        $cut = Db::getInstance()->escape(Tools::getValue("cut"));
        Db::getInstance()->escape(Tools::getValue("validated"));
        if ($validated == "true") {
            $validated = true;
        } else {
            $validated = false;
        }
        if ($owned == "true") {
            $owned = true;
        } else {
            $owned = false;
        }
        $files = json_decode(Tools::getValue("files"));
        $images = json_decode(Tools::getValue("images"));

        $product_id = Db::getInstance()->escape(Tools::getValue("product_id"));
        /*categories section*/
        $categories = Tools::getValue("categories");

        $categoriesTmp = Category::getAllCategoriesName($this->getCategoryId(), Context::getContext()->language->id);
        $categoriesParsed = [];
        $id_main = $this->getCategoryId();

        foreach ($categories as $id) {
            $valid = false;
            foreach ($categoriesTmp as $category) {
                if ($category["id_category"] !== $id_main && $category["id_category"] == $id) {
                    $valid = true;
                    array_push($categoriesParsed, $category["id_category"]);
                }
            }
            if (!$valid) {
                echo "Vyberte Kategorii ze seznamu";
                http_response_code(422);
                return;
            }
        }
        $categoriesTmp = $categoriesParsed;

        $product = new Product($product_id);

        $product->updateCategories(array_merge([$this->getCategoryId()], array_intersect($categories, $categoriesTmp)));
        /*end categories*/

        $product->meta_keywords[Context::getContext()->language->id] = $keywords;
        $product->price = intval($price);
        $product->description_short[Context::getContext()->language->id] = $shortDescription;
        $product->meta_description[Context::getContext()->language->id] = $shortDescription;
        $product->description[Context::getContext()->language->id] = $description;
        $db = \Db::getInstance();
        $db->update("user_product_validation", array(
            "owned" => intval($owned),
            "cut" => intval($cut) / 100
        ), "id_product=$product_id");

        if (!$product->active && intval($validated)) {
            $db = \Db::getInstance();
            $db->update("user_product_validation", array(
                "validated" => 1
            ), "id_product=$product_id");


            $user_id = $this->getFromTable("user_product_validation", "id_user", "id_user", "id_product=$product_id")[0]["id_user"];

            $user = new Customer($user_id);
            Mail::Send(
                (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                'adminnewproduct', // email template file to be use
                "Vaše produkt " . $product->name[Context::getContext()->language->id] . " byl schválen", // email subject
                array(
                    '{heading}' => "Vaš produkt  " . $product->name . " byl schválen k prodeji.",
                    '{message1}' => "Dobrý den, Váš produkt byl schválen k prodeji, nyní se objeví pod kategorií kterou jste zvolili. Na stránce <a href='https://www.allstl-shop.cz/module/STLuploader/creatorpage#/productdetail/" . $product->id . "'>o produktu</a>. Produkt vám nepůjde nadále upravovat, pokud chcete zažádat o úpravu napište administrátorovi přes chat nebo email.",
                    '{message2}' => "V případě potíží či potřeby kontaktovat administrátora použijte tento <a href='mailto:" . Configuration::get('PS_SHOP_EMAIL') . "'>email</a>.",
                    '{message3}' => "Hezký zbytek dne"
                ),
                $user->email, // receiver email address
                NULL, //receiver name
                NULL, //from email address
                NULL,  //from name,
                NULL,
                NULL,
                _PS_MODULE_DIR_ . 'STLuploader/mails/'
            );
        }
        $product->active = intval($validated);

        $product->name[Context::getContext()->language->id] = $name;
        $zipPath = _PS_UPLOAD_DIR_ . Tools::getValue("prefix") . "/";

        $this->addImages($images, Tools::getValue("prefix"), $product_id);

        try {
            $product_download_id = $this->getFromTable("product_download", "id_product", "id_product_download", "id_product = $product->id")[0]["id_product_download"];
        } catch (Throwable $error) {

            echo "Tento produkt není virtuální!";
            return "Tento produkt není virtuální!";
        }
        $download = new ProductDownload($product_download_id);
        $replacePath = _PS_DOWNLOAD_DIR_ . $download->filename;



        if (!file_exists($zipPath) || !file_exists($replacePath)) {
            return new Response("Soubory neexistují");
        }
        rename($replacePath, $replacePath . ".bak");

        if (!$this->Zip($zipPath, $replacePath)) {
            unlink($replacePath);
            rename($replacePath . ".bak", $replacePath);
            return new Response("Nastala chyba nemůžu otevřít zip", 500);
        }

        // close and save archive

        if ($product->update()) {
            return new Response("OK");
            unlink($replacePath . ".bak");
        } else {
            unlink($replacePath);
            rename($replacePath . ".bak", $replacePath);

            return new Response("Nastala chyba", 500);
        }
    }

    public function addImages($imageList, $prefix, $id_product)
    {
        /*remove images if there are not suposed to be there*/
        $target_path = _PS_UPLOAD_DIR_ . $prefix . "/img/";
        $files = scandir($target_path);
        if (!$files) {
            return false;
        }
        $filtered_files = [];

        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                array_push($filtered_files, $file);
            }
        }
        if (count($imageList) < count($filtered_files)) { /* checks if there are no "impostor" files in directory if there are it will thow them out of spaceship*/
            foreach ($filtered_files as $file) {
                $delete = true; /* if this file is not found and this will not be changed to false it will be deleted*/
                foreach ($imageList as $imageFile) {
                    if ($imageFile->name == $file) {
                        $delete = false;
                    }
                }
                if ($delete) {
                    unlink($target_path . $file);
                } else {
                }
            }
        }

        $product = new Product($id_product);

        $product->deleteImages();

        foreach ($imageList as $imageFile) {

            $url = $target_path . $imageFile->name;

            if (file_exists($url)) {
                $image = new Image();
                $image->id_product = $id_product;
                $image->position = 1;

                $image->cover = $imageFile->cover; // or false;
                $imgUrl = urldecode($url);

                if (($image->validateFields(false, true)) === true &&
                    ($image->validateFieldsLang(false, true)) === true && $image->add()
                ) {

                    if (AdminImportControllerOverride::copyImg($id_product, $image->id, $imgUrl, 'products', false)) {

                        if ($imageFile->cover) {
                            $product->setCoverWs($image->id);
                        }
                        $imageName = new stluploaderImagesClass();
                        $imageName->name = $imageFile->name;
                        $imageName->id_image = $image->id;
                        $imageName->save();
                    } else {
                        $image->delete();
                    }
                }
            }
        }
    }
    public function activateAction()
    {
        $product_id = Tools::getValue("product_id");
        $product = new Product($product_id);
        $validated = Tools::getValue("validated");
        $product->active = intval($validated);
        $db = \Db::getInstance();
        $db->update("user_product_validation", array(
            "validated" => 1
        ), "id_product=$product_id");

        $user_id = $this->getFromTable("user_product_validation", "id_user", "id_user", "id_product=$product_id")[0]["id_user"];
        $user = new Customer($user_id);
        if ($product->update()) {

            Mail::Send(
                (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                'adminnewproduct', // email template file to be use
                "Vaše produkt " . $product->name[Context::getContext()->language->id] . " byl schválen", // email subject
                array(
                    '{heading}' => "Vaš produkt  " . $product->name[Context::getContext()->language->id] . " byl schválen k prodeji.",
                    '{message1}' => "Dobrý den, Váš produkt byl schválen k prodeji, nyní se objeví pod kategorií kterou jste zvolili. Na stránce <a href='https://www.allstl-shop.cz/module/STLuploader/creatorpage#/productdetail/" . $product->id . "'>o produktu</a>. Produkt vám nepůjde nadále upravovat, pokud chcete zažádat o úpravu napište administrátorovi přes chat nebo email.",
                    '{message2}' => "V případě potíží či potřeby kontaktovat administrátora použijte tento <a href='mailto:" . Configuration::get('PS_SHOP_EMAIL') . "'>email</a>.",
                    '{message3}' => "Hezký zbytek dne"
                ),
                $user->email, // receiver email address
                NULL, //receiver name
                NULL, //from email address
                NULL,  //from name,
                NULL,
                NULL,
                _PS_MODULE_DIR_ . 'STLuploader/mails/'
            );
            return new Response("OK");
        } else {
            return new Response("Nastala chyba", 500);
        }
    }
    public function removeAction()
    {
        $product_id = Tools::getValue("product_id");
        $product = new Product($product_id);
        $db = \Db::getInstance();

        if ($product->delete() && $db->delete('user_product_validation', "id_product=$product_id") && $db->delete('stluploader_message', "id_product=$product_id") && $db->delete('user_creation', "id_product=$product_id")) {
            return new Response("OK");
        } else {
            return new Response("Nastala chyba", 500);
        }
    }
    function Zip($source, $destination)
    {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true) {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                } else if (is_file($file) === true) {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        } else if (is_file($source) === true) {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }
    public function getCategoryId()
    {
        $categories = $this->getFromTable("category_lang", "id_category", "id_category", "name='3D soubory'");
        if (count($categories) == 0) {

            $category_id = new CategoryId(10);
            throw new CategoryNotFoundException($category_id, "Vytvořte novou kategorii se jménem '3D soubory'");
        }
        return $categories[0]['id_category'];
    }
    /*public function getHomeCategoryId()
    {
        $categories = $this->getFromTable("category_lang", "id_category", "id_category", "name='Domů'");
        if (count($categories) == 0) {
            $category_id = new CategoryId(10);
            throw new CategoryNotFoundException($category_id, "Vytvořte novou kategorii se jménem 'Domů'");
        }
        return $categories[0]['id_category'];
    }*/
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
}

class stluploaderImagesClass extends ObjectModel
{
    public $id_image;
    public $name;
    public static $definition = array(
        "table" => "stluploader_images",
        "primary" => "id_image",
        "fields" => array(
            "id_image" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "name" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            )
        )
    );
}

class AdminImportControllerOverride extends AdminImportControllerCore
{

    public static function copyImg($id_entity, $id_image = null, $url = '', $entity = 'products', $regenerate = true)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();

                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . (int) $id_entity;

                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . (int) $id_entity;

                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_ . (int) $id_entity;

                break;
            case 'stores':
                $path = _PS_STORE_IMG_DIR_ . (int) $id_entity;

                break;
        }

        $url = urldecode(trim($url));
        $parced_url = parse_url($url);

        if (isset($parced_url['path'])) {
            $uri = ltrim($parced_url['path'], '/');
            $parts = explode('/', $uri);
            foreach ($parts as &$part) {
                $part = rawurlencode($part);
            }
            unset($part);
            $parced_url['path'] = '/' . implode('/', $parts);
        }

        if (isset($parced_url['query'])) {
            $query_parts = [];
            parse_str($parced_url['query'], $query_parts);
            $parced_url['query'] = http_build_query($query_parts);
        }

        if (!function_exists('http_build_url')) {
            require_once _PS_TOOL_DIR_ . 'http_build_url/http_build_url.php';
        }

        $url = http_build_url('', $parced_url);

        $orig_tmpfile = $tmpfile;

        if (Tools::copy(urldecode($url), $tmpfile)) {
            // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);

                return false;
            }

            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize($tmpfile, $path . '.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5, $src_width, $src_height);
            $images_types = ImageType::getImagesTypes($entity, true);
            foreach ($images_types as $image_type) {
                ImageManager::resize($tmpfile, $path . '-' . stripslashes($image_type['name']) . '.jpg', $image_type['width'], $image_type['height']);
                if (in_array($image_type['id_image_type'], $watermark_types)) {
                    Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                }
            }
            if (false) {

                $previous_path = null;
                $path_infos = [];
                $path_infos[] = [$tgt_width, $tgt_height, $path . '.jpg'];
                foreach ($images_types as $image_type) {
                    $tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

                    if (ImageManager::resize(
                        $tmpfile,
                        $path . '-' . stripslashes($image_type['name']) . '.jpg',
                        $image_type['width'],
                        $image_type['height'],
                        'jpg',
                        false,
                        $error,
                        $tgt_width,
                        $tgt_height,
                        5,
                        $src_width,
                        $src_height
                    )) {

                        // the last image should not be added in the candidate list if it's bigger than the original image
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = [$tgt_width, $tgt_height, $path . '-' . stripslashes($image_type['name']) . '.jpg'];
                        }
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '_' . (int) Context::getContext()->shop->id . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '_' . (int) Context::getContext()->shop->id . '.jpg');
                            }
                        }
                    }
                    if (in_array($image_type['id_image_type'], $watermark_types)) {
                        Hook::exec('actionWatermark', ['id_image' => $id_image, 'id_product' => $id_entity]);
                    }
                }
            }
        } else {
            @unlink($orig_tmpfile);
            return false;
        }
        unlink($orig_tmpfile);

        return true;
    }
}
