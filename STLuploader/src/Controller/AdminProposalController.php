<?php

namespace STLuploader\Controller;

use Db;
use PrestaShop\PrestaShop\Adapter\Entity\DbQuery;

use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Symfony\Component\HttpFoundation\Response;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use PrestaShop\PrestaShop\Adapter\Entity\Customer;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;
use PrestaShop\PrestaShop\Adapter\Entity\ObjectModel;
use PrestaShop\PrestaShop\Adapter\Entity\Mail;

use PrestaShop\PrestaShop\Adapter\Entity\Configuration;

class AdminProposalController extends FrameworkBundleAdminController
{

    // you can use symfony DI to inject services


    public function sendAction()
    {
        $id_user = Db::getInstance()->escape(Tools::getValue("id_user"));
        $messagetext = Db::getInstance()->escape(Tools::getValue("message"));
        $proposal = $this->getFromTable("stluploader_registeringdesigners", "id_user", "id_user, notification_user, lastsawuser", "id_user=$id_user");
        $infonotification = $proposal[0];
        $isfake = !boolval(count($proposal));
        if ($isfake) {
            return new Response("Access denied", 403);
        }
        if (strlen($messagetext) < 1) {
            return new Response("Pošlete delší zprávu", 422);
        }
        $message = new proposalMessageClass();
        $customer = new Customer($id_user);
        $message->id_user = (int) $id_user;
        $message->message = $messagetext;
        $message->author = "admin";
        $t = time();
        $message->time_stamp = date("Y-m-d H:i:s", $t);
        $db = \Db::getInstance();
        $date1 = new DateTime('now');
        $date2 = new DateTime($infonotification["lastsawuser"]);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $hours = $hours + ($diff->days * 24);
        $customer = new Customer($infonotification["id_user"]);
        if ($message->save()) {

            if (intval($infonotification["notification_user"]) == 0 && $hours > 12) {
                Mail::Send(
                    (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                    'adminnewproduct', // email template file to be use
                    "Nová zpráva od Admina", // email subject
                    array(
                        '{heading}' => "Admin vám napsal k odpověď do chatu, kvůli vaší žádosti stát se designerem:",
                        '{message1}' => $messagetext,
                        '{message2}' => "Chat naleznete <a href='https://www.allstl-shop.cz/muj-ucet'>zde</a>",
                        '{message3}' => "Hezký zbytek dne",

                    ),
                    $customer->email, //Configuration::get('PS_SHOP_EMAIL'), // receiver email address
                    NULL, //receiver name
                    NULL, //from email address
                    NULL,  //from name,
                    NULL,
                    NULL,
                    _PS_MODULE_DIR_ . 'STLuploader/mails/'
                );
            }
        }
        $query = "UPDATE `" . _DB_PREFIX_ . "stluploader_registeringdesigners` SET notification_user = CASE WHEN notification_user < 98 THEN notification_user + 1 ELSE notification_user END WHERE id_user=$id_user";

        $db->Execute($query);
        return new Response("OK", 200);
    }
    public function getAction()
    {
        $id_user = Db::getInstance()->escape(Tools::getValue("id_user"));
        $proposal = $this->getFromTable("stluploader_registeringdesigners", "id_user", "id_user", "id_user=$id_user");
        $isfake = !boolval(count($proposal));

        if ($isfake) {
            return new Response("Access denied", 403);
        }

        $sql = "SELECT 
                            message,
                            author,
                            id_message
                from " . _DB_PREFIX_ . "stluploader_proposal_message
                where id_user=$id_user ORDER BY id_message ASC;";
        $result = Db::getInstance()->executeS($sql);

        $json_toreturn = [];
        $id_user = $id_user;
        foreach ($result as $line) {
            $author = "";
            $you = false;
            if ($line["author"] == strval($id_user)) {

                // Validate customer object
                // Get Customer First Name
                $customer = new Customer((int) $id_user);
                $author = $customer->firstname . " " . $customer->lastname;
            } else {
                $you = true;

                $author = "Já";
            }
            if ($line["message"] != NULL) {
                array_push($json_toreturn, [
                    "id" => $line["id_message"],
                    "author" => $author,
                    "message" => $line["message"],
                    "you" => $you

                ]);
            }
        }
        $db = \Db::getInstance();

        $query = "UPDATE `" . _DB_PREFIX_ . "stluploader_registeringdesigners` SET lastsawadmin = CURRENT_TIMESTAMP, notification_admin = 0 WHERE id_user=$id_user";
        $db->Execute($query);
        return new JsonResponse($json_toreturn);
    }
    function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");

        return Db::getInstance()->executeS($sql);
    }

    function accept()
    {
        $id_user = Db::getInstance()->escape(Tools::getValue("id_user"));
        $db = \Db::getInstance();
        $user = new Customer($id_user);
        $t = time();
        $query = "UPDATE `" . _DB_PREFIX_ . "stluploader_registeringdesigners` SET validated = 2 WHERE id_user=$id_user";



        if ($db->Execute($query)) {
            if (!$this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $id_user)) {
                $designer = new DesignerClass();
                $designer->id_user = intval($id_user);
                $designer->time_stamp = date("Y-m-d H:i:s", $t);
                $designer->save();
            }
            Mail::Send(
                (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                'adminnewproduct', // email template file to be use
                "Vaše žádost byla schválena", // email subject
                array(
                    '{heading}' => "Vaše žádost byla schválena",
                    '{message1}' => "Dobrý den, Váše žádost byla schválena. Nyní můžete vytvářet návrhy pro produkt. Váš produkt poté projde validací a s adminem se dohodnete na ceně a procentech zisku. Může se stát, že od vás odkoupit práva na tento produkt.",
                    '{message2}' => "V případě potíží či potřeby kontaktovat administrátora použijte tento <a href='mailto:" . Configuration::get('PS_SHOP_EMAIL') . "'>email</a>.",
                    '{message3}' => "Hezký zbytek dne"
                ),
                $user->email, // receiver email address
                NULL, //receiver name
                NULL, //from email address
                NULL,  //from name,
                NULL,
                NULL,
                _PS_MODULE_DIR_ . 'STLuploader/mails/'
            );
            return new Response("OK");
        } else {
            return new Response("Database error", 500);
        }
    }
    function deny()
    {
        $id_user = Db::getInstance()->escape(Tools::getValue("id_user"));
        $db = \Db::getInstance();

        $query = "UPDATE `" . _DB_PREFIX_ . "stluploader_registeringdesigners` SET lastsawadmin = CURRENT_TIMESTAMP, validated = 1 WHERE id_user=$id_user";

        if ($db->Execute($query)) {
            try {
                $query = "DELETE FROM`" . _DB_PREFIX_ . "stluploader_designers` WHERE id_user=$id_user";
                $db->Execute($query);
            } catch (\Throwable $th) {
                //throw $th;
            }
            return new Response("OK");
        } else {
            return new Response("Database error", 500);
        }
    }
}


class proposalMessageClass extends ObjectModel
{
    public $id_user;
    public $author;
    public $time_stamp;
    public $message;
    public static $definition = array(
        "table" => "stluploader_proposal_message",
        "primary" => "id_user",
        "fields" => array(

            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "message" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            ),

            "author" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            ),

            "time_stamp" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true

            )
        )
    );
}
class DesignerClass extends ObjectModel
{
    public $id_user;
    public $time_stamp;
    public static $definition = array(
        "table" => "stluploader_designers",
        "primary" => "id_product",
        "fields" => array(

            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),

            "time_stamp" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true
            )
        )
    );
}
