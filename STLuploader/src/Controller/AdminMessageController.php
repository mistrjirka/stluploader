<?php

namespace STLuploader\Controller;

use Configuration;
use Db;
use PrestaShop\PrestaShop\Adapter\Entity\DbQuery;

use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Symfony\Component\HttpFoundation\Response;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use PrestaShop\PrestaShop\Adapter\Entity\Customer;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;
use Mail;
use PrestaShop\PrestaShop\Adapter\Entity\ObjectModel;
use Product;

class AdminMessageController extends FrameworkBundleAdminController
{

    // you can use symfony DI to inject services


    public function sendAction()
    {
        $messagetext = Db::getInstance()->escape(Tools::getValue("message"));
        $id_product = Db::getInstance()->escape(Tools::getValue("id_product"));
        $infonotification = $this->getFromTable("user_product_validation", "id_user", "id_user, notification_admin, lastsawuser", "id_product=$id_product");
        $isfake = !boolval(count($infonotification));
        if ($isfake) {
            return new Response("Access denied", 403);
        }
        $infonotification = $infonotification[0];
        if (strlen($messagetext) < 1) {
            return new Response("Pošlete delší zprávu", 422);
        }
        $message = new stluploaderMessageClass();
        $message->id_product = (int) intval($id_product);
        $message->id_user = (int) $infonotification["id_user"];
        $message->message = $messagetext;
        $message->author = "admin";
        $t = time();
        $message->time_stamp = date("Y-m-d H:i:s", $t);
        //$message->save();
        $date1 = new DateTime('now');
        $date2 = new DateTime($infonotification["lastsawuser"]);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $hours = $hours + ($diff->days * 24);
        $context = Context::getContext();
        $customer = new Customer($infonotification["id_user"]);
        $product = new Product($id_product);
        if ($message->save()) {
            if (intval($infonotification["notification_admin"]) == 0 && $hours > 12) {
                echo "nice";

                Mail::Send(
                    (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                    'adminnewproduct', // email template file to be use
                    "Nová zpráva od Admina u produktu " . $product->name[$context->language->id], // email subject
                    array(
                        '{heading}' => "Nová zpráva od Admina",
                        '{message1}' => "U produktu <a href='https://www.allstl-shop.cz/module/STLuploader/creatorpage#/productdetail/" . $product->id . "'>" . $product->name[$context->language->id] . " (ID: " . $id_product . ") </a> <br> Zpráva:",
                        '{message2}' => $messagetext,
                        '{message3}' => "V případě potíží či potřeby kontaktovat administrátora použijte tento <a href='mailto:" . Configuration::get('PS_SHOP_EMAIL') . "'>email</a>. <br> Hezký zbytek dne",

                    ),
                    $customer->email, // receiver email address
                    NULL, //receiver name
                    NULL, //from email address
                    NULL,  //from name,
                    NULL,
                    NULL,
                    _PS_MODULE_DIR_ . 'STLuploader/mails/'
                );
            }
        }
        $db = \Db::getInstance();

        $query = "UPDATE `" . _DB_PREFIX_ . "user_product_validation` SET notification_user = CASE WHEN notification_user < 98 THEN notification_user + 1 ELSE notification_user END WHERE id_product=$id_product";
        $db->Execute($query);
        return new Response("OK", 200);
    }
    public function getAction()
    {
        $id_product = Db::getInstance()->escape(Tools::getValue("id_product"));
        $product = $this->getFromTable("user_product_validation", "id_user", "id_user", "id_product=$id_product");
        $isfake = !boolval(count($product));

        if ($isfake) {
            return new Response("Access denied", 403);
        }
        $product = $product[0];
        $sql = "SELECT 
                            message,
                            author,
                            id_message
                from " . _DB_PREFIX_ . "stluploader_message
                where id_product=$id_product ORDER BY id_message ASC;";
        $result = Db::getInstance()->executeS($sql);

        $json_toreturn = [];
        $id_user = $product["id_user"];
        foreach ($result as $line) {
            $author = "";
            $you = false;
            if ($line["author"] == strval($id_user)) {

                // Validate customer object
                // Get Customer First Name
                $customer = new Customer((int) $id_user);
                $author = $customer->firstname . " " . $customer->lastname;
            } else {
                $you = true;

                $author = "Já";
            }
            if ($line["message"] != NULL) {
                array_push($json_toreturn, [
                    "id" => $line["id_message"],
                    "author" => $author,
                    "message" => $line["message"],
                    "you" => $you

                ]);
            }
        }
        $db = \Db::getInstance();

        $query = "UPDATE `" . _DB_PREFIX_ . "user_product_validation` SET lastsawadmin = CURRENT_TIMESTAMP, notification_admin = 0 WHERE id_product=$id_product";
        $db->Execute($query);
        return new JsonResponse($json_toreturn);
    }
    function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");

        return Db::getInstance()->executeS($sql);
    }
}

class stluploaderMessageClass extends ObjectModel
{
    public $id_product;
    public $id_user;
    public $author;
    public $time_stamp;
    public $message;
    public static $definition = array(
        "table" => "stluploader_message",
        "primary" => "id_product",
        "fields" => array(
            "id_product" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "id_user" => array(
                "type" => self::TYPE_INT,
                "validate" => "isGenericName",
                "required" => true
            ),
            "message" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            ),

            "author" => array(
                "type" => self::TYPE_STRING,
                "validate" => 'isString',
                "required" => true
            ),

            "time_stamp" => array(
                "type" => self::TYPE_NOTHING,
                "required" => true

            )
        )
    );
}
