<?php
class STLuploaderuploadimageModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;
    public function initContent()
    {
        // In the template, we need the vars paymentId & paymentStatus to be defined
        $this->context->smarty->assign(
            array()
        );

        // Will use the file modules/cheque/views/templates/front/validation.tpl

        $this->setTemplate('module:STLuploader//views/templates/front/ok.tpl');
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
    public function postProcess()
    {


        if (!empty($_FILES)) {/*this checks if there is availible file to upload. If not it continues*/
            $this->context = Context::getContext();
            $user_id = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $tempFile = $_FILES['file']['tmp_name']; /*creating temporary file from $_FILES */
            if ($_FILES['file']["size"] > 8145728) {
                http_response_code(413);
                echo "file too large";
                return;
            }
            $mimetype = mime_content_type($_FILES['file']['tmp_name']);
            if (!in_array($mimetype, array('image/jpeg', 'image/gif', 'image/png'))) {
                echo 'Nahraj skutečný obrázek';
                http_response_code(422);
                return;
            }
            $targetPath = _PS_UPLOAD_DIR_ . $this->context->customer->id . "/img/"; /* default prestashop directory for uploads (not safe, no protection from downloading!!!!!)*/

            $targetFile = $targetPath . $_FILES['file']['name']; /*target file path. "/img"*/
            try {
                mkdir(_PS_UPLOAD_DIR_ . $this->context->customer->id);
            } catch (Throwable $error) {
            }
            try {
                mkdir(_PS_UPLOAD_DIR_ . $this->context->customer->id . "/img");
            } catch (Throwable $error) {
            }
            echo "OK";
            move_uploaded_file($tempFile, $targetFile);
        }
        if (Tools::getValue("action") === "submitRemoveFile") {
            $this->context = Context::getContext();
            $user_id = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $targetPath = _PS_UPLOAD_DIR_ . $this->context->customer->id . "/img/";
            $targetFile = $targetPath . Tools::getValue("fileName");
            echo $targetPath;
            try {
                unlink($targetFile);
            } catch (Throwable $error) {
                echo "Wrong user file";
            }
            echo "OK";
        }
    }
}
