<?php
class STLuploaderuploadproposalModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;
    public function initContent()
    {
        // In the template, we need the vars paymentId & paymentStatus to be defined
        $this->context->smarty->assign(
            array()
        );

        // Will use the file modules/cheque/views/templates/front/validation.tpl

        $this->setTemplate('module:STLuploader//views/templates/front/ok.tpl');
    }
    public function postProcess()
    {


        if (!empty($_FILES)) {/*this checks if there is availible file to upload. If not it continues*/
            $this->context = Context::getContext();

            $tempFile = $_FILES['file']['tmp_name']; /*creating temporary file from $_FILES */
            if ($_FILES['file']["size"] > 9145728) {
                http_response_code(413);
                echo "Příliš velký soubor";
                return;
            }
            $mimetype = mime_content_type($_FILES['file']['tmp_name']);
            if (!in_array($mimetype, array('image/jpeg', 'image/gif', 'image/png'))) {
                echo 'Nahraj skutečný obrázek';
                http_response_code(422);
                return;
            }
            $targetPath = _PS_UPLOAD_DIR_ . "/registering_proposals/" . $this->context->customer->id . "/"; /* default prestashop directory for uploads (not safe, no protection from downloading!!!!!)*/

            $targetFile = $targetPath . $_FILES['file']['name']; /*target file path. "/img"*/
            try {
                mkdir(_PS_UPLOAD_DIR_ . "/registering_proposals/");
            } catch (Throwable $error) {
            }
            try {
                mkdir(_PS_UPLOAD_DIR_ . "/registering_proposals/" . $this->context->customer->id);
            } catch (Throwable $error) {
            }
            echo "OK";
            move_uploaded_file($tempFile, $targetFile);
        }
        if (Tools::getValue("action") === "submitRemoveFile") {
            $this->context = Context::getContext();

            $targetPath = _PS_UPLOAD_DIR_ . "/registering_proposals/" . $this->context->customer->id . "/";
            $targetFile = $targetPath . Tools::getValue("fileName");
            try {
                unlink($targetFile);
            } catch (Throwable $error) {
                echo "Wrong user file";
            }
            echo "OK";
        }
    }
}
