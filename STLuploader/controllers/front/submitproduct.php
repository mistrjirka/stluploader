<?php
class STLuploadersubmitproductModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;

    public function initContent()
    {
        // In the template, we need the vars paymentId & paymentStatus to be defined
        $this->context->smarty->assign(
            array()
        );

        // Will use the file modules/cheque/views/templates/front/validation.tpl

        $this->setTemplate('module:STLuploader//views/templates/front/ok.tpl');
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }

    public function postProcess()
    {


        if (Tools::getValue("action") === "submitProduct") {
            $id_user = $this->context->customer->id;

            $name = Db::getInstance()->escape(Tools::getValue("name"));
            $shortDescription = Db::getInstance()->escape(Tools::getValue("shortDescription"));
            $description = Tools::getValue("description");
            $terms = intval(Tools::getValue("terms"));
            $keywords = json_decode(Tools::getValue("keywords"));
            $categories = [];
            $pictures = json_decode(Tools::getValue("pictures"));
            $files = json_decode(Tools::getValue("files"));
            $price = intval(Db::getInstance()->escape(Tools::getValue("price")));
            $categoriesTmp = Category::getAllCategoriesName($this->module->getCategoryId(), $this->context->language->id);
            $id_main = $this->module->getCategoryId();

            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $id_user))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $doc = new DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);


            libxml_clear_errors();
            $doc->loadHTML('<?xml encoding="utf-8" ?>' . $description);
            $script_tags = $doc->getElementsByTagName('script');
            $length = $script_tags->length;


            // for each tag, remove it from the DOM
            for ($i = 0; $i < $length; $i++) {
                $script_tags->item($i)->parentNode->removeChild($script_tags->item($i));
            }
            $description = $this->closetags($doc->saveHTML($doc->documentElement));


            /*foreach ($categories as $id) {
                $valid = false;
                foreach ($categoriesTmp as $category) {
                    if ($category["id_category"] !== $id_main && $category["id_category"] == $id) {
                        $valid = true;
                    }
                }
                if (!$valid) {
                    echo "Vyberte Kategorii ze seznamu";
                    http_response_code(422);
                    return;
                }
            }*/
            $stl = false;
            if (!$terms) {
                echo "Prosím potvrďte podmínky pro používání služby";
                http_response_code(422);
                return;
            }

            if (count($files) === 0) {
                echo 'Nahraj soubory';
                http_response_code(422);
                return;
            }
            if (count($pictures) < 4) {
                echo 'Nahrajte alespoň 4 obrázky';
                http_response_code(422);
                return;
            }
            if (count($keywords) === 0) {
                echo 'Přidej klíčová slova';
                http_response_code(422);
                return;
            }
            if (strlen($name) < 5) {
                echo 'Přidej delší jméno';
                http_response_code(422);
                return;
            }
            if (strlen($description) < 10) {
                echo 'Přidej delší popis';
                http_response_code(422);
                return;
            }
            if (strlen($shortDescription) < 3) {
                echo 'Přidej delší krátký popis';
                http_response_code(422);
                return;
            }
            if ($price < 10) {
                echo 'Cena musí být větší jak 10Kč';
                http_response_code(422);
                return;
            }
            foreach ($files as $file) {
                if (pathinfo(_PS_UPLOAD_DIR_ . $this->context->customer->id . "/3D/$file")['extension'] == "stl" || pathinfo(_PS_UPLOAD_DIR_ . $this->context->customer->id . "/3D/$file")['extension'] == "STL") {
                    $stl = true;
                }
            }
            if (!$stl) {
                http_response_code(422);
                echo "Přidej stl!";
                return;
            }
            $this->module->CreateProductProposal(
                strval(rand(pow(10, 13 - 1), pow(10, 13) - 1)),                         // Product EAN13
                $shortDescription,                         // Product reference
                $name,                               // Product name
                9999,                                       // Product quantity
                $description, // Product description,
                array(),
                $price,                                // Product price
                $pictures,       // Product image
                $this->module->getCategoryId(),                                       // Product default category
                array_merge(array($this->module->getCategoryId()), $categories),                              // All categorys for product (array)
                $shortDescription,
                implode(', ', $keywords),
                $files
            );
            echo 'Váš produkt byl nahrán a čeká na schválení, prosím vyčkejte na potvrzení. Stav a komunikaci s adminem najdete v Vaše produkty->produkt';
            http_response_code(200);
            return;
        }
    }
    protected function closetags($html)
    {
        preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];
        $len_opened = count($openedtags);
        if (count($closedtags) == $len_opened) {
            return $html;
        }
        $openedtags = array_reverse($openedtags);
        for ($i = 0; $i < $len_opened; $i++) {
            if (!in_array($openedtags[$i], $closedtags)) {
                $html .= '</' . $openedtags[$i] . '>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }
}
