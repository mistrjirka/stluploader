<?php
class STLuploaderedit3dModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;

    public function initContent()
    {
        // In the template, we need the vars paymentId & paymentStatus to be defined
        $this->context->smarty->assign(
            array()
        );

        // Will use the file modules/cheque/views/templates/front/validation.tpl

        $this->setTemplate('module:STLuploader//views/templates/front/ok.tpl');
    }

    public function postProcess()
    {
        if (Tools::getValue("action") == "submitRemoveFile") {
            $id_product = Db::getInstance()->escape(Tools::getValue("id_product"));
            $id_user = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $id_user))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $secret = Db::getInstance()->escape(Tools::getValue("secret"));
            $name = Db::getInstance()->escape(Tools::getValue("file_name"));
            $isfake = !boolval(count($this->getFromTable("user_product_validation", "id_product", "validated, id_product", "id_product=$id_product AND id_user=$id_user")));

            if ($isfake) {
                http_response_code("403");
                echo "access denied";
                return;
            }
            $product = new Product($id_product);
            if ($product->active) {
                echo "Tento produkt už nemůžete upravovat. Napište prosím adminovi";
                http_response_code(422);
                return;
            }
            $targetFile = _PS_UPLOAD_DIR_ . $secret . "usr" . $id_user . $id_product . "/3D/" . $name;
            if (!file_exists($targetFile)) {
                echo "wrong secret";
                http_response_code(422);
                return;
            }
            try {
                unlink($targetFile);
            } catch (Throwable $error) {
                echo "Chyba s odstraňováním souboru";
                http_response_code(500);
                return;
            }
            echo "OK";
            http_response_code(200);
            return;
        }
        if (!empty($_FILES)) {/*this checks if there is availible file to upload. If not it continues*/
            $id_product = Db::getInstance()->escape(Tools::getValue("id_product"));
            $id_user = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $id_user))) {
                http_response_code(403);
                echo "Produkt nepatří tomuto uživateli nebo produkt neexistuje";
                return;
            }
            $secret = Db::getInstance()->escape(Tools::getValue("secret"));
            $isfake = !boolval(count($this->getFromTable("user_product_validation", "id_product", "validated, id_product", "id_product=$id_product AND id_user=$id_user")));
            $product = new Product($id_product);
            if ($product->active) {
                echo "Tento produkt už nemůžete upravovat. Napište prosím adminovi";
                http_response_code(422);
                return;
            }
            if ($isfake) {
                http_response_code("403");
                echo "access denied";
                return;
            }
            $tempFile = $_FILES['file']['tmp_name']; /*creating temporary file from $_FILES */
            if ($_FILES['file']["size"] > 256000000) {
                echo "file too large";
                http_response_code(413);
                return;
            }
            $targetPath = _PS_UPLOAD_DIR_ . $secret . "usr" . $id_user . $id_product . '/3D/';
            if (!file_exists($targetPath)) {
                echo "wrong secret";
                http_response_code(422);
                return;
            }
            $mimetype = mime_content_type($_FILES['file']['tmp_name']);
            if (!in_array($mimetype, array('application/sla', 'model/stl', 'application/wavefront-obj', 'application/vnd.ms-package.3dmanufacturing-3dmodel+xml', 'application/STEP', 'application/octet-stream', "text/plain", "application/zip"))) {
                echo "Nahraj skutečný 3D model! $mimetype";
                http_response_code(422);
                return;
            }
            /* default prestashop directory for uploads (not safe, no protection from downloading!!!!!)*/

            $targetFile = $targetPath . "/" . Db::getInstance()->escape($_FILES['file']['name']); /*target file path. "/img"*/
            echo $targetFile;


            if (move_uploaded_file($tempFile, $targetFile)) {
                echo "OK";
                http_response_code(200);
                return;
            } else {
                echo "Internal server error cannot move files";
                http_response_code(500);
                return;
            };
        }
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
    protected function deleteDirectory($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}
