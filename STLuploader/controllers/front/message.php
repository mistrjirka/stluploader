<?php

/**
 * <ModuleClassName> => STLuploader
 * <FileName> => creatorpage.php
 * Format expected: <ModuleClassName><FileName>ModuleFrontController
 */
require_once(_PS_MODULE_DIR_ . "STLuploader/classes/stluploaderMessageClass.php");
class STLuploadermessageModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;
    public function initContent()
    {
        $this->setTemplate('module:STLuploader//views/templates/front/ok.tpl');
    }

    public function postProcess()
    {
        if (Tools::getValue("action") === "sendmessage") {
            $messagetext = Db::getInstance()->escape(Tools::getValue("message"));

            $id_product = Db::getInstance()->escape(Tools::getValue("id_product"));
            $id_user = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $id_user))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $infonotification = $this->getFromTable("user_product_validation", "id_product", "validated, id_product, notification_admin, lastsawadmin", "id_product=$id_product AND id_user=$id_user");

            $isfake = !boolval(count($infonotification));
            if ($isfake) {
                http_response_code(403);
                echo "access denied";
                return;
            }

            if (strlen($messagetext) < 1) {
                http_response_code(422);
                echo "pošelte delší zprávu";
                return;
            }

            $db = \Db::getInstance();
            $t = time();

            $query = "UPDATE `" . _DB_PREFIX_ . "user_product_validation` SET notification_admin = CASE WHEN notification_admin < 98 THEN notification_admin + 1 ELSE notification_admin END WHERE id_product=$id_product AND id_user=$id_user";

            $db->Execute($query);
            $product = new Product(intval($id_product));

            $message = new stluploaderMessageClass();
            $message->id_product = (int) intval($id_product);
            $message->id_user = (int) $this->context->customer->id;
            $message->message = $messagetext;
            $message->author = strval($this->context->customer->id);
            $message->time_stamp = date("Y-m-d H:i:s", $t);
            $date1 = new DateTime('now');
            $date2 = new DateTime($infonotification[0]["lastsawadmin"]);

            $diff = $date2->diff($date1);

            $hours = $diff->h;
            $hours = $hours + ($diff->days * 24);
            if ($message->save()) {
                if (intval($infonotification[0]["notification_admin"]) == 0 && $hours > 12) {
                    Mail::Send(
                        (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                        'adminnewproduct', // email template file to be use
                        "Nová zpráva od autora produktu " . $product->name[$this->context->language->id], // email subject
                        array(
                            '{heading}' => "Nová zpráva od " .  $this->context->customer->firstname . " " . $this->context->customer->lastname,
                            '{message1}' => "U produktu " . $product->name[$this->context->language->id] . " (ID: " . $id_product . ") <br> Zpráva:",
                            '{message2}' => $messagetext,
                            '{message3}' => "Hezký zbytek dne",

                        ),
                        Configuration::get('PS_SHOP_EMAIL'), // receiver email address
                        NULL, //receiver name
                        NULL, //from email address
                        NULL,  //from name,
                        NULL,
                        NULL,
                        _PS_MODULE_DIR_ . $this->module->name . '/mails/'
                    );
                }
                return "ok";
            }
        }
        if (Tools::getValue("action") === "getmessages") {
            $id_product = Db::getInstance()->escape(Tools::getValue("id_product"));
            $id_user = $this->context->customer->id;
            $isfake = !boolval(count($this->getFromTable("user_product_validation", "id_product", "validated, id_product", "id_product=$id_product AND id_user=$id_user")));

            if ($isfake) {
                http_response_code(403);
                echo "access denied";
                return;
            }
            $t = time();
            $sql = "SELECT 
                        
                            message,
                            author,
                            id_message
                        
                        
                        
                from " . _DB_PREFIX_ . "stluploader_message
                where id_product=$id_product;";
            $result = Db::getInstance()->executeS($sql);

            $json_toreturn = [];
            foreach ($result as $line) {
                $author = "";
                if ($line["author"] == strval($id_user)) {
                    $author = "me";
                } else {
                    $author = "admin";
                }
                if ($line["message"] != NULL) {
                    array_push($json_toreturn, [
                        "id" => $line["id_message"],
                        "type" => "text",
                        "author" => $author,
                        "data" => [
                            "text" => $line["message"]
                        ]
                    ]);
                }
            }
            $db = \Db::getInstance();

            $query = "UPDATE `" . _DB_PREFIX_ . "user_product_validation` SET lastsawuser = CURRENT_TIMESTAMP, notification_user = 0 WHERE id_product=$id_product AND id_user=$id_user";
            $db->Execute($query);
            echo json_encode($json_toreturn);
            return;
        }
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");

        return Db::getInstance()->executeS($sql);
    }
    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    protected function deleteDirectory($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}
