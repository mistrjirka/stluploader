<?php

require_once(_PS_MODULE_DIR_ . "STLuploader/classes/stluploaderImagesClass.php");

class STLuploadereditimageModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;
    public function initContent()
    {
        // In the template, we need the vars paymentId & paymentStatus to be defined
        $this->context->smarty->assign(
            array()
        );

        // Will use the file modules/cheque/views/templates/front/validation.tpl

        $this->setTemplate('module:STLuploader//views/templates/front/ok.tpl');
    }
    public function postProcess()
    {
        if (!empty($_FILES)) {/*this checks if there is availible file to upload. If not it continues*/
            $user_id = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }

            $id_product = Tools::getValue("id_product");
            $product = new Product($id_product);
            $secret = Db::getInstance()->escape(Tools::getValue("secret"));
            if ($product->active) {
                echo "Tento produkt už nemůžete upravovat. Napište prosím adminovi";
                http_response_code(422);
                return;
            }
            $isreal = boolval(count($this->getFromTable("user_product_validation", "id_product", "validated, id_product", "id_product=$id_product AND id_user=$user_id")));
            if (!$isreal) {
                http_response_code(403);
                echo "Produkt nepatří tomuto uživateli nebo produkt neexistuje";
                return;
            }
            $tempFile = $_FILES['file']['tmp_name']; /*creating temporary file from $_FILES */
            $cover = 0;
            if (Tools::getValue("cover") == "true") {
                $cover = true;
            } else {
                $cover = false;
            }

            if ($_FILES['file']["size"] > 8145728) {
                http_response_code(413);

                echo 'Příliš velký soubor';
            }
            try {
                $mimetype = mime_content_type($_FILES['file']['tmp_name']);
            } catch (Throwable $err) {
                http_response_code(422);
                echo "Soubor nelze najít";
                return;
            }
            if (!in_array($mimetype, array('image/jpeg', 'image/gif', 'image/png'))) {
                http_response_code(422);
                echo 'Nahraj skutečný obrázek';
                return;
            }

            $targetPath = _PS_UPLOAD_DIR_ .  $secret . "usr" . $user_id . $id_product . '/img/'; /* default prestashop directory for uploads (not safe, no protection from downloading!!!!!) saved by random directory*/
            $targetFile = $targetPath . $_FILES['file']['name']; /*target file path. "/img"*/
            try {
                mkdir(_PS_UPLOAD_DIR_ .  $secret . "usr/");
            } catch (Throwable $error) {
            }
            try {
                mkdir($targetPath);
            } catch (Throwable $error) {
            }
            move_uploaded_file(
                $tempFile,
                $targetFile
            );


            return;
        }
        if (Tools::getValue("action") === "submitRemoveFile") {
            try {

                $user_id = $this->context->customer->id;
                if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                    http_response_code(403);
                    echo "Uživatel není designer";
                    return;
                }
                $id_product = Tools::getValue("id_product");
                $name = Tools::getValue("name");
                $secret = Db::getInstance()->escape(Tools::getValue("secret"));
                $product = new Product($id_product);
                if ($product->active) {
                    echo "Tento produkt už nemůžete upravovat. Napište prosím adminovi";
                    http_response_code(422);
                    return;
                }
                $isreal = boolval(count($this->getFromTable("user_product_validation", "id_product", "validated, id_product", "id_product=$id_product AND id_user=$user_id")));
                if (!$isreal) {
                    http_response_code(403);
                    echo "Produkt nepatří tomuto uživateli nebo produkt neexistuje";
                    return;
                }

                try {
                    $targetFile = _PS_UPLOAD_DIR_ . $secret . "usr" . $user_id . $id_product . "/img/" . $name;
                    unlink($targetFile);
                } catch (Throwable $e) {
                }
            } catch (Throwable $error) {
                echo json_encode(["error" => $error]);
                return;
            }
            echo json_encode(["message" => "ok"]);
        }
    }

    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    protected function deleteDirectory($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
    public static function copyImg($id_entity, $id_image = null, $url = '', $entity = 'products', $regenerate = true)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();

                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . (int) $id_entity;

                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . (int) $id_entity;

                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_ . (int) $id_entity;

                break;
            case 'stores':
                $path = _PS_STORE_IMG_DIR_ . (int) $id_entity;

                break;
        }

        $url = urldecode(trim($url));
        $parced_url = parse_url($url);

        if (isset($parced_url['path'])) {
            $uri = ltrim($parced_url['path'], '/');
            $parts = explode('/', $uri);
            foreach ($parts as &$part) {
                $part = rawurlencode($part);
            }
            unset($part);
            $parced_url['path'] = '/' . implode('/', $parts);
        }

        if (isset($parced_url['query'])) {
            $query_parts = [];
            parse_str($parced_url['query'], $query_parts);
            $parced_url['query'] = http_build_query($query_parts);
        }

        if (!function_exists('http_build_url')) {
            require_once _PS_TOOL_DIR_ . 'http_build_url/http_build_url.php';
        }

        $url = http_build_url('', $parced_url);

        $orig_tmpfile = $tmpfile;

        if (Tools::copy(urldecode($url), $tmpfile)) {
            // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);

                return false;
            }

            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize($tmpfile, $path . '.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5, $src_width, $src_height);
            $images_types = ImageType::getImagesTypes($entity, true);
            foreach ($images_types as $image_type) {
                ImageManager::resize($tmpfile, $path . '-' . stripslashes($image_type['name']) . '.jpg', $image_type['width'], $image_type['height']);
                if (in_array($image_type['id_image_type'], $watermark_types)) {
                    Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                }
            }
            if (false) {

                $previous_path = null;
                $path_infos = [];
                $path_infos[] = [$tgt_width, $tgt_height, $path . '.jpg'];
                foreach ($images_types as $image_type) {
                    $tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

                    if (ImageManager::resize(
                        $tmpfile,
                        $path . '-' . stripslashes($image_type['name']) . '.jpg',
                        $image_type['width'],
                        $image_type['height'],
                        'jpg',
                        false,
                        $error,
                        $tgt_width,
                        $tgt_height,
                        5,
                        $src_width,
                        $src_height
                    )) {


                        // the last image should not be added in the candidate list if it's bigger than the original image
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = [$tgt_width, $tgt_height, $path . '-' . stripslashes($image_type['name']) . '.jpg'];
                        }
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '_' . (int) Context::getContext()->shop->id . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '_' . (int) Context::getContext()->shop->id . '.jpg');
                            }
                        }
                    }
                    if (in_array($image_type['id_image_type'], $watermark_types)) {
                        Hook::exec('actionWatermark', ['id_image' => $id_image, 'id_product' => $id_entity]);
                    }
                }
            }
        } else {
            @unlink($orig_tmpfile);
            return false;
        }
        unlink($orig_tmpfile);

        return true;
    }
    protected static function get_best_path($tgt_width, $tgt_height, $path_infos)
    {
        $path_infos = array_reverse($path_infos);
        $path = '';
        foreach ($path_infos as $path_info) {
            list($width, $height, $path) = $path_info;
            if ($width >= $tgt_width && $height >= $tgt_height) {
                return $path;
            }
        }

        return $path;
    }
}
