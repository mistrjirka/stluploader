<?php

/**
 * <ModuleClassName> => STLuploader
 * <FileName> => creatorpage.php
 * Format expected: <ModuleClassName><FileName>ModuleFrontController
 */
session_start();
class ModifiedProduct extends ProductCore
{
    public static function getNbOfSales($id_product)
    {
        $res = Db::getInstance()->getRow('
			SELECT quantity FROM `' . _DB_PREFIX_ . 'product_sale`
			WHERE `id_product` = ' . (int)$id_product);
        if (!$res) return 0;
        return $res['quantity'];
    }
    public function getNbSoldfrom($date)
    {
        $sql = "SELECT SUM(order_detail.product_quantity) FROM " . _DB_PREFIX_ . "order_detail as order_detail INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.date_add > " . '"' . $date . '"' . " AND orders.valid=1 AND order_detail.product_id=" . $this->id;
        $result = Db::getInstance()->executeS($sql)[0]["SUM(order_detail.product_quantity)"];
        if ($result == null) {
            $result = 0;
        }
        return $result;
    }
    public function getSales($date)
    {
        $sql = "SELECT SUM(order_detail.total_price_tax_incl) AS price FROM " . _DB_PREFIX_ . "order_detail as order_detail INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.date_add > " . '"' . $date . '"' . " AND orders.valid=1 AND order_detail.product_id=" . $this->id;
        $result = Db::getInstance()->executeS($sql)[0]["price"];
        if ($result == null) {
            $result = 0;
        }
        return $result;
    }
    public function getSalesTotal()
    {
        $sql = "SELECT SUM(order_detail.total_price_tax_incl) AS price FROM " . _DB_PREFIX_ . "order_detail as order_detail INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.valid=1 AND order_detail.product_id=" . $this->id;
        $result = Db::getInstance()->executeS($sql)[0]["price"];
        if ($result == null) {
            $result = 0;
        }
        return $result;
    }
}
class STLuploaderGetstatsModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;
    public function initContent()
    {
    }

    public function postProcess()
    {
        header('Content-Type: application/json; charset=utf-8');
        $id_user = $this->context->customer->id;
        if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $id_user))) {
            http_response_code(403);
            echo "Uživatel není designer";
            return;
        }
        /*get all product ids of this user*/
        $from = new DateTime();
        $from->setDate(intval(date("Y")), intval(date("m", strtotime("-1 months"))), intval(Configuration::get("STLUPLOADER_PAYDAY", 15)));

        $to = new DateTime();
        $to->setDate(intval(date("Y")), intval(date("m")), intval(Configuration::get("STLUPLOADER_PAYDAY", 15)));

        $from_before = new DateTime();
        $from_before->setDate(intval(date("Y")), intval(date("m", strtotime("-2 months"))), intval(Configuration::get("STLUPLOADER_PAYDAY", 15)));

        $to_before = new DateTime();
        $to_before->setDate(intval(date("Y")), intval(date("m", strtotime("-1 months"))), intval(Configuration::get("STLUPLOADER_PAYDAY", 15)));

        $toreturn = [];
        $info = array_merge(
            $this->getStatsTotal(),
            $this->getStatsFrom($from->format('Y-m-d'), $to->format('Y-m-d')),
            $this->getStatsFrom($from_before->format('Y-m-d'), $to_before->format('Y-m-d'), "last_")
        );
        $toreturn = [
            "product_month" => $this->getBestProductFrom(date("Y-m-d H:i:s", strtotime("-1 months"))),
            "from" => $from->format('Y-m-d'),
            "to" => $to->format('Y-m-d'),
            "from_before" => $from_before->format('Y-m-d'),
            "to_before" => $to_before->format('Y-m-d'),
            "info" => [
                [
                    "name" => $this->l("Prodaných kusů"),
                    "this_month" => round($info["sold_month"], 2),
                    "last_month" => round($info["last_sold_month"], 2),
                    "total" => strval(round($info["sold_total"], 2))
                ],
                [
                    "name" => $this->l("Výnos pro vás"),
                    "this_month" => round($info["sales_month_fee"], 2) . " Kč",
                    "last_month" => round($info["last_sales_month_fee"], 2) . " Kč",
                    "total" => round($info["sales_total_fee"], 2) . " Kč",
                ],
                [
                    "name" => $this->l("Průměrný poplatek"),
                    "this_month" => round($info["cut_avg_month"], 2) * 100 . " %",
                    "last_month" => round($info["last_cut_avg_month"], 2) * 100 . " %",
                    "total" => round($info["cut_avg_total"], 2) * 100 . " %",
                ]

            ]

        ];
        echo json_encode($toreturn);
    }
    protected function getStatsTotal()
    {
        $sql = "SELECT  COALESCE(SUM(order_detail.total_price_tax_incl), 0) AS sales_total, 
                        COALESCE(SUM(order_detail.product_quantity),0) AS sold_total, 
                        COALESCE(SUM(order_detail.total_price_tax_incl - (product_validation.cut * order_detail.total_price_tax_incl)),0) AS sales_total_fee,
                        COALESCE(AVG(product_validation.cut),0) AS cut_avg_total 
        FROM " . _DB_PREFIX_ . "order_detail as order_detail 
        INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order 
        INNER JOIN " . _DB_PREFIX_ . "user_product_validation as product_validation ON product_validation.id_product=order_detail.product_id 
        WHERE product_validation.owned=0 AND orders.valid=1 AND product_validation.id_user=" . $this->context->customer->id;
        $result = Db::getInstance()->executeS($sql);

        return $result[0];
    }
    protected function getStatsFrom($from, $to, $prefix = "")
    {

        $sql = "SELECT  COALESCE(SUM(order_detail.total_price_tax_incl),0) AS " . $prefix . "sales_month, 
        COALESCE(SUM(order_detail.product_quantity),0) AS " . $prefix . "sold_month, 
        COALESCE(SUM(order_detail.total_price_tax_incl - (product_validation.cut * order_detail.total_price_tax_incl)),0) AS " . $prefix . "sales_month_fee,
        COALESCE(AVG(product_validation.cut), 0) AS " . $prefix . "cut_avg_month 
        FROM " . _DB_PREFIX_ . "order_detail as order_detail 
        INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order 
        INNER JOIN " . _DB_PREFIX_ . "user_product_validation as product_validation ON product_validation.id_product=order_detail.product_id 
        WHERE product_validation.owned=0 AND orders.valid=1 AND product_validation.id_user=" . $this->context->customer->id . " AND orders.date_add > " . '"' . $from . '" AND orders.date_add < "' . $to . '"';
        $result = Db::getInstance()->executeS($sql);
        return $result[0];
    }

    protected function getBestProductFrom($date)
    {
        $ids_product_sql = "SELECT id_product FROM " . _DB_PREFIX_ . "user_product_validation WHERE id_user=" . $this->context->customer->id;
        $result_ids =  Db::getInstance()->executeS($ids_product_sql);
        $ids = "";
        foreach ($result_ids as $i => $id) {
            if ($i + 1 < count($result_ids)) {
                $ids .= $id["id_product"] . ",";
            } else {
                $ids .= $id["id_product"];
            }
        }
        $result = [
            "sales" => 0,
            "sold" => 0,
            "sales_fee" => 0,
            "cut" => 0,
            "name" => ""
        ];

        if (count($result_ids)) {
            $sql = "SELECT  COALESCE(SUM(order_detail.total_price_tax_incl),0) AS sales, 
                            COALESCE(SUM(order_detail.product_quantity),0) AS sold, 
                            COALESCE(SUM(order_detail.total_price_tax_incl - (product_validation.cut * order_detail.total_price_tax_incl)),0) AS sales_fee,
                            COALESCE(product_validation.cut, 0) AS cut,
                            COALESCE(product.name, '') AS name
            FROM " . _DB_PREFIX_ . "order_detail as order_detail 
            INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order 
            INNER JOIN " . _DB_PREFIX_ . "user_product_validation as product_validation ON product_validation.id_product=order_detail.product_id 
            INNER JOIN " . _DB_PREFIX_ . "product_lang as product ON product.id_product=order_detail.product_id 
            WHERE product_validation.owned=0 AND orders.valid=1 AND product.id_lang=" . $this->context->language->id . ' AND product_validation.id_user=' . $this->context->customer->id . " AND orders.date_add > " . '"' . $date . '" AND product_validation.id_product=(
                SELECT id_product FROM `' . _DB_PREFIX_ . 'product_sale`
                WHERE quantity = (SELECT 
                MAX(quantity)
                    FROM
                `' . _DB_PREFIX_ . 'product_sale` WHERE id_product IN (' . $ids . ') ) AND id_product IN (' . $ids . ') LIMIT 1
            );';
            $result = Db::getInstance()->executeS($sql)[0];
        }
        return $result;
    }

    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    protected function deleteDirectory($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}
