<?php

/**
 * <ModuleClassName> => STLuploader
 * <FileName> => creatorpage.php
 * Format expected: <ModuleClassName><FileName>ModuleFrontController
 */
class STLuploaderCreatorpageModuleFrontController extends ModuleFrontController
{
  public $auth = true;
  public $guestAllowed = false;
  protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
  {
    $sql = new DbQuery();
    $sql->select("$whatToget");
    $sql->from("$tableName");
    $sql->where("$condition");
    $sql->orderBy("$orderBy");
    return Db::getInstance()->executeS($sql);
  }
  public function initContent()
  {
    // In the template, we need the vars paymentId & paymentStatus to be defined
    parent::initContent();
    $categoriesTmp = Category::getAllCategoriesName($this->module->getCategoryId(), $this->context->language->id);
    $categories = [];
    $children = [];
    $user_id = $this->context->customer->id;
    if (!count($this->getFromTable(
      "stluploader_designers",
      "",
      "id_user",
      "id_user=" . $user_id
    ))) {
      http_response_code(403);
      echo "Produkt nepatří tomuto uživateli nebo produkt neexistuje";
      header("Location: https://www.allstl-shop.cz/muj-ucet");
    }
    $id_main = $this->module->getCategoryId();
    foreach ($categoriesTmp as $category) {
      if ($category["id_category"] !== $id_main) {
        $categoryClass = new Category($category["id_category"]);
        $childrenCategories = $categoryClass->getAllChildren();
        if (!in_array($category["name"], $children)) {
          array_push($categories, [
            "id" => $category["id_category"],
            "name" => $category["name"],
            "code" => substr(
              hash("md5", $category["name"]),
              0,
              2
            )
          ]);
        }

        foreach ($childrenCategories as $key => $child) {
          array_push($children, $child->name);
          array_push($categories, [
            "id" => $child->id,
            "name" =>  " - " . $child->name,
            "code" => substr(
              hash("md5", $child->name),
              0,
              2
            )
          ]);
        }
      }
    }

    $this->context->smarty->assign(
      array(
        'pathApp' => $this->module->getPathUri() . 'views/js/app.js',
        'chunkVendor' => $this->module->getPathUri()  . 'views/js/chunk-vendors.js',
        'categories_list' => json_encode($categories)
      )
    );


    // Will use the file modules/cheque/views/templates/front/validation.tpl

    $this->setTemplate('module:STLuploader//views/templates/front/creatorpage.tpl');
  }
  public function postProcess()
  {


    if (!empty($_FILES)) {/*this checks if there is availible file to upload. If not it continues*/
      $this->context = Context::getContext();

      $tempFile = $_FILES['file']['tmp_name']; /*creating temporary file from $_FILES */

      $targetPath = _PS_UPLOAD_DIR_ . $this->context->customer->id . "/"; /* default prestashop directory for uploads (not safe, no protection from downloading!!!!!)*/

      $targetFile =  $targetPath . $_FILES['file']['name'];   /*target file path*/
      try {
        mkdir(_PS_UPLOAD_DIR_ . $this->context->customer->id);
      } catch (Throwable $error) {
      }
      echo "OK";
      move_uploaded_file($tempFile, $targetFile);
    }
    if (Tools::getValue("action") === "submitRemoveFile") {
      $this->context = Context::getContext();

      $targetPath = _PS_UPLOAD_DIR_ . $this->context->customer->id . "/";
      $targetFile =  $targetPath . Tools::getValue("fileName");
      echo $targetPath;
      try {
        unlink($targetFile);
      } catch (Throwable $error) {
        echo "Wrong user file";
      }
      echo "OK";
    }
  }
}
