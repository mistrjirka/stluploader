<?php
class STLuploadereditproductModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;

    public function initContent()
    {
        // In the template, we need the vars paymentId & paymentStatus to be defined
        $this->context->smarty->assign(
            array()
        );

        // Will use the file modules/cheque/views/templates/front/validation.tpl

        $this->setTemplate('module:STLuploader//views/templates/front/ok.tpl');
    }

    public function postProcess()
    {
        if (Tools::getValue("action") === "submitProduct") {
            $user_id = Db::getInstance()->escape($this->context->customer->id);
            $id_product = Tools::getValue("id_product");
            $name = Tools::getValue("name");
            $shortDescription = Tools::getValue("shortDescription");
            $description = Tools::getValue("description");
            $keywords = json_decode(Tools::getValue("keywords"));
            $files = json_decode(Tools::getValue("files"));
            $images = json_decode(Tools::getValue("images"));
            $price = intval(Db::getInstance()->escape(Tools::getValue("price")));
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $secret = Tools::getValue("secret");
            /*$categories = [];
            $categoriesTmp = Category::getAllCategoriesName($this->module->getCategoryId(), $this->context->language->id);
            $categoriesParsed = [];
            $id_main = $this->module->getCategoryId();*/
            $product = new Product($id_product);
            $info = $this->getFromTable("user_product_validation", "id_product", "*", "id_product=$id_product AND id_user=$user_id");
            $isfake = !boolval(count($info));
            if ($isfake) {
                http_response_code("403");
                echo "access denied";
                return;
            }
            $editable = intval($info[0]["owned"]);
            if ($editable) {
                echo "Tento produkt už nemůžete upravovat, protože byl odkoupen. Napište prosím adminovi";
                http_response_code(422);
                return;
            }
            if ($product->active) {
                echo "Tento produkt už nemůžete upravovat, protože byl schválen k prodeji. Napište prosím adminovi";
                http_response_code(422);
                return;
            }
            $doc = new DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);


            libxml_clear_errors();
            $doc->loadHTML('<?xml encoding="utf-8" ?>' . $description);
            $script_tags = $doc->getElementsByTagName('script');
            $length = $script_tags->length;


            // for each tag, remove it from the DOM
            for ($i = 0; $i < $length; $i++) {
                $script_tags->item($i)->parentNode->removeChild($script_tags->item($i));
            }
            $description = $this->closetags($doc->saveHTML($doc->documentElement));
            /*
            foreach ($categories as $id) {
                $valid = false;
                foreach ($categoriesTmp as $category) {
                    if ($category["id_category"] !== $id_main && $category["id_category"] == $id) {
                        $valid = true;
                        array_push($categoriesParsed,$category["id_category"]);
                    }
                }
                if (!$valid) {
                    echo "Vyberte Kategorii ze seznamu";
                    http_response_code(422);
                    return;
                }
            }
            $categoriesTmp = $categoriesParsed;
            $stl = false;
            if (count($categories) === 0) {
                echo "Vyberte Kategorie";
                http_response_code(422);
                return;
            }*/

            $zipPath = _PS_UPLOAD_DIR_ . $secret . "usr" . $user_id . $id_product . "/";
            if (!file_exists($zipPath)) {
                die($zipPath);
                echo 'Nahraj soubory';
                http_response_code(422);
                return;
            }
            if (count($files) === 0) {
                echo 'Nahraj soubory';
                http_response_code(422);
                return;
            }
            if (count($images) === 0) {
                echo 'Nahraj obrázky';
                http_response_code(422);
                return;
            }
            if (strlen($secret) === 0) {
                echo 'Chyba v tajemství';
                http_response_code(422);
                return;
            }
            if (count($keywords) === 0) {
                echo 'Přidej klíčová slova';
                http_response_code(422);
                return;
            }
            if (strlen($name) < 5) {
                echo 'Přidej delší jméno';
                http_response_code(422);
                return;
            }
            if (strlen($description) < 10) {
                echo 'Přidej delší popis';
                http_response_code(422);
                return;
            }
            if (strlen($shortDescription) < 3) {
                echo 'Přidej delší krátký popis';
                http_response_code(422);
                return;
            }
            if ($price < 10) {
                echo 'Cena musí být větší jak 10Kč';
                http_response_code(422);
                return;
            }


            $product->meta_keywords[Context::getContext()->language->id] = implode(', ', $keywords);
            $product->price = intval($price);
            $product->description_short[Context::getContext()->language->id] = $shortDescription;
            $product->meta_description[Context::getContext()->language->id] = $shortDescription;
            $product->description[Context::getContext()->language->id] = $description;
            $product->name[Context::getContext()->language->id] = $name;
            try {
                $product_download_id = $this->getFromTable("product_download", "id_product", "id_product_download", "id_product = $product->id")[0]["id_product_download"];
            } catch (Throwable $error) {
                echo "Tento produkt není virtuální!";
                return "Tento produkt není virtuální!";
            }

            $this->addImages($images, $zipPath, $id_product);
            $download = new ProductDownload($product_download_id);
            $replacePath = _PS_DOWNLOAD_DIR_ . $download->filename;
            rename($replacePath, $replacePath . ".bak");

            if (!$this->Zip($zipPath, $replacePath)) {
                unlink($replacePath);
                rename($replacePath . ".bak", $replacePath);
                http_response_code(500);

                echo "Nastala chyba nemůžu otevřít zip";
                return;
            }

            // close and save archive
            if ($product->update()) {
                http_response_code(200);
                echo "Produkt byl upraven.";
                unlink($replacePath . ".bak");

                return;
            } else {
                unlink($replacePath);
                rename($replacePath . ".bak", $replacePath);

                http_response_code(500);

                echo "Nastala chyba";
                return;
            }
        }
    }

    public function addImages($imageList, $prefix, $id_product)
    {
        /*remove images if there are not suposed to be there*/

        $target_path = $prefix . "img/";
        $files = scandir($target_path);
        if (!$files) {
            return false;
        }
        $filtered_files = [];

        foreach ($files as $file) {

            if ($file !== "." && $file !== "..") {
                array_push($filtered_files, $file);
            }
        }
        if (count($imageList) < count($filtered_files)) { /* checks if there are no "impostor" files in directory if there are it will thow them out of spaceship*/
            foreach ($filtered_files as $file) {
                $delete = true; /* if this file is not found and this will not be changed to false it will be deleted*/
                foreach ($imageList as $imageFile) {
                    if ($imageFile->name == $file) {
                        $delete = false;
                    }
                }
                if ($delete) {
                    unlink($target_path . $file);
                } else {
                }
            }
        }

        $product = new Product($id_product);

        $product->deleteImages();

        foreach ($imageList as $imageFile) {

            $url = $target_path . $imageFile->name;

            if (file_exists($url)) {
                $image = new Image();
                $image->id_product = $id_product;
                $image->position = 1;

                $image->cover = $imageFile->cover; // or false;
                $imgUrl = urldecode($url);

                if (($image->validateFields(false, true)) === true &&
                    ($image->validateFieldsLang(false, true)) === true && $image->add()
                ) {

                    if (AdminImportControllerOverride::copyImg($id_product, $image->id, $imgUrl, 'products', false)) {

                        if ($imageFile->cover) {
                            $product->setCoverWs($image->id);
                        }
                        $imageName = new stluploaderImagesClass();
                        $imageName->name = $imageFile->name;
                        $imageName->id_image = $image->id;
                        $imageName->save();
                    } else {
                        $image->delete();
                    }
                }
            }
        }
    }


    function Zip($source, $destination)
    {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true) {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                } else if (is_file($file) === true) {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        } else if (is_file($source) === true) {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }
    protected function closetags($html)
    {
        preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];
        $len_opened = count($openedtags);
        if (count($closedtags) == $len_opened) {
            return $html;
        }
        $openedtags = array_reverse($openedtags);
        for ($i = 0; $i < $len_opened; $i++) {
            if (!in_array($openedtags[$i], $closedtags)) {
                $html .= '</' . $openedtags[$i] . '>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
    protected function deleteDirectory($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}
