<?php

/**
 * <ModuleClassName> => STLuploader
 * <FileName> => creatorpage.php
 * Format expected: <ModuleClassName><FileName>ModuleFrontController
 */
session_start();
class ModifiedProduct extends ProductCore
{
    public static function getNbOfSales($id_product)
    {
        $res = Db::getInstance()->getRow('
			SELECT quantity FROM `' . _DB_PREFIX_ . 'product_sale`
			WHERE `id_product` = ' . (int)$id_product);
        if (!$res) return 0;
        return $res['quantity'];
    }
    public function getNbSoldfrom($date)
    {
        $sql = "SELECT COALESCE(SUM(order_detail.product_quantity),0) as quantity FROM " . _DB_PREFIX_ . "order_detail as order_detail INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.date_add > " . '"' . $date . '"' . " AND orders.valid=1 AND order_detail.product_id=" . $this->id;
        $result = Db::getInstance()->executeS($sql)[0]["quantity"];
        if ($result == null) {
            $result = 0;
        }
        return $result;
    }
    public function getSales($date)
    {
        $sql = "SELECT COALESCE(SUM(order_detail.total_price_tax_incl),0) AS price FROM " . _DB_PREFIX_ . "order_detail as order_detail INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.date_add > " . '"' . $date . '"' . " AND orders.valid=1 AND order_detail.product_id=" . $this->id;
        $result = Db::getInstance()->executeS($sql)[0]["price"];
        if ($result == null) {
            $result = 0;
        }
        return $result;
    }
    public function getSalesTotal()
    {
        $sql = "SELECT COALESCE(SUM(order_detail.total_price_tax_incl),0) AS price FROM " . _DB_PREFIX_ . "order_detail as order_detail INNER JOIN " . _DB_PREFIX_ . "orders AS orders ON orders.id_order=order_detail.id_order WHERE orders.valid=1 AND order_detail.product_id=" . $this->id;
        $result = Db::getInstance()->executeS($sql)[0]["price"];
        if ($result == null) {
            $result = 0;
        }
        return $result;
    }
}
class STLuploaderGetproductsModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;
    public function initContent()
    {
    }

    public function postProcess()
    {
        header('Content-Type: application/json; charset=utf-8');
        if (Tools::getValue("type") === "getproducts") {
            $user_id = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $products_validation = $this->getFromTable("user_product_validation", "id_product DESC", "*", "id_user=$user_id");
            $products = [];
            foreach ($products_validation as $product_info) {
                $exists = boolval(count($this->getFromTable("product", "id_product", "id_product", "id_product=" . $product_info["id_product"])));
                if ($exists) {
                    $product = new ModifiedProduct($product_info["id_product"]);
                    $img = $product->getCover($product->id);
                    $link = new Link();

                    array_push(
                        $products,
                        [
                            'ID' => $product_info["id_product"],
                            'image' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https://" : "http://") . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $img['id_image'], 'small_default'),
                            "name" => $product->name[$this->context->language->id],
                            "price" => round(
                                $product->price,
                                2
                            ) . " Kč",
                            "validated" => (!boolval($product_info["validated"])) ? $this->trans('Ne', [], 'Modules.STLuploader.getproducts.php') :
                                $this->trans('Ano', [], 'Modules.STLuploader.getproducts.php'),
                            "notifications" => $product_info["notification_user"],
                            "num_sold" => $product->getNbOfSales($product_info["id_product"]),
                            "uneditable" => boolval(
                                intval($product->active)
                            ),
                            "owned" => boolval($product_info["owned"]),
                            "cut" => floatval($product_info["cut"])
                        ]
                    );
                }
            }
            echo json_encode($products);
        }
        if (Tools::getValue("type") === "getinfo") {
            $user_id = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $id_product = Db::getInstance()->escape(Tools::getValue("id_product"));
            $isfake = !boolval(count($this->getFromTable("user_product_validation", "id_product", "validated, id_product", "id_product=$id_product AND id_user=$user_id")));


            if ($isfake) {
                http_response_code(403);
                echo "access denied";
                return;
            }
            $product = new ModifiedProduct($id_product);
            $img = $product->getCover($product->id);
            $link = new Link;
            $image_objects = $product->getImages($this->context->language->id);

            $image_urls = array();
            foreach ($image_objects as $image_object) {
                if ($image_object["cover"] != 1) {
                    $imgClass = new Image(intval($image_object['id_image']));

                    $name = strval($imgClass->id_image) . ".jpg";
                    try {
                        $name = $this->getFromTable("stluploader_images", "id_image", "name", "id_image=" . $imgClass->id_image)[0]["name"];
                    } catch (Throwable $error) {
                    }

                    array_push($image_urls, array(
                        "url" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id],  $image_object['id_image'], 'small_default'),
                        "url_large" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $image_object['id_image'], ''),
                        "name" => $name,
                        "size" => 128,
                        "id" => $imgClass->id
                    ));
                }
            }

            $name = $img['id_image'] . ".jpg";
            try {
                $name = $this->getFromTable("stluploader_images", "id_image", "name", "id_image=" . $img['id_image'])[0]["name"];
            } catch (Throwable $error) {
            }
            $images = array(
                "cover" => array(
                    "url" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $img['id_image'], 'home_default'),
                    "url_large" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $img['id_image'], ''),
                    "name" => $name,
                    "size" => 128,
                    "id" => $img['id_image']
                ),
                "others" => $image_urls
            );



            try {
                $product_download_id = $this->getFromTable("product_download", "id_product", "id_product_download", "id_product = $product->id")[0]["id_product_download"];
            } catch (Throwable $error) {
                echo "Tento produkt není virtuální!";
                return "Tento produkt není virtuální!";
            }

            $download = new ProductDownload($product_download_id);
            $user_id = $this->context->customer->id;
            $download_path = _PS_DOWNLOAD_DIR_ . $download->filename;
            $random_string = $this->generateRandomString(20);
            $file_name = _PS_UPLOAD_DIR_ . $random_string . "usr" . $user_id . $product->id;
            try {
                mkdir($file_name);
            } catch (Throwable $err) {
            }

            if (isset($_SESSION["uploadedfiles_" . $user_id])) {
                try {
                    $this->deleteDirectory($_SESSION["uploadedfiles_" . $user_id]);
                } catch (Throwable $err) {
                }
            }
            $zip = new ZipArchive;
            $filelist_uf = [];
            $filelist = [];

            if ($zip->open($download_path) === TRUE) {
                // Unzip Path
                $zip->extractTo($file_name);
                $zip->close();
                $filelist_uf = scandir($file_name . "/3D", SCANDIR_SORT_ASCENDING);
            } else {
                echo 'Unzipped Process failed';
            }
            foreach ($filelist_uf as $value) {
                if ($value == "." || $value == "..") {
                } else {
                    array_push($filelist, ["name" => $value, "size" => filesize($file_name . "/3D/" . $value)]);
                }
            };
            $_SESSION["uploadedfiles_" . $user_id] = $file_name;

            $categoriesTmp = $product->getCategories();
            $categories = [];
            $id_main = $this->module->getCategoryId();

            foreach ($categoriesTmp as $category) {
                if ($category != $id_main) {
                    $categoryClass = new Category(intval($category));
                    $nameofcategory =  $categoryClass->getName();
                    $parrents = $categoryClass->getParentsCategories($this->context->language->id);
                    $prefix = "";
                    for ($i = 0; $i < count($parrents) - 2; $i++) {

                        $prefix .= "- ";
                    }
                    array_push(
                        $categories,
                        [
                            "id" => $category,
                            "name" => $prefix . $nameofcategory,
                        ]
                    );
                }
            }
            $products_info = $this->getFromTable("user_product_validation", "id_product", "*", "id_product=$id_product");
            $product_info = $products_info[0];
            echo json_encode([
                "name" => $product->name[$this->context->language->id],
                "price" => round($product->price),
                "num_sold" => $product->getNbOfSales($id_product),
                "num_sold_lastmonth" => $product->getNbSoldfrom(date("Y-m-d H:i:s", strtotime("-1 months"))),
                "sales_total" => round(intval($product->getSalesTotal())),
                "sales_lastmonth" => round($product->getSales(date("Y-m-d H:i:s", strtotime("-1 months"))), 2),
                "shortDescription" => $product->description_short[$this->context->language->id],
                "description" => $product->description[$this->context->language->id],
                "keyWords" => explode(",", $product->meta_keywords[$this->context->language->id]),
                "validated" => boolval($product->active),
                "images" => $images,
                "files" => $filelist,
                "filesecret" => $random_string,
                "categories" => $categories,
                "notifications" => $product_info["notification_user"],
                "uneditable" => boolval(
                    $product->active
                ),
                "owned" => intval($product_info["owned"]),
                "cut" => floatval($product_info["cut"])
            ]);
        }
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    protected function deleteDirectory($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}
