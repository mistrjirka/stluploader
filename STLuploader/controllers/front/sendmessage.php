<?php

/**
 * <ModuleClassName> => STLuploader
 * <FileName> => creatorpage.php
 * Format expected: <ModuleClassName><FileName>ModuleFrontController
 */
session_start();
class ModifiedProduct extends ProductCore
{
    public static function getNbOfSales($id_product)
    {
        $res = Db::getInstance()->getRow('
			SELECT quantity FROM `' . _DB_PREFIX_ . 'product_sale`
			WHERE `id_product` = ' . (int)$id_product);
        if (!$res) return 0;
        return $res['quantity'];
    }
}
class STLuploaderSendmessageModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $guestAllowed = false;
    public function initContent()
    {
        $this->setTemplate('module:STLuploader//views/templates/front/ok.tpl');
    }

    public function postProcess()
    {
        if (Tools::getValue("action") === "sendmessage") {
            $user_id = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $products_validation = $this->getFromTable("user_product_validation", "id_product", "validated, id_product", "id_user=$user_id");
            $products = [];
            foreach ($products_validation as $product_info) {
                $exists = boolval(count($this->getFromTable("product", "id_product", "id_product", "id_product=" . $product_info["id_product"])));
                if ($exists) {
                    $product = new Product($product_info["id_product"]);
                    $img = $product->getCover($product->id);
                    $link = new Link();

                    array_push(
                        $products,
                        [
                            'ID' => $product_info["id_product"],
                            'image' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https://" : "http://") . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $img['id_image'], 'small_default'),
                            $this->trans('name', [], 'Modules.STLuploader.getproducts.php') => $product->name[$this->context->language->id],
                            $this->trans('price', [], 'Modules.STLuploader.getproducts.php') => $product->price . " Kč",
                            $this->trans('validated', [], 'Modules.STLuploader.getproducts.php') => (!boolval($product_info["validated"])) ? $this->trans('No', [], 'Modules.STLuploader.getproducts.php') :
                                $this->trans('Yes', [], 'Modules.STLuploader.getproducts.php'),
                        ]
                    );
                }
            }
            echo json_encode($products);
        }
        if (Tools::getValue("type") === "getinfo") {
            $user_id = $this->context->customer->id;
            if (!count($this->getFromTable("stluploader_designers", "", "id_user", "id_user=" . $user_id))) {
                http_response_code(403);
                echo "Uživatel není designer";
                return;
            }
            $id_product = Tools::getValue("id_product");
            $product = new ModifiedProduct($id_product);

            //imported from STL uploader
            $img = $product->getCover($product->id);
            $link = new Link;
            $image_objects = $product->getImages($this->context->language->id);
            $image_urls = array();
            foreach ($image_objects as $image_object) {
                if ($image_object["cover"] != 1) {
                    $imgClass = new Image(intval($image_object['id_image']));
                    array_push($image_urls, array(
                        "url" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id],  $image_object['id_image'], 'small_default'),
                        "url_large" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $image_object['id_image'], ''),
                        "name" => $imgClass->id_image . ".jpg",
                        "size" =>
                        128,
                        "id" => $imgClass->id
                    ));
                }
            }


            $images = array(
                "cover" => array(
                    "url" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $img['id_image'], 'home_default'),
                    "url_large" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $link->getImageLink($product->link_rewrite[Context::getContext()->language->id], $img['id_image'], ''),
                    "name" => $img['id_image'] . ".jpg",
                    "size" => 128,
                    "id" => $img['id_image']
                ),
                "others" => $image_urls
            );



            try {
                $product_download_id = $this->getFromTable("product_download", "id_product", "id_product_download", "id_product = $product->id")[0]["id_product_download"];
            } catch (Throwable $error) {
                echo "Tento produkt není virtuální!";
                return "Tento produkt není virtuální!";
            }

            $download = new ProductDownload($product_download_id);
            $user_id = $this->context->customer->id;
            $download_path = _PS_DOWNLOAD_DIR_ . $download->filename;
            $random_string = $this->generateRandomString(20);
            $file_name = _PS_UPLOAD_DIR_ . $random_string . "usr" . $user_id . $product->id;

            try {
                mkdir($file_name);
            } catch (Throwable $err) {
            }

            if (isset($_SESSION["uploadedfiles_" . $user_id])) {
                try {
                    $this->deleteDirectory($_SESSION["uploadedfiles_" . $user_id]);
                } catch (Throwable $err) {
                }
            }
            $zip = new ZipArchive;
            $filelist_uf = [];
            $filelist = [];

            if ($zip->open($download_path) === TRUE) {
                // Unzip Path
                $zip->extractTo($file_name);
                $zip->close();
                $filelist_uf = scandir($file_name, SCANDIR_SORT_ASCENDING);
            } else {
                echo 'Unzipped Process failed';
            }
            foreach ($filelist_uf as $value) {
                if ($value == "." || $value == "..") {
                } else {
                    array_push($filelist, ["name" => $value, "size" => filesize($file_name . "/" . $value)]);
                }
            };
            $_SESSION["uploadedfiles_" . $user_id] = $file_name;


            echo json_encode([
                "name" => $product->name[$this->context->language->id],
                "price" => $product->price,
                "num_sold" => $product->getNbOfSales($id_product),
                "shortDescription" => $product->description_short[$this->context->language->id],
                "description" => $product->description[$this->context->language->id],
                "keyWords" => explode(",", $product->meta_keywords[$this->context->language->id]),
                "validated" => boolval($product->active),
                "images" => $images,
                "files" => $filelist,
                "filesecret" => $random_string
            ]);
        }
    }
    protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
    {
        $sql = new DbQuery();
        $sql->select("$whatToget");
        $sql->from("$tableName");
        $sql->where("$condition");
        $sql->orderBy("$orderBy");
        return Db::getInstance()->executeS($sql);
    }
    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    protected function deleteDirectory($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}
