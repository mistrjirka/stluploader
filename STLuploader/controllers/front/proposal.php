<?php

/**
 * <ModuleClassName> => STLuploader
 * <FileName> => creatorpage.php
 * Format expected: <ModuleClassName><FileName>ModuleFrontController
 */
require_once(_PS_MODULE_DIR_ . "STLuploader/classes/proposalClass.php");
class STLuploaderProposalModuleFrontController extends ModuleFrontController
{
  public $auth = true;
  public $guestAllowed = false;
  public $error = "";
  public function initContent()
  {
    // In the template, we need the vars paymentId & paymentStatus to be defined
    $userid = $this->context->customer->id;
    $pictures = [];
    try {
      $pictures_uf = scandir(_PS_UPLOAD_DIR_ . "registering_proposals/" . $this->context->customer->id);
      foreach ($pictures_uf as $value) {

        if ($value == "." || $value == "..") {
        } else {

          array_push($pictures, ["name" => $value, "size" => 1000000, "url" => "/upload/registering_proposals/" . $this->context->customer->id . "/" . $value]);
        }
      };
    } catch (\Throwable $th) {
      //throw $th;
    }

    parent::initContent();
    if (boolval(count($this->getFromTable("stluploader_registeringdesigners", "id_user", "id_user", "id_user=$userid")))) {

      setcookie("stl_proposal_notification", 1, time() + (86400 * 30), "/");
      header("Location: " . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/muj-ucet");
    }
    $this->context->controller->addJquery();
    $message = Tools::getValue("propose_description", "");
    $doc = new DOMDocument('1.0', 'UTF-8');

    libxml_use_internal_errors(true);
    libxml_clear_errors();
    $doc->loadHTML('<?xml encoding="utf-8" ?>' . $message);
    $script_tags = $doc->getElementsByTagName('script');
    $length = $script_tags->length;


    // for each tag, remove it from the DOM
    for ($i = 0; $i < $length; $i++) {
      $script_tags->item($i)->parentNode->removeChild($script_tags->item($i));
    }
    $message = $this->closetags($doc->saveHTML($doc->documentElement));


    $this->context->smarty->assign(
      [
        "scripts" => [
          $this->module->getPath() . 'views/js/suneditor.min.js',

          $this->module->getPath() . 'views/js/dropzone.min.js',
          $this->module->getPath() . 'views/js/proposal.js',

        ],
        "styles" => [
          $this->module->getPath() . 'views/css/suneditor.min.css',
          $this->module->getPath() . 'views/css/dropzone.css'
        ],
        "error" => $this->error,
        "pictures" => json_encode($pictures, JSON_UNESCAPED_UNICODE),
        "description" => $message
      ]
    );
    $this->setTemplate('module:STLuploader//views/templates/front/propose.tpl');
  }
  public function postProcess()
  {
    if (Tools::isSubmit("submitproposal")) {
      if (!file_exists(_PS_UPLOAD_DIR_ . "/registering_proposals/" . $this->context->customer->id)) {
        http_response_code(422);
        $this->error = "Přidejte alespoň 3 fotografie";
        return "Přidejte alespoň 3 fotografie";
      }
      $pictures = scandir(_PS_UPLOAD_DIR_ . "/registering_proposals/" . $this->context->customer->id);
      if (count($pictures) - 2 < 3) {
        http_response_code(422);
        $this->error = "Přidejte alespoň 3 fotografie";
        return "Přidejte alespoň 3 fotografie";
      }
      $doc = new DOMDocument('1.0', 'UTF-8');
      libxml_use_internal_errors(true);
      $message = Tools::getValue("propose_description");

      libxml_clear_errors();
      $doc->loadHTML('<?xml encoding="utf-8" ?>' . $message);
      $script_tags = $doc->getElementsByTagName('script');
      $length = $script_tags->length;


      // for each tag, remove it from the DOM
      for ($i = 0; $i < $length; $i++) {
        $script_tags->item($i)->parentNode->removeChild($script_tags->item($i));
      }
      $message = $this->closetags($doc->saveHTML($doc->documentElement));

      $id_user = $this->context->customer->id;
      $registred = strval(count($this->getFromTable("stluploader_registeringdesigners", "id_user", "message", "id_user=$id_user"))) || strval(count($this->getFromTable("stluploader_designers", "id_user", "id_user", "id_user=$id_user")));

      if ($registred) {
        return "Jste už registrovaný";
      }

      if (strlen($message) > 100) {
        $proposal = new ProposalClass();
        $proposal->notification_admin = 0;
        $proposal->notification_user = 0;
        $proposal->id_user = $this->context->customer->id;
        $proposal->message = $message;
        $proposal->validated = 0;
        $t = time();
        $proposal->time_stamp = date("Y-m-d H:i:s", $t);
        $proposal->lastsawuser = '0000-00-00 00:00:00';
        $proposal->lastsawadmin = '0000-00-00 00:00:00';
        $proposal->save();
        Mail::Send(
          (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
          'adminnewproduct', // email template file to be use
          "Vaše žádost byla předána ke zpracování", // email subject
          array(
            '{heading}' => "Vaše žádost byla odeslána",
            '{message1}' => "Dobrý den, Váše žádost byla předána ke schvalování. Proces může trvat 1-2 pracovní dny a admin vás během tohoto času může kontaktovat..",
            '{message2}' => "V případě potíží či potřeby kontaktovat administrátora použijte tento <a href='mailto:" . Configuration::get('PS_SHOP_EMAIL') . "'>email</a> nebo použijte zabudovaný chat na stránce Můj účet <a href='https://www.allstl-shop.cz/muj-ucet'> produktu </a>",
            '{message3}' => "Hezký zbytek dne"
          ),
          $this->context->customer->email, // receiver email address
          NULL, //receiver name
          NULL, //from email address
          NULL,  //from name,
          NULL,
          NULL,
          _PS_MODULE_DIR_ . $this->module->name . '/mails/'
        );
        Mail::Send(
          (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
          'adminnewproduct', // email template file to be use
          'Uživatel zažádal o post designéra', // email subject
          array(
            '{heading}' => "Žádost o post designéra",
            '{message1}' => "Uživatel <a href='mailto:"  . $this->context->customer->email . "'>" . $this->context->customer->firstname . " " . $this->context->customer->lastname . "</a> zažádal o post designéra.",
            '{message2}' => "",
            '{message3}' => "Hezký zbytek dne"
          ),
          Configuration::get('PS_SHOP_EMAIL'), // receiver email address
          NULL, //receiver name
          NULL, //from email address
          NULL,  //from name,
          NULL,
          NULL,
          _PS_MODULE_DIR_ . $this->module->name . '/mails/'
        );
      } else {
        http_response_code(422);
        $this->error = "Váš požadavek potřebuje alespoň 100 znaků";
        return "Váš požadavek potřebuje alespoň 100 znaků";
      }
    }
    if (Tools::getValue("action") === "submitRemoveFile") {
      try {
        $user_id = $this->context->customer->id;
        $id_product = Tools::getValue("id_product");
        $id_image = Tools::getValue("id_image");
        $isreal = boolval(count($this->getFromTable("user_product_validation", "id_product", "validated, id_product", "id_product=$id_product AND id_user=$user_id")));
        if (!$isreal) {
          http_response_code(403);
          echo "Produkt nepatří tomuto uživateli nebo produkt neexistuje";
          return;
        }
        $product = new Product($id_product);
        foreach ($product->getImages($this->context->language->id) as $value) {
          if ($value["id_image"] === $id_image) {
            $image = new Image($id_image);
            $image->delete();
          }
        }
      } catch (Throwable $error) {
        echo json_encode(["error" => $error]);
        return;
      }
      echo json_encode(["message" => "ok"]);
    }
  }
  protected function getFromTable($tableName, $orderBy, $whatToget = "*", $condition = "1=1")
  {
    $sql = new DbQuery();
    $sql->select("$whatToget");
    $sql->from("$tableName");
    $sql->where("$condition");
    $sql->orderBy("$orderBy");

    return Db::getInstance()->executeS($sql);
  }
  protected function closetags($html)
  {
    preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
      return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i = 0; $i < $len_opened; $i++) {
      if (!in_array($openedtags[$i], $closedtags)) {
        $html .= '</' . $openedtags[$i] . '>';
      } else {
        unset($closedtags[array_search($openedtags[$i], $closedtags)]);
      }
    }
    return $html;
  }
}
